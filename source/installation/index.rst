
****************************
Installation & Upgrade Guide
****************************

In-depth documentation on installation and deployment of XiVO solution systems.

.. _installation:

XiVO Installation & Upgrade
===========================

.. toctree::
   :maxdepth: 1

   Installation <xivo/installation/installsystem>
   xivo/architecture
   xivo/installation/configuration_wizard/configuration_wizard
   xivo/installation/defaultfrenchconf
   xivo/installation/postinstall
   xivo/hardware/hardware

**XiVO UC**

.. toctree::
   :maxdepth: 1

   xivo/xivouc/xivouc

**Upgrading**

.. toctree::
   :maxdepth: 2

   Upgrade <xivo/upgrade/upgrade>


XiVOcc Installation & Upgrade
=============================

The XiVO-CC software suite is made of several independent components. Depending on your system size,
they can be installed on separate virtual or physical machines. In this section, we will explain how to
install these components on a single machine.

.. important:: Before installing XiVO CC, study carefully the :ref:`xivocc_architecture_and_flows` diagram.

.. toctree::
   :maxdepth: 1

   Installation <xivocc/installation/installation>
   Configure or Customize Links Between Components <xivocc/components_configuration>
   xivocc/phone_integration
   XiVOcc Installation Checks <xivocc/troubleshooting_install>

**For specific installations**

.. toctree::
   :maxdepth: 2

   Manual Installation <xivocc/installation/manual_configuration>

**Upgrading**

.. toctree::
   :maxdepth: 2

   Upgrade <xivocc/upgrade/upgrade>


XiVO Distributed System
=======================

.. toctree::
   :maxdepth: 1

   Installation <xds/installation>
   Architecture <xds/architecture>
   Upgrade <xds/upgrade>
   Uninstallation/Reinstallation <xds/uninstallation>

