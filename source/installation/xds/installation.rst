**************
Installing XDS
**************

.. important:: Before installing, make sure you understand the :ref:`xds_architecture` and links between components.

.. contents:: :local:

The XDS architecture has the following components:

* XiVO
* Media Server (MDS) (one or more)

An XDS needs also:

* a XiVO UC/CC with the UC/CC features (i.e. the CTI Server),
* a XiVO UC/CC with the Reporting features for the centralized call history (i.e. the Reporting Server).

This page will guide you through:

#. the configuration of the XiVO (see :ref:`xds_xivo-configuration` section)
#. the installation and configuration of the MDS (see :ref:`xds_mds-configuration` section)
#. and the configuration of the UC/CC server (CTI and Reporting Server) (see :ref:`xds_cc-configuration` section)



Requirements
============

Before starting you need to have 3 servers.
Here's a table summarizing what we are installing. Replace the IP by those you chose.

+-------------+-----------+--------------+------------------------------+
| **Server**  | server1   | server2      | server3                      |
+-------------+-----------+--------------+------------------------------+
| **Role**    | XiVO      | Media Server | UC/CC (CTI/Reporting Server) |
+-------------+-----------+--------------+------------------------------+
| **Name**    | mds0      | mds1         | cc                           |
+-------------+-----------+--------------+------------------------------+
| **IP Data** | 10.32.0.1 | 10.32.0.101  | 10.32.0.9                    |
+-------------+-----------+--------------+------------------------------+
| **IP VoIP** | 10.32.5.1 | 10.32.5.101  | 10.32.5.9                    |
+-------------+-----------+--------------+------------------------------+

.. _xds_xivo-configuration:

XiVO Configuration
==================

On *server1*:

* install XiVO (see :ref:`install`).
* pass the Wizard


.. _xds_main_ami_configuration:

AMI configuration
-----------------

.. note:: Once a :ref:`media server is defined <xds_xivo-define_media_server>` in webi, the *xucserver* from UC/CC Server will
   immediately start to use the VoIP interface for AMI connection to all media servers **and also to XiVO**.
   Therefore we must ensure that UC/CC Server is able to connect to XiVO AMI via its VoIP interface.

.. warning:: If a XiVO UC/CC is already installed, you **MUST** do the following steps **BEFORE** adding media server.

Otherwise you can first define media servers and do these steps right after XiVO CC installation,
but before starting it to prevent problems with fail2ban.

#. Edit **existing** file :file:`/etc/asterisk/manager.d/02-xivocc.conf` to add permission for *xucserver** of UC/CC Server (CTI Server):

   * ``permit`` to authorize the VoIP IP of the UC/CC Server (CTI Server). E.g.:

      .. code-block:: bash

       ...
       deny=0.0.0.0/0.0.0.0
       permit=10.32.5.9/255.255.255.255
       permit=10.32.0.9/255.255.255.255
       ...


#. Apply the configuration::

    asterisk -rx 'manager reload'


.. _xds_xivo-define_media_server:

Define Media Servers
--------------------

.. note:: Here we define our Media Servers (MDS) names and VoIP IP address.

In XiVO webi,

#. Go to :menuselection:`Configuration --> Management --> Media Servers`
#. Add a line per Media Server (MDS) (below an example for mds1):

   #. :guilabel:`Name`: mdsX (e.g. mds1)
   #. :guilabel:`Displayed Name`: Media Server X (e.g. Media Server 1)
   #. :guilabel:`IP VoIP`: <VoIP IP of mdsX> (e.g. 10.32.5.101) - *note:* the VoIP streams between XiVO and mdsX will go through this IP

Once you define a media server, you will be able to create local SIP trunks that exist only there.
The location can be set in :menuselection:`tab General --> Media server` in the SIP trunk configuration.

Define Media Servers for Provisionning
--------------------------------------

.. note:: Here we configure the Media Servers (MDS) for the phones.

In XiVO webi

#. Go to :menuselection:`Configuration --> Provisioning --> Template Line`
#. Create a template line per MDS (below the example for mds1):

   #. :guilabel:`Unique name`: <mdsX> (e.g. mds1) - *note:* it **must be** the same name as the one defined in section :ref:`xds_xivo-define_media_server`
   #. :guilabel:`Displayed Name`: <Media Server X> (e.g. Media Server 1)
   #. :guilabel:`Registrar Main`: <VoIP IP of mdsX> (e.g. 10.32.5.101)
   #. :guilabel:`Proxy Main`: <VoIP IP of mdsX> (e.g. 10.32.5.101)


Media Servers connection to the XIVO database
---------------------------------------------

.. note:: The MDS need to connect to the XiVO database.

In file :file:`/var/lib/postgresql/15/data/pg_hba.conf` add an authorization **per mds** to connect to the db. Here you will probably want to use the Data IP of mdsX::

  host      asterisk    all     10.32.4.201/32      md5


And reload the database configuration:

.. code-block:: bash

  xivo-dcomp reload db

.. _xds_xivo-smtp-relay:

SMTP relay configuration
------------------------

.. note:: This step is specific to **XiVO Main**

SMTP relay must be configured to receive voicemail notifications from media servers.
:ref:`mail` on XiVO must be configured also.

#. Create custom template for postfix configuration:

    .. code-block:: bash

        mkdir -p /etc/xivo/custom-templates/mail/etc/postfix/
        cp /usr/share/xivo-config/templates/mail/etc/postfix/main.cf /etc/xivo/custom-templates/mail/etc/postfix/

#. Open :file:`/etc/xivo/custom-templates/mail/etc/postfix/main.cf` for editing
#. Add MDS Data IP value to the ``mynetworks`` option:

    .. code-block:: diff

        mynetworks =    127.0.0.0/8
                        [::1/128]
        +               10.32.0.101/32

#. Update configuration

    .. code-block:: bash

        xivo-update-config

.. _xds_xivo-rsync-init:

XDS File Synchronization
------------------------

.. note:: You need to do this manual step on **XiVO Main** for the file sync to take place. See :ref:`xds_file_sync` page for the feature description.


#. Rename files in synced dir (and mainly custom dialplans in ``/etc/asterisk/extensions_extra.d/``) which should not be synchronized to be prefixed with ``xds_override``.
   All files named with this prefix will be excluded from synchronization.
#. Initialize synchronization of dialplans by running the command:

    .. code-block:: bash

        xivo-xds-sync -i

.. note:: The initialization will:

    #. generate a ssh key pair: ``~/.ssh/rsync_xds`` and ``~/.ssh/rsync_xds.pub``
    #. copy the public key to ``/usr/share/xivo-certs/``
    #. make the public key available at ``https://XIVO_HOST/ssh-key``
    #. create cron job ``/etc/cron.d/xivo-xds-sync`` to schedule the synchronization



.. _xds_edge-configuration:

Edge configuration
==================

.. warning:: If an edge is already configured and you are switching from a normal setup to an XDS one,
   do not forget to do the following configuration on the edge side.
   For edge 3 VM, see :ref:`edge_config_3serverssipproxy`.
   For edge on a single VM, see :ref:`edge_config_1serverconf`.

.. _xds_mds-configuration:

Media Server Configuration
==========================

Requirements
------------

On *server2* install a **Debian 11** with:

   * ``amd64`` architecture,
   * ``en_US.UTF-8`` locale,
   * ``ext4`` filesystem
   * a hostname correctly set (files :file:`/etc/hosts` and :file:`/etc/hostname` must be coherent).

Before installing the MDS you **have to** have added:

   * the MDS to the XiVO configuration (see :ref:`xds_xivo-define_media_server` section)


Installation
------------

.. important:: The MDS installer will ask you:

    * the XiVO Data IP Address
    * the Media Server you're installing (taken from the Media Server you declared at step :ref:`xds_xivo-define_media_server`)
    * the Media Server Data IP
    * the Reporting Server IP (i.e. the XiVO UC/CC with reporting features - database, xivo_stats ...)

To install the MDS, download the XiVO installation script:

.. code-block:: bash

   wget http://mirror.xivo.solutions/mds_install.sh
   chmod +x mds_install.sh

and run it:

.. important:: Use ``-a`` switch to chose **the same version** as your XiVO (mds0)

.. code-block:: bash

    ./mds_install.sh -a 2023.05-latest

When prompted:

* give the IP of XiVO (mds0): <XiVO Data IP> (e.g. 10.32.0.2)
* select the MDS you're installing: <mdsX> (e.g. mds1)
* enter the MDS Data IP: <mdsX Data IP> (e.g. 10.32.0.101)
* and finally the Reporting Server Data IP: <reporting Data IP) (e.g. 10.32.0.5)

.. important:: In case of ``re-installation``, you will be prompted to drop the existing database replication slot.

Configuration
-------------

To finalize the MDS configuration you have to:

#. Configure NTP to synchronize on XiVO (mds0) by replacing preconfigured servers/pools in file :file:`/etc/ntp.conf` by:

   .. code-block:: bash

    server 10.32.0.1 iburst

#. And restart NTP:

   .. code-block:: bash

    systemctl restart ntp

#. Create file :file:`/etc/asterisk/manager.d/02-xuc.conf` to add permission for Xuc Server to connect with:

   * ``secret`` must be the same as the secret for xuc user on XiVO,
   * ``permit`` to authorize the VoIP IP of the UC/CC Server (CTI Server). E.g.:

      .. code-block:: bash

       cat > /etc/asterisk/manager.d/02-xuc.conf << EOF
       [xuc]
       secret = muq6IWgNU1Z
       deny=0.0.0.0/0.0.0.0
       permit=10.32.5.9/255.255.255.255
       read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
       write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
       writetimeout = 10000
       EOF

#. Restart the services::

    xivo-service restart all

.. _xds_mds-mail-conf:

Mail configuration
------------------

The install script installs and configures postfix:

* to relay mails towards the XiVO Main - see ``relayhost`` parameter in :file:`/etc/postfix/main.cf` file,
* and use the content of :file:`/etc/mailname` file as the domain part of the from address - see ``myorigin`` parameter in :file:`/etc/postfix/main.cf`.

.. note:: Note that the content of :file:`/etc/mailname` file is taken during installation from the domain if set or the hostname.

   Therefore if mail sent from the MDS are not correctly relayed by the XiVO Main, you should check and play with the value
   of the :file:`/etc/mailname` file. And then reload the postfix if needed ``service postfix reload``.


Outgoing Call Configuration
===========================

Create the Provider Trunk
-------------------------

Add on the XiVO the trunk towards your provider (it can be an ISDN or SIP trunk).
When creating the trunk, select the Media Server on which it will be located.


Create Outgoing Call Rule
-------------------------

.. note:: This outgoing call rule will handle outgoing call from XDS to Provider.

In XiVO Webi:

#. Go to :menuselection:`Services --> IPBX --> Call Management --> Outgoing calls`
#. Create a route for XDS:

   #. Call pattern: X.
   #. Trunks: <your provider trunk>
   #. (after opening the advanced form) Caller ID: <main DID of the system>


.. _xds_cc-configuration:

XiVO CC Configuration
=====================

On *server3*:

* install a XiVO CC (see :ref:`ccinstallation`)
* configure it
* before starting it, change the :ref:`AMI configuration <xds_main_ami_configuration>` on XiVO.


.. _xds_webrtc-configuration:

Enable WebRTC on MDS
--------------------

.. note:: The manual procedure has been automated in kuma.

WebRTC users can be configured on the MDS. When you add a new mds and you don't have edge, you need to update the xivocc nginx configuration as shown below.

#. Either refresh the sip proxy configuration on the nginx and reload it:

   .. code-block:: bash

    xivocc-dcomp exec nginx /docker-entrypoint.d/50-mds-sip-proxy.sh
    xivocc-dcomp reload nginx

#. Or restart the container:

   .. code-block:: bash

    xivocc-dcomp restart nginx
    xivocc-dcomp up -d nginx

#. You can check out your configuration here 

   .. code-block:: bash

    xivocc-dcomp exec nginx cat /etc/nginx/sip_proxy/sip_proxy.conf


Known Limitations
=================

Agent states after XUC restart
------------------------------

Restarting XUC server with active calls in XDS environment will result in having some agents in incorrect state.
Please see the note in :ref:`restarting <agent_states_after_xuc_restart>` XUC server with active calls.
