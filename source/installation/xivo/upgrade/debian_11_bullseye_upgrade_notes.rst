.. _debian_11_upgrade_notes:

**********************************
Debian 11 (Bullseye) Upgrade Notes
**********************************

Debian was upgraded to Debian 11 (Bullseye) in XiVO 2022.XX release.

.. warning:: Upgrade from versions *earlier* than XiVO Freya (2020.18) are not supported.
     You MUST first upgrade to at least XiVO Freya (2020.18) or more before upgrading to XiVO Izar.


.. _debian_11_upgrade_notes_before:

Before the upgrade
==================

.. important:: **Make sure you have sufficient space for the upgrade**.
   You should have more than 2GiB available in the filesystem that holds the ``/var`` and ``/`` directories.

* It is not possible to upgrade from XiVO below Freya (2020.18) version. You first need to upgrade to XiVO Freya.
* Note that the upgrade will take longer than usual because of all the system upgrade.
* You **MUST** deactivate all non-xivo apt sources list:

  * in directory `/etc/apt/sources.list.d/` you should only have the files :file:`xivo-dist.list` and (from Aldebaran) :file:`docker.list` :file:`pgdg.list`.
  * you **MUST** suffix all other files with `.save` to deactivate them.
* You **MUST** check the Debian sources list are correct: the file :file:`/etc/apt/sources.list` must contain the following and only the following::

    deb http://ftp.fr.debian.org/debian/ bullseye main
    deb-src http://ftp.fr.debian.org/debian/ bullseye main

    deb http://security.debian.org/ bullseye-security/updates main
    deb-src http://security.debian.org/ bullseye-security/updates main

    # stretch-updates, previously known as 'volatile'
    deb http://ftp.fr.debian.org/debian/ bullseye-updates main
    deb-src http://ftp.fr.debian.org/debian/ bullseye-updates main

* You may want to clean your system before upgrading:

  * Remove package that were automatically installed and are not needed anymore::

     apt-get autoremove --purge
  * Purge removed packages. You can see the list of packages in this state by running ``dpkg -l |
    awk '/^rc/ { print $2 }'`` and purge all of them with::

      apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')
  * Remove :file:`.dpkg-old`, :file:`.dpkg-dist` and :file:`.dpkg-new` files from previous upgrade. You can see a list
    of these files by running::

      find /etc -name '*.dpkg-old' -o -name '*.dpkg-dist' -o -name '*.dpkg-new'


.. _debian_11_upgrade_notes_after:

After the upgrade
=================


Changes
=======

No major change to advertise here that would come with XiVO on Debian 11.


External Links
==============

`Official Debian 11 Release Notes <https://www.debian.org/releases/bullseye/amd64/release-notes/>`_

