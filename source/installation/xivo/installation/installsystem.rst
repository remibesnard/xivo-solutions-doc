.. _install:

*********************
Installing the System
*********************

Please refer to the section :ref:`Troubleshooting <troubleshooting>` if ever you have errors during the installation.

There are two official ways to install XiVO:

* using the official ISO image
* using a minimal Debian installation and the XiVO installation script

XiVO can be installed on both virtual (QEMU/KVM, VirtualBox, ...) and physical machines. That said, since
Asterisk is sensitive to timing issues, you might get better results by installing XiVO on real
hardware.

.. warning:: By default XiVO installation will pre-empt network subnets 172.17.0.0/16 and 172.18.1.0/24.
    If these subnets are already used, some manual steps will be needed to be able to install XiVO.
    These steps are not described here.



Installing from the ISO image
=============================

.. note:: Our ISO image does not support UEFI system

* Download the ISO image. (`latest LTS version`_) (`all versions`_)
* Boot from the ISO image, select ``Install`` and follow the instructions. You must select locale
  ``en_US.UTF-8``.
* At the end of the installation, you can continue by running the :ref:`configuration
  wizard. <configuration_wizard>`

During the installation of Debian, only a proxy that supports proxying http/https requests may eventually be entered.
Otherwise GPG key of XiVO repository will not be installed and must be added manually::

   wget http://mirror.xivo.solutions/xivo_current.key -O - | apt-key add -

.. _all versions: http://mirror.xivo.solutions/iso/archives
.. _latest LTS version: http://mirror.xivo.solutions/iso/xivo-current


Installing from a minimal Debian installation
=============================================

XiVO can be installed directly over a **64-bit** **Debian 11 (Bullseye)**. When doing so, you are strongly
advised to start with a clean and minimal installation of Debian **Bullseye**.

The latest installation image for Debian **Bullseye** can be found at https://www.debian.org/releases/bullseye/debian-installer.


Requirements
^^^^^^^^^^^^

The installed Debian must:

* not have caps in the hostname
* use the architecture ``amd64``
* have a default locale ``en_US.UTF-8``
* use ``ext4`` filesystem (for compatibility with docker overlay2 storage driver)
* use legacy network interface naming ``eth#``. To change the network interface naming to ``eth#`` use this procedure:

  * Edit ``/etc/default/grub``, find line ``GRUB_CMDLINE_LINUX`` and set it to ``GRUB_CMDLINE_LINUX="net.ifnames=0"``
  * Run ``update-grub``
  * Edit interface names in ``/etc/network/interfaces``
  * Reboot the machine

Installation
^^^^^^^^^^^^

.. note:: If your server needs a proxy to access Internet, configure the proxy for ``apt``, ``wget`` and ``curl`` as documented in :ref:`system_proxy`.

Once you have your Debian Bullseye properly installed, download the XiVO installation script and make
it executable::

   wget http://mirror.xivo.solutions/xivo_install.sh
   chmod +x xivo_install.sh

And run it::

   ./xivo_install.sh -a 2023.05-latest

At the end of the installation, you can continue by running the :ref:`configuration
wizard. <configuration_wizard>`


Alternative versions
^^^^^^^^^^^^^^^^^^^^

The installation script can also be used to install an :ref:`archive version <archive-version>` of
XiVO (14.18 or later only). For example, if you want to install XiVO 2020.18-latest::

   ./xivo_install.sh -a 2020.18-latest

When installing an archive version, note that:

* versions 14.18 to 15.19 of XiVO can only be installed on a Debian 7 (wheezy) system
* the 64-bit versions of XiVO are only available starting from 15.16

You may also install development versions of XiVO with this script. These versions may be unstable
and should not be used on a production server. Please refer to the usage of the script::

   ./xivo_install.sh -h


Other installation methods
==========================

It's also possible to install XiVO by PXE. It is not documented here.
