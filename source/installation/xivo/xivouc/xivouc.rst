.. _xivouc_addon:

**************
XiVO UC add-on
**************

This page describes how to install *XiVO UC* on the *XiVO PBX* server and how to use it.
By *XiVO UC* we mean a subset of *XiVO CC* application, namely the :ref:`Web and Desktop Assistant <uc-assistant>`.


Prerequisites
=============

.. important::
  Your **XiVO PBX** server **MUST** meet the following requirements:

- OS : **Debian 11** (Bullseye), **64 bits**
- **4 GB of RAM**
- 4-core CPU
- 20 GB of free disk space
- you have a *XiVO PBX* installed in a compatible version (basically the two components XiVO and *XiVO UC* have to be
  in the *same* version).
- the *XiVO PBX* **is setup** (wizard must be passed) with users, queues and agents, you must be able to place and answer calls.

.. warning::
  - By default XiVO-UC installation will pre-empt network subnets 172.17.0.0/16 and 172.18.0.0/24.
    If these subnets are already used, some manual steps will be needed to be able to install XiVO-UC.
    These steps are not described here.
  - After installing the XiVO UC the XiVO PBX Administration will be only available at https://XiVO_PBX_IP/admin

.. _xivouc_addon_archi:

Architecture
============

.. figure:: XiVO_UC_Add-on.png

Install process overview
========================

The installation and configuration of *XiVO UC* is handled by the ``xivouc-installer`` script provided with XiVO.
You will be asked few questions during the process:

* the XiVO PBX IP address,
* and whether or not you want to restart *XiVO PBX* by the installer or later

The ``xivouc-installer`` script will install packages `xivouc` and `xivocc-docker-components`.

These packages contain these docker compose files:

* :file:`/etc/docker/compose/docker-xivocc.yml` adds UC components which are managed by ``xivocc-dcomp`` script
* :file:`/etc/docker/xivo/docker-xivo-uc.override.yml` which adds the UC env variable to nginx container so it is started in UC-mode.
  
Install XiVO UC
---------------

On *XiVO PBX*, run XiVO UC installer script::

    xivouc-installer

If you choose to restart *XiVO PBX* later, please do so as soon as possible to apply the modifications made by the installer.
Until then, the *XiVO UC* will not be able to connect correctly to the database.

To restart XiVO services manually, run

.. code-block:: bash

    xivo-service restart all

XiVO will start with DB Replic and Nginx container configured for XiVO UC.


After-install steps
-------------------

After a successful installation, start docker containers using the installed ``xivocc-dcomp`` script:

.. code-block:: bash

    xivocc-dcomp up -d

.. note::
  Please, ensure your server date is correct before starting. If system date differs too much from correct date,
  you may get an authentication error preventing download of the docker images.


Install Chat Backend
====================

:ref:`install_chat_backend`


Install trusted certificates
============================

See :ref:`cert_change_xivonginx_cert`

Upgrade
=======

Packages `xivouc` and `xivocc-docker-components` will be upgraded automatically during :ref:`XiVO PBX upgrade <upgrade>`.

The XiVO UC upgrade must then be completed by pulling new docker containers and starting them:

.. code-block:: bash

    xivocc-dcomp pull
    xivocc-dcomp up -d


Using XiVO UC
=============

After this installation you have on your *XiVO PBX* the *XiVO UC*.

You can now use the :ref:`uc-assistant` using the XiVO PBX IP address. For example you can
access it at:

- https://XIVO_PBX_IP

The XiVO PBX Admin interface is now available at: https://XIVO_PBX_IP/admin


Monitoring
==========

You can monitor and control XiVO UC components from the XiVO web interface (see :ref:`monitoring`).


Uninstallation
==============

Uninstallation consists of these steps:

#. stop Chat backend: ``xivocc-dcomp stop mattermost``
#. purge Chat backend: ``apt-get purge xivo-chat-backend``
#. purge the xivouc package: ``apt-get purge xivouc``
#. purge DB Replic compose file and monit check: ``apt-get purge xivocc-docker-components``
#. remove docker network: ``docker network rm xivocc_default``

.. warning::
  Do not purge the ``xivouc-installer`` package! It is required by XiVO.
