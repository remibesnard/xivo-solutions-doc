.. _agent:

********************
CC Agent Environment
********************

.. note:: This section describes the CC Agent application features.
   It is available as a web application from your Web Browser.
   It is also available as a *desktop application* with these additionnal features:

     * show integrated popup when receiving call
     * get keyboard shortcut to answer/hangup and make call using :ref:`Select2Call feature <dapp_global_key>`
     * :ref:`handle callto: and tel: links <dapp_call_url>`
     * be able to :ref:`minimize the application to a side bar <dapp_agent_minimize>`

   To install the *desktop application*, see :ref:`the desktop application installation <desktop-application-installation>` page.


**What is the CC Agent application ?**

CC Agent is a Web application for contact center operators. Some parameters for Recording, Callbacks, Queue control, Pause statuses and Sheet popup may be configured.
Instructions can be found in the :ref:`configuration section <agent_configuration>`.

From the interface you will be able to :
 * Manage your activities you are subscribed to receive calls.
 * See the customer history inside your organization when a call is coming
 * Interact with your phone from the call control panel
 * Get some real time statistics of your session

The web application can either be displayed in a minimal bar or be extended as seen in screen shot below when launched as standalone application. See :ref:`desktop application <desktop-application-installation>`.

.. figure:: ccagent.png
    :scale: 80%

.. warning:: The application offers support for the WebRTC lines, currently there's a limitation on complementary services like the second call which is only partially supported.


Login
=====

.. figure:: ccagent-login.png
    :scale: 50%

Enter your CTI username, password and phone set you want to logged to on the login page.

If you are using Kerberos authentication and enabled SSO (see :ref:`kerberos-configuration`), then you only have to set your phone set number, the authentication and login will be done automatically:

.. note:: Automatic login keeps you signed in until you log out.

.. _ccagent_statistics:

Statistics
==========

At the top of the application, :ref:`computed statistics <agent_statistics>` from XUC are displayed to monitor the agent activity. Simply hover the icon to know its definition.

They are updated once current action is over.

.. figure:: ccagent-stats.png
    :scale: 80%

Those statistics are clickable and display dynamic charts on top of the counters. The goal is to offer a global view to agents on their activity during the day.

There are two differents charts displayed.

The first one is a bar chart focusing on the time unit :

- Total Available Time
- Total Pause Time
- Inbound ACD Calls Total Time
- Outbound Calls Total Time
- Total Wrapup Time

On click on each one of those buttons, the bar chart will be displayed.
You can hover each bar to have the detailed time spent for a specific indicator (HH:MM:SS).

.. figure:: ccagent-barChart.png
    :scale: 80%

The second one is a donut chart focusing on the number of calls :

- Number of Inbound ACD calls
- Number of Inbound Answered ACD Calls
- Number of Outbound Calls

On click on each one of those buttons, the donut chart will be displayed.
You can hover each part of the donut to have the number of calls for one indicator.

.. figure:: ccagent-donut.png
    :scale: 80%

The charts are updated once current action is over.


Activities
==========

Once logged in you are automatically redirected to ``activities`` view, this view contains the list of activities you are registered in.

.. figure:: ccagent-activities.png
    :scale: 80%

Hovering an activity triggers a popover which displays some real-time statistics about call distribution in this queue.
You may also call or transfer a call to an activity using the displayed phone icon

It is possible to filter on `favorite` activities just by clicking ``My activities`` check box.


Activity Management
-------------------

You can manage ``subscription`` if allowed globally for the application (see :ref:`agent_configuration`). If enabled you will be able to enter/quit an activity just by clicking on checkbox associated to it.

Each time you enter an activity, it is automatically added to your favorites. At any time you can remove it by clicking on `minus` sign next to the name in ``My activities`` view.

.. note:: You can remove an activity if and only if you are not already registered in.

It's also possible to enter all your favorites activities just by one click on ``All`` checkbox.

.. figure:: ccagent-favorites.png
    :scale: 80%


.. _agent_activity_colors:

Activity Colors
---------------

Activity color changes depending on call waiting and agent status:

  * **Grey**: No call, No agents logged in this activity
  * **Green**: At least one agent logged and available in this activity
  * **Orange**: At least one agent logged, but no agents available in this activity
  * **Red**: No agents logged in this activity, but one or more waiting calls in this activity

.. _agent_activity_waitingcalls:

Activity Waiting Calls
----------------------

A little badge displays the number of waiting calls in each activity. The sum of all waiting calls in the agent activities is displayed in top menu and refreshed in real time.

.. figure:: ccagent-waiting-call.png
    :scale: 80%


.. _agent_activity_failed_dst:

Activity's Failed Destination
-----------------------------

In XiVO, when an activity is exceptionnally closed, a sound file containing a message can be played to the caller.
From its *CC Agent* application, an agent who has access to the dissuasion can:

* see which sound file or default queue is currently configured
* select another sound file to be played
* select a default queue as a failed destination, instead of a sound file.

An agent can change the dissuasion if they have been granted access, it means if they have an administrator profile or a supervisor profile with the permission to change the dissuasion.
The agent profile can be set from the configmgt interface. See :ref:`profile_mgt` section.

.. note:: The sound files available for the activity and displayed to the agent and the default queue are to be set by an administrator of the XiVO.
     See :ref:`agent_configuration-activity_failed_dst` section.

In the Activity view, when clicking the *Dissuasion* button, a menu appears with the list of the sound files or default queues available for a queue.
If one of the sound file or default queue is selected for this queue, it will be highlighted in orange and the circle icon will be checked.

.. figure:: ccagent-failed-destination-selected.png
    :scale: 80%

If no sound file or default queue are selected for the queue, the circle icon won't be checked.

.. figure:: ccagent-failed-destination-of-a-queue-not-selected.png
    :scale: 80%

The agent can change the selected sound file or default queue from the dropdown menu.


.. _agent_search:

Search
======

When using the Agent interface, you can at any time search for a user existing in your directory:

.. figure:: ccagent-search.png
    :scale: 90%

When clicking on the icon of a search result a sub-menu appears with the following actions:

**For phone numbers entries:**

* start call (if you click on the number)
* copy phone number into your clipboard (to paste it elsewhere)

**For email entry:**

* start writing an email (it will open the user's configured email client)

    .. note:: by default only the email destination is pre-filled
     but you can also pre-fill the email *subject* and *body* via a template - see :ref:`email_template_configuration`

* copy email into your clipboard (to paste it elsewhere)

.. important:: Integration: to enable this feature, you must configure the directories in the *XiVO PBX*
 as described in :ref:`directories` and :ref:`dird-integration-views`.

 Though the *CC Agent* only supports the display of:

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email


.. _agent_list:

Agent list
==========

When clicking on the ``Agents`` menu, you will see all the agents of **your group**. By hovering one of them, you will quickly find his current state (ready, calling, in pause...)

.. figure:: ccagent-list.png
    :scale: 90%

A simple click on the phone icon when agent is hovered will trigger a call to its phone number associated.

By default, agents are shown only if they are logged in (checkbox *Logged* checked). By unchecking the checkbox *Logged*, you will see all the agents of your group even if they are logged out.


.. _agent_call_control:

Call tracking & control
=======================

When using the Agent interface, you will see your current calls at the top of the screen:

.. figure:: ccagent-call-info.png
    :scale: 90%

This panel will display the current caller name & number and also the associated activity if the call came from one.
You also have two indicator on the right side letting you know:

* if the call is currently recorded
* and if the call is currently listened by a supervisor (on this subject see also :ref:`ccmanager-warn-when-spied`).

By hovering your mouse on the call line, an action pane will slide to display action button on the related call. The available buttons depend on the call state.

.. figure:: ccagent-call-hover.png
    :scale: 90%

.. figure:: ccagent-call-hold-hover.png
    :scale: 90%

The Agent interface use Desktop notification for incoming calls and notify long calls on hold, but this feature needs to be enabled from the browser window when logging in:

  .. figure:: agent-notification.png
      :scale: 90%


.. _uc_agent_shortcuts:

Keyboard shortcuts for call control
-----------------------------------

Simple keyboard navigation (with **UP** and **DOWN** directional keys) is allowed to browse search results. Once a contact has been focused, **ENTER** key
can be pressed to open drop-down menu and so choose, still with directional keys, the action to achieve.


The Agent can also use the basic keyboard shortcuts available to perform basic call control tasks :

- **F3** to **answer** a call

- **F4** to **hangup** the current ongoing call

- **F7** to **complete an attended transfer**

- **F10** to put the *focus* in the **search bar**

.. note:: The list of usable keybindings is available by clicking on the menu at the top-right corner of the ccagent and the switchboard.


.. _uc_hold_notification:

On hold notifications
---------------------

You can be notified if you forget a call in hold for a long time, see :ref:`configuration section <hold_notification>`.

.. figure:: agent-hold-notification.png
    :scale: 90%

Known limitations
-----------------

- For agents with default WebRTC lines that connects to another webrtc line (aka "roaming agent") may randomly take the wrong line configuration (especially when you refresh the page). Only workaround so far is to logout/login the agent to force the retrieval of the correct line. For roaming agent, it is recommended to not set default line to agent and use free sitting to wanted line.

Also see the phone integration :ref:`Known limitations <phone_integration_limitations>`.


.. _agent_agent_history:

Agent Call history
==================

First menu ``History`` tab is displaying the call history of connected agent.
It displays the last 20 calls for the last 7 days period.

Information shown in the call list are :

* *destination number* or *name* of callee if call is *emitted*
* *source number* or *name* of caller if a call is *received* or *missed*
* Call icon status and call start date

By clicking on phone icon you will be able to call back if needed.

.. warning:: Pay attention that agent history is **not** the phone device history, but his call activity independently the device he is connected to. Actually when agent is logged out, if a call is received on his last used phone, nothing will be shown in his history.

.. note:: A call answered by another agent from the queue, will appear as answered in the history of the first agent.

.. figure:: ccagent-history.png
    :scale: 80%


.. _agent_customer_history:

Customer Call History
=====================

While phone is ringing or discussion is ongoing, it is possible to have a quick overview of the customer call history of the caller just by clicking ``information`` menu.

.. important:: :ref:`caller_number_normalization` rules breaks the Customer Call History

The customer history is displayed from most recent to last one with an icon to know quickly waiting time of current or previous call :

- agent with grey `play` icon states for current call
- agent with red `bubble` icon states for an unanswered call
- agent with green `bubble` icon states for an answered call

.. note:: If a call is answered (`green icon`), hovering the line will give the name of the agent who took the call.

.. figure:: ccagent-pathway.png
    :scale: 80%

.. _agent_customer_context:

Customer Call Context
=====================

Second tab of ``information`` menu displays all attached data enriched to the ongoing call or display Sheet fields if you are using :ref:`sheet_configuration`.

The customer history is displayed from most recent to last one with an icon to know quickly waiting time of current or previous call :

.. figure:: ccagent-context.png
    :scale: 80%

It's also possible to trigger either to open a web page, see :ref:`screen_popup` or completely integrate a third party application while agent is having calls, see :ref:`xucmgt_3rd_configuration`.

.. _agent_callbacks:

Callbacks
=========

This view allows to manage callback request see :ref:`Processing Callbacks with CCAgent <callbacks_with_ccagent>` for details.

.. _agent_external_directory_feature:

External directory
==================

This feature will add an additional book button on the left side of the search bar.
It allows to open and close an external directory and is integrated the same way as any other tab content.

.. figure:: ccagent-external-directory.png
   :scale: 70%

To enable it, see :ref:`configuration section <external_directory>`.

Meeting room
============

You can join an audio-video conference, called meeting room, which will open on the side of your application.
See :ref:`meetingrooms` for more information.
