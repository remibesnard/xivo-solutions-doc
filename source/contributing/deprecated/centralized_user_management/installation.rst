.. _gcu_installation:

************
Installation
************

.. contents::

Requirements & Limitations
==========================

The XiVO Centralized User Management requires :

- A server with:

  - Debian 8
  - PostgreSQL >= 9.5 (see `Debian backports <https://backports.debian.org/Instructions/>`_ or `Postgresql Wiki <https://wiki.postgresql.org/wiki/Apt>`_ for installing instructions)
  - Docker > 1.12 and corresponding Docker-Compose. Since version 2018.04 XCU
    requires Docker-CE instead of Docker Engine.
  - git installed
  - sudo installed

- Some XiVOs to manage !

  - see the next section for limitations on managed XiVOs.

.. _gcu_installation_requirements_for_xivos:

XiVO(s) Requirements & Limitations
==================================

.. warning:: Please double-check these requirements to prevent unexpected behavior.

For each Xivo which will be added to XCU ensure:

1. Create an Incoming calls interval in the `from-exten` context with a did length equal to the internal number length for each interval managed by XCU.
2. SCCP devices are not supported an may trigger error in the Centralized User Management. You must remove them on your XiVO before using this application.
3. On any context, Users interval *Number range start* and *Number range end* from must be 1-6 six digits (no other characters are allowed).
4. If you are making circular inclusions of asterisk context the XCU can potentially load users for a while, you should be **very** careful with such deployment.

Centralized routing will require further configuration - see :ref:`xcu_configuration_of_centralized_routing`.

Installation by installer package
=================================

Install the *gcu-installer* package via *apt*:

#. Create the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the following line
   (replace **VERSION** with the current version, e.g. *2017.11*)::

    deb http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main

#. Add GPG key of XiVO repository::

    wget http://mirror.xivo.solutions/xivo_current.key -O - | apt-key add -

#. Update your source list and install the package::

    apt-get update
    apt-get install gcu-installer

The configuration files are located in ``/etc/docker``.

.. _xcu_configuration:

Configuration
=============

The XCU configuration files are installed by the installer package to the `/etc/docker/` directory.

Authentication
--------------

Authentication is configured in ``/etc/docker/interface-centralisee/application.conf``, section ``authentication``:

* in ``authentication.login`` you can change initial user credentials (default admin / superpass)
* in ``authentication.ldap`` you can add configuration to use authentication via LDAP


.. _xcu_interval_overlapping:

Interval overlapping
--------------------

A parameter called `allowIntervalOverlap` with default value `false` is available in
``/etc/docker/interface-centralisee/application.conf``. When set to `false`, the XCU does not allow use overlapping
intervals, when an interval is created or edited the XCU checks whether the interval overlaps with other intervals
on all XiVOs and if it does the action is rejected. This default setting helps you to preserve a coherent numbering plan.

If for some reason you need to allow interval overlapping, you just need to change the value in the configuration file
to `true` and restart the XCU. It can be useful when some existing XiVO servers with overlapping intervals were
imported or when you want to be able to migrate some user to another XiVO without changing its number.

Run the application
===================

Star XCU by following command:

.. code-block:: bash

	docker-compose -p icdu -f /etc/docker/compose/icdu.yml up -d

Alternatively, you can set a bash alias for conveniently run XCU:

.. code-block:: bash

	alias dcomp='docker-compose -p icdu -f /etc/docker/compose/icdu.yml'

In that case you can use simpler command :

.. code-block:: bash

	dcomp up -d

XCU should now be accessible through http://my-server-ip or http://my-server-ip:9001


Application logs
================

1. General application log is in ``/var/log/interface-centralisee/application.log`` with daily rotation, historic logs
   retained for 5 days.
2. User actions are logged to ``/var/log/interface-centralisee/user_actions.log`` with daily rotation, historic logs
   retained for 366 days.

By default ``user_actions.log`` contains only brief information about which authorize XCU user did what action.
To log with more detail (including data of create and update actions), change in
``/etc/docker/interface-centralisee/logback.xml`` line:

.. code-block:: xml

    <logger name="UserActions"  level="INFO">

into:

.. code-block:: xml

    <logger name="UserActions"  level="DEBUG">
