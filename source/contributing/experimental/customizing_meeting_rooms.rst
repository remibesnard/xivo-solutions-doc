*************************
Customizing Meeting Rooms
*************************

.. warning:: Experimental Feature

    * this is Work In Progress
    * this is not for production use
    * no support will be provided for this feature

Description
===========

Our Meeting Rooms come with additionnal features that you can enable to enhance your experience. 
The ones described here are available but won't be supported.



.. _jitsi_background_selection:

Background Selection
====================

Starting with the Izar LTS, it becomes possible to allow users in a Meeting Room to select their own background during the meeting.
To do so, an administrator of the Meeting Rooms virtual machine must add the following environment variable in the :file:`/etc/docker/meetingrooms/.env` file  :

    .. code-block:: bash

     ENABLE_BACKGROUND_SELECTION=true

We recommend to set it under the `Basic Config` category.

Then the jitsi web container must be recreated :

    .. code-block:: bash

     meetingrooms-dcomp up -d web


To select the background, the users in the conference must go to the ... on the option bar then click on the `Select background` option.
Here they get to chose a background among the ones available by default, from an image file on their machine, or to dynamically share one of their application as a background.
They can also blur instead their default background if they want to. The application remembers the last used background for the next conference. 

.. figure:: backgroundselection.png

.. warning::
    The background selection can become quite hungry networking-wise, and may affect the other imputs of the conference.
    Be sure that your connection is strong enough before choosing to establish it.

To disable it, you need to remove the variable from the :file:`/etc/docker/meetingrooms/.env` file or setting it to false instead of true, then to recreate the jitsi web container once more.

    .. code-block:: bash

     ENABLE_BACKGROUND_SELECTION=false

    .. code-block:: bash

     meetingrooms-dcomp up -d web
