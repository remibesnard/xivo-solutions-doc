*********
Reporting
*********

.. contents::

.. _totem_panels:

Totem Panels
============

The *XiVO CC* comes with the ELK [1]_ stack. This offers the possibility to do some dashboard to visualize, for example, your call center activity.

This can also be used to explore your data and analyze it.

Example:

.. figure:: kibana_dashboard_ccagent_example.png


.. seealso::

  * :ref:`totem_panels_configuration` section to have the demo dashboard.
  * :ref:`totem_data_flow` section to learn about what is put inside Elasticsearch.

.. [1] ELK: Elasticsearch, Logstash, Kibana - see https://www.elastic.co


.. _totem_panels_configuration:

Import Default Configuration and Demo Dashboards
------------------------------------------------

Preconfigured panels and index pattern are available on our gitlab repository.

#. Download the sample panels from https://gitlab.com/xivocc/sample_reports/blob/master/kibana-set/kibana_from_gaia.ndjson
#. Import this json file in Kibana:

  * Goto the `Kibana homepage --> manage`, then select `Kibana --> Saved Object`

     .. figure:: kibana_mngt.png

  * Click on :menuselection:`Import` button,

    .. figure:: kibana_import.png
  
  * In the new menu select the previously :file:`kibana_from_gaia.ndjson` downloaded file
  * If not checked, activate the *Automatically overwrite all saved object ?* toggle
  * Click on *Import* button

You can now browse the sample reports in :menuselection:`Dashboard`

.. figure:: kibana_dashboardmenu.png


Note that to work on your environment, the *CC - Queues* dashboard must edited to change the queues name.


.. _totem_data_flow:

Data flow
---------

Event data (i.e. the ``queue_log`` table) is replicated in two steps:

1. Replication from XiVO PBX to XiVO CC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Data are replicated almost instantaneously with the ``db_replic`` service. See also the :ref:`Replication schema <replication_schema>`.
To see replication status open :file:`/var/log/xivo-db-replication/xivo-db-replication.log` on XiVO.

2. Replication from XiVO CC to Elasticsearch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Data are replicated every 1 minute with Logstash.

The information about the last replicated id is lost when the Logstash container is recreated
(e.g. when upgrading to another LTS). In this case Logstash re-replicates the last 7 days of data.
Note that as the data are already stored in Elasticsearch, no progress will be visible and no new data will appear
for few minutes.

.. _reporting_es_memory:

Elasticsearch memory management
-------------------------------

Since Helios, Elasticsearch's default memory is set to 1.5 GB (but it actually uses more memory, something around 500 MB up to the limit).  
You are able to override this value by setting the ``ES_JAVA_OPTS`` variable in the ``/etc/docker/compose/custom.env`` file on XiVO CC. 
You can choose your values as shown below (Xms and Xmx must be the same):  

   * ``ES_JAVA_OPTS=-Xms3g -Xmx3g``  or 
   * ``ES_JAVA_OPTS=-Xms2500m -Xmx2500m``

.. warning:: To apply the configuration you must run ``xivocc-dcomp up -d``.

   **Warning**: This will **recreate** the elasticsearch container. Therefore you will loose all data (kibana dashboard included).
   So you  **MUST** :ref:`backup your Kibana configuration beforehand <xivocc_backup_kibana>`.


Elasticsearch data purging
--------------------------

Elasticsearch is configured to keep at least 30 days of data.


