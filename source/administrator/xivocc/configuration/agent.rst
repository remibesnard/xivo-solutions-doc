.. _agent_configuration:

**********************
CC Agent configuration
**********************


.. contents::

.. _agent_recording:

Recording
=========

Recording can be paused or started by an agent.

+---------------------------------------+------------------------------------------+
| Recording On                          | Recording Paused                         |
+=======================================+==========================================+
|                                       |                                          |
|   .. figure:: agent_recording_on.png  |  .. figure:: agent_recording_pause.png   |
|       :scale: 70%                     |      :scale: 70%                         |
|                                       |                                          |
+---------------------------------------+------------------------------------------+

This feature can be disabled by changing ``showRecordingControls`` option in file :file:`application.conf`.
You can also set the environnment variable ``SHOW_RECORDING_CONTROLS`` to false for your xucmgt container in :file:`/etc/docker/compose/custom.env` file. When disabled the recording status is not displayed any more


Activity (Queue) control
========================

By using the ``showQueueControls`` option in application.conf, you may allow an agent to enter or leave an activity.
You can also use ``SHOW_QUEUE_CONTROLS`` environment variable in :file:`/etc/docker/compose/custom.env` file.

.. figure:: ccagent_activities.png
    :scale: 90%


.. _agent_configuration-activity_failed_dst:

Activity's Failed Destination Configuration
===========================================

An agent can see and change the sound file played to the caller when the Activity is temporarily closed [#]_.

For this to work the XiVO Administrator needs to:

#. :ref:`import sound files in the XiVO PBX with the queue name prefix <agent_configuration-activity_failed_dst-soundfiles>`,
#. :ref:`configure the default queue <agent_configuration-activity_failed_dst-dfltqueue>`,
#. :ref:`give the rights to the XiVO User <agent_configuration-activity_failed_dst-rights>`.

.. [#] Temporarily closed, here, means when no agent is logged in.
       More precisely when the call is sent to the *Fail* destination (see :menuselection:`Services --> Call Center --> Queues`
       in queue configuration, tab :menuselection:`No Answer` and *Fail* section).
       A call is sent to the *Fail* destination according to the :guilabel:`Join an empty queue` and :guilabel:`Remove callers if there are no agents` parameters
       in the :menuselection:`Advanced` tab of the queue.


.. _agent_configuration-activity_failed_dst-soundfiles:

1. Import sound files in the *XiVO PBX*
---------------------------------------

To see the sound files, a XiVO Administrator must import them with the queue name prefix as described below:

* Given the queue *Sales* with its :guilabel:`Name` being ``sales``,
* Given I want to have two messages available for this queue:

  * :file:`closed_during_afternoon` (*'We are closed this afternoon ...'*)
  * :file:`in_a_meeting` (*'Currently in a meeting ...'*)
* Then, as a XiVO Administrator I **MUST** import these sound files **with the prefix** ``sales_``. For each file, do:

  * Go to :menuselection:`Services --> IPBX --> IPBX Services --> Audio files`
  * Click to **Add** a file

    * :guilabel:`File name`: select the file on your disk
    * :guilabel:`Directory`: select **playback** directory (this is mandatory)
  * When file is added, edit it and *prefix the file name* **with the queue name followed by an underscore**, in our exampe ``sales_``


Said differently the agent will be able to see and set all sound files prefixed with the queue name.


.. _agent_configuration-activity_failed_dst-dfltqueue:

2. Configure the default queue for dissuasion in the *XiVO PBX*
---------------------------------------------------------------

You can configure a default queue for dissuasion by setting the queue name in ``defaultQueueDissuasion`` in application.conf.
This option once set, will show the queue in the dissuasion dropdown.
You can also use ``QUEUEDISSUASION_DEFAULT_QUEUE`` environment variable in :file:`/etc/docker/xivo/custom.env` file on the XiVO PBX and restart the services with ``xivo-dcomp up -d``.


.. _agent_configuration-activity_failed_dst-rights:

3. Give the rights to the XiVO User
-----------------------------------

To see/change the dissuasion destination via the CC Agent or CC Manager, a XiVO User must have a supervisor right with *Update dissuasion* access.
To do this a XiVO Administrator must connect to the configmgt (https://XIVO_IP/configmgt) and configure the user with:

* :guilabel:`Profile`: Supervisor
* :guilabel:`Access`: check the box *Update dissuasions*
* :guilabel:`Queues`: select the queues the user can see

See :ref:`profile_mgt`.


.. _hold_notification:

On hold notification 
====================

You can configure ``notifyOnHold`` (in seconds)) in application.conf, this option once set, will trigger a popup and a system notification to user if he has a call on hold for a long time.
You can also use ``NOTIFY_ON_HOLD`` environment variable in :file:`/etc/docker/compose/custom.env` file.

Pause Cause and Status
======================

You can configure XiVO to have the following scenario:

* The agent person leaves temporarily his office (lunch, break, ...)
* He sets his presence in the CCAgent to the according state
* The agent will be automatically set in pause and his phone will not ring from
  queues
* He comes back to his office and set his presence to 'Available'
* The pause will be automatically cancelled

.. figure:: ccagent_states.png
    :scale: 90%

By default the pause action from the agent cannot be specified with a specific cause such as Lunch Time, or Tea Time. To be able to use a specific cause,
you will have to define new Presences in the cti server configuration.

.. figure:: agentpausecauses.png
    :scale: 90%

You **must** define pause status with action **Activate pause to all queue** to **true**, to be able to pause agent on all the queues he is working on.

.. note:: **Optional** You can edit *ready* presence defined with an action **Disable pause to all queue** to fallback to be available whenever the agent reconnects back to CCAgent. (for example, you closed the webpage, or did a F5 refresh). Once you reconnect you will be put in *ready* state. 
  
By default, if you don't add option to *ready* state, you will reconnect with last presence state known.


When this presences are defined, you must restart the xuc server to be able to use them in ccagent, these presences will also be
automatically available in :ref:`CCmanager <ccmanager>` and new real time counters will be calculated.

+---------------------------------+---------------------------------+
| Presence from ready to pause    |   Presence from pause to ready  |
+=================================+=================================+
|                                 |                                 |
|   .. figure:: readytopause.png  |  .. figure:: pausetoready.png   |
|       :scale: 70%               |      :scale: 70%                |
|                                 |                                 |
+---------------------------------+---------------------------------+

.. note:: Presence color are not taken into account in the CC Agent (or CC Manager) application.
    They all will be displayed in *Red*.

Screen Popup
============

This section has been moved to :ref:`configuration <screen_popup>`

Run executable
==============

This section has been moved to :ref:`configuration <run_executable_sheet>`
          
.. _login_pause_funckeys:

Login or Pause management using function keys
=============================================

You can configure Login or Pause keys on an agent phone. Their state will be synchronized with the state in *XiVO CC*
applications.

Behavior
--------

* **Login Key**:

  * change status of agent: login if it was logged out, and logout if it was logged in
  * LED of phone key will be updated accordingly as well as the status in *XiVO CC* applications (CCAgent...)
  * if you login/logout via *XiVO CC* applications (CCAgent...), status will be updated and phone key LED will be updated.

* **Pause key**:

  * change status of agent: pause if it was ready, ready if it was paused or in wrapup
  * LED of phone key will be updated accordingly as well as the status in *XiVO CC* applications (CCAgent...)
  * if you pause/unpause via *XiVO CC* applications (CCAgent...), status will be updated and phone key LED will be updated
  * **Wrapup**: if agent is on wrapup, the phone key will *blink*. If you press key while on wrapup, agent status will be changed to ready

    .. note::

      * The key blinks on Snom and Yealink phone sets. It doesn't blink on Polycom phone sets.
      * To be able to terminate Wrapup via the key on Snom phones you must use correct version of plugin (see
        `devices releasenotes <https://documentation.xivo.solutions/projects/devices/en/latest/devices/devices.html#plugins-release-notes>`_).


Configuration
-------------

There are two types of customizable :ref:`function keys <function_keys>` that can be used

* Login: it will toggle login/logout of agent. There are two configuration patterns (see also below):

    * either ``***30<PHONE NUMBER>``: in this case it will ask for agent number and will then login the given agent on phone
      ``<PHONE NUMBER>``
    * or ``***30<PHONE NUMBER>*<AGENT NUMBER>``: in this case it will log agent ``<AGENT NUMBER>`` on phone ``<PHONE NUMBER>``

* Pause: it will toggle pause/unpause of agent (and will blink if agent is on Wrapup). Configuration pattern (see also below):

   * ``***34<PHONE NUMBER>`` : it will toggle pause/unpause of agent logged on phone ``<PHONE NUMBER>``

To use it you must:

#. On on XiVO PBX edit ``/etc/xivo-xuc.conf`` and change variables:

   * ``XUC_SERVER_IP`` to IP address of XivoCC
   * ``XUC_SERVER_PORT`` to port of XUC Server (default is 8090)

#. On XiVO CC (XuC) check that the *XiVO PBX* IP is in the list of host allowed to use the :ref:`restapi_xuc_deprecated` (see ``DEPRECATED_API_HOST``)

#. Configure function key on user (example with user 1000, 1001 and agent 8000 associated to user 1000):

   * Open :menuselection:`Services > IPBX > IPBX settings > Users`
   * Edit the user, open *Func Keys* tab and add keys like:
   * For Pause/Unpause agent logged on phone 1000 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***341000``,
      * :guilabel:`Label`: ``Pause``,
      * :guilabel:`Supervision`: ``Enabled``

   * For Login/Logout agent on phone 1000 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***301000``,
      * :guilabel:`Label`: ``Login``,
      * :guilabel:`Supervision`: ``Enabled``

   * For Login/Logout agent 8000 on phone 1001 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***301001*8000``,
      * :guilabel:`Label`: ``LoginOn1001``,
      * :guilabel:`Supervision`: ``Enabled``


.. figure:: agent_funckey.png
    :scale: 90%


.. _listen_funckeys:

Listened advertisement using function keys
==========================================

You can configure a *Listened* key on agent phone to warn him when his conversation is listened by a supervisor.


Behavior
--------

* **Listen key**:

  * LED of phone key will be lit if supervisor starts to listen to agent's conversation (see :ref:`ccmanager_actions_agent`)

Configuration
-------------

To configure add a customizable :ref:`function key <function_keys>`:

* Listened: the phone key will be lit when the agent's conversation is listened to by the supervisor. Configuration pattern (see also below):

   * ``***35<PHONE NUMBER>`` : it will toggle listened status for agent loged on phone ``<PHONE NUMBER>``


#. Configure function key on user (example with user 1000):

   * Open :menuselection:`Services > IPBX > IPBX settings > Users`
   * Edit the user, open *Func Keys* tab and add keys like:
   * For Pause/Unpause agent logged on phone 1000 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***351000``,
      * :guilabel:`Label`: ``Listened``,
      * :guilabel:`Supervision`: ``Enabled``

#. Activate option for xucserver: see :ref:`ccmanager-warn-when-spied` section.

