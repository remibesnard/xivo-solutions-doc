.. _webassistant_configuration:

*************************
Web / Desktop Application
*************************

.. contents::


Common features
===============

.. _webassistant_disable_webrtc:

Disabling WebRTC
----------------

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.


.. _webassistant_history_nbdays:

Call History Number of Days
---------------------------

Call history by default shows the last 7 days. You can change it by setting the ``CALL_HISTORY_NB_OF_DAYS`` environment variable to a specific number of days in the :file:`/etc/docker/compose/custom.env` file.

.. warning:: Note that setting this to a large number of days may slow down the solution.


.. _webassistant_disable_chat:

Disabling chat in UC Assistant and Switchboard
------------------------------------------------

The chat feature can be disabled globally by setting the ``DISABLE_CHAT`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.


.. _disable_download_buttons:

Disabling download desktop applications
---------------------------------------

To disable download buttons from the login page, you need to set ``SHOW_APP_DOWNLOAD`` to ``false`` in XiVO CC :file:`/etc/docker/compose/custom.env` file.

.. _webassistant_lang_fallback:

Change default fallback language
--------------------------------

Currently all web applications are translated in French if the locale of the browser (or the OS lang for desktop application) is not found.
You can change, for example to fallback to english if the locale is not known by XiVO.

To do so, you need to set ``APP_LANG_FALLBACK`` in XiVO CC :file:`/etc/docker/compose/custom.env` file with one of the following value:

* ``fr`` : French (default)
* ``en`` : English
* ``de`` : German

.. _external_directory:

External Directory
------------------

To enable External Directory feature you need to configure it in the :file:`/etc/docker/compose/custom.env` file::

    EXTERNAL_VIEW_URL=https://myxivocc/externaldir


.. note:: When ``EXTERNAL_VIEW_URL`` is set, it will be displayed in both UC Assistant and CC Agent

.. warning::

  Take care of the following restrictions:

  * The ``EXTERNAL_VIEW_URL`` must be seen as hosted by XiVO CC platform (otherwise it won't open because of CORS restriction).
  * You MUST use the same HTTP protocol to access the CC application (UC Assistant or CC Agent) AND to access the external view.
    For example, if you access the application over HTTPS, you must access the external view over HTTPS too (to the avoid *Mixed Content* errors).
  * The external URL *MUST NOT* have the 'X-Frame-Options' to 'sameorigin', else the feature will not work (e.g. you can't use google.com as directory...).

.. _credentials_validity_period:

Credentials validity period
---------------------------

.. warning::

  For security reasons, we strongly recommend **to not change** these options (i.e. to not **raise** the credential lifetime).
  This warning applies even more if you are connecting UC application through the :ref:`xivo_edge` solutions (i.e. when the UC applications
  are opened on the Internet).

The default validity period for authentification credentials is one hour for a CTI user and one day for a web service user. This credential is automatically renewed while using the assistants.
This is also the period during which you do not have to enter your password when reconnecting to the assistants.

This duration in seconds can be configured in the :file:`/etc/docker/compose/custom.env` file::

    AUTH_CTI_EXPIRES=3600
    AUTH_WEBSERVICE_EXPIRES=86400

.. _email_template_configuration:

Email Template
--------------

To pre-fill email when clicking on an email link of XiVO users search results (see UC Assistant :ref:`uc_assistant_search` section) you need to create the :file:`/etc/docker/xucmgt/email.tpl` file on XiVO CC server:

This template must be in a `Json` valid format and **must contain** the two following fields:

+--------------+----------------------------------------+
| field        | Description                            |
+==============+========================================+
| subject      | Pre-filled text for email subject      |
+--------------+----------------------------------------+
| body         | Pre-filled text for email body         |
+--------------+----------------------------------------+

The template text can contain parameters that will be automatically replaced by actual value when clicking on email link. Format to include in text is ``{parameter}``.
If the replacement is not possible the parameter will be replaced by a default place holder value.

+--------------+----------------------------------------------------------+----------------+
| Parameter    | Description                                              | Default value  |
+==============+==========================================================+================+
| callernum    | The ongoing call caller phone number                     | ``##NUMBER##`` |
+--------------+----------------------------------------------------------+----------------+
| dstname      | The full name of search result associated to this email  | ``##NAME##``   |
+--------------+----------------------------------------------------------+----------------+

Here is a sample of a valid email template:

.. code-block:: json

    {
      "subject": "Information about missed call",
      "body": "Hello {dstname}, someone tried to reach you. He wants to be called back on {callernum}."
    }

.. warning::

  Take care of the following limitations:

  * Email display must be configured for directory serach (see :ref:`directories` and :ref:`dird-integration-views`)
  * This features relies on the html `mailto:` tag therefore:

    * an email client must be configured for the email to be sent
    * beware of the URI maximum length for browser: the *subject* and *body* will be appended in a `mailto:` tag to the email address.
      Beware that there may be some limitations on the URI size (2000 characters might be good maximum
      `as you can see in this stackoverflow post <https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers>`_)
  * Template will apply to all users of the system on all applications (UC Assistant, CC Agent, Switchboard)
  * No carriage return can be put in the text template (email body will be only on 1 line)

.. _screen_popup:

Screen Popup
------------

It is possible to display customer information in an external web application using Xivo :ref:`sheet <custom-call-form>` mecanism.

* Go to :menuselection:`Services > CTI Server > Sheets > Models` to configure a sheet:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with at least ``folderNumber`` and ``popupUrl`` fields set:

    * ``folderNumber`` (MANDATORY)

      * field type = ``text``
      * It has to be defined. Can be calculated or use a default value not equal to "-"
      * Note: You could leave "empty" using a / symbol or using a whitespace (in hexadecimal: %20)

    * ``popupUrl`` (MANDATORY)

      * field type = ``text``
      * The url to open when call arrives : i.e. http://mycrm.com/customerInfo?folder= the folder number will be automatically
        appended at the end of the URL
      * Additionally to the existing xivo variables, you can also use here the following variables(only available in Web Agent and Desktop Agent):

        * ``{xuc-token}``: will be replaced by a token used for xuc websocket and rest api, for example ``http://mycrm.com/customerInfo?token={xuc-token}&folder=``
        * ``{xuc-username}``: will be replaced by the username of the logged on user, for example ``http://mycrm.com/customerInfo?username={xuc-username}&folder=``

    * ``multiTab`` (OPTIONAL)

      * field type = ``text``
      * set to the text ``true`` to open each popup in a new window.


* Then go to :menuselection:`Services > CTI Server > Sheets > Events` and choose the right events for opening the URL (if you choose two events, url will opened twice etc.)

Example : Using the caller number to open a customer info web page

* Define ``folderNumber`` with any default value i.e. 123456
* Define ``popupUrl`` with a display value of http://mycrm.com/customerInfo?nb={xivo-calleridnum}&fn= when call arrives web page http://mycrm.com/customerInfo?nb=1050&fn=123456 will be displayed

    .. figure:: example_xivo_sheet.png
      :scale: 90%

.. _screen_popup_ucassistant:

Screen popup on UC Assistant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default the sheet:

* on *CC Agent* application the sheet is opened by default,
* on *UC Assistant* application the sheet is **not** opened by default.

You can change the behavior with the following sheet variables:

* ``popupUCActivated``:

  * if set to ``true`` the sheet will be opened on *UC Assistant* application
  * if set to ``false`` (default) the sheet won't be opened on *UC Assistant* application

* ``popupAgentActivated``:

  * if set to ``true`` (default) the sheet will be opened on *CC Agent* application
  * if set to ``false`` the sheet won't be opened on *CC Agent* application


For example, if you want the sheet to **only** open on UC Assistant application you should add in your sheet configuration:

* in :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *Sheet* add the following definition:

    * ``popupAgentActivated``

      * field type = ``text``
      * display value = ``false``
    * ``popupUCActivated``

      * field type = ``text``
      * display value = ``true``


.. note:: These variables can also be filled via a dialplan variable value with the ``UserEvent`` application and the ``{dp-...}`` syntax mechanism.
    See the XiVO PBX :ref:`sheet description <custom-call-form>`.


Desktop Assistant Specific Features
===================================

.. _run_executable_sheet:

Run executable
--------------

It is also possible to run an executable using Xivo :ref:`sheet <custom-call-form>` mecanism. This is only available in the desktop agent and desktop assistant.

.. warning:: For the executable to be run on a Desktop Assistant in **UC mode**, you need to activate the :ref:`screen_popup_ucassistant`
    (in **CC Agent mode** the screen popup doesn't need to be activated and therefore the executable will be run out-of-the-box).

* Go to :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with at least ``runAsExecutable`` and ``popupUrl`` fields set:

    * ``popupUrl`` (MANDATORY)

      * field type = ``text``
      * It should contain an executable name accessible by the client user (where the desktop application is) or a full executable path.

    * ``runAsExecutable`` (MANDATORY)

      * field type = ``text``
      * Display value ``true``

    * ``executableArgs`` (OPTIONAL)

      * field type = ``text``
      * set the argument for the executable.


* Then go to :menuselection:`Services > CTI Server > Sheets > Events` and choose the right events for starting the application.

Example : Run the ``notify-send`` command on linux:

* Define ``popupUrl`` with a display value of ``notify-send``
* Define ``runAsExecutable`` with a display value of ``true``
* Define ``executableArgs`` with a display value of ``caller:{xivo-calleridnum}`` where the variable ``xivo-calleridnum`` will be replaced by the caller phone number.

.. figure:: example_xivo_sheet_exe.png
    :scale: 90%
