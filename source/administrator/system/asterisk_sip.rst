.. _asterisk_sip:

**************
Asterisk & SIP
**************

Asterisk default SIP channel driver is since XiVO Izar *PJSIP*.

.. note:: Please note the :ref:`upgrade_chan_sip_pjsip_migration_guide`.

Debugging
=========

See useful links:

- The upgrade notes on the dialplan differences: :ref:`upgrade_chan_sip_pjsip_migration_guide`
- https://www.asterisk.org/new-pjsip-logging-functionality/
- https://wiki.asterisk.org/wiki/display/AST/Asterisk+PJSIP+Troubleshooting+Guide


.. _asterisk_sip_chan_sip_fallback:

Fallback to ``chan_sip`` SIP channel driver
===========================================

.. warning:: If you chose to fallback to ``chan_sip`` you will need to redo it for each bugfix upgrade.


For Izar LTS a fallback mechanism has been implemented to be able to fallback to deprecated ``chan_sip`` SIP channel driver.

On your XiVO, run the following command to switch your XiVO from ``res_pjsip`` to ``chan_sip``::

  xivo-switch-sip-driver SIP

And follow the instructions displayed.

.. note:: If you're running an XDS installation you MUST switch all the XiVO and MDS of the installation to the same
   SIP channel driver.

