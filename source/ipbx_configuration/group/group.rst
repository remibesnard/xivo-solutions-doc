******
Groups
******

Groups are used to be able to call a set or users.

Group name cannot be ``general`` reserved in asterisk configuration.

When calling a group, if nobody is online in the group, the call will be directed toward the "fail" no-answer scenario configured for this specific group.