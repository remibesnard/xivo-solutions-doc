.. _labels:

***********
User Labels
***********

You can configure user labels in :menuselection:`Services --> IPBX --> IPBX Settings --> User Labels`.
Label allows an administrator to group its XiVO users.


.. important:: **Note for Webi admin user**

  For admin users of the Webi (see :ref:`webi_admin_users`) edit its permission and add permission:

  * for the *IPBX -> IPBX Settings -> User Labels* menu
  * **and** for *Control System --> Authenticate configuration server web services request (required for admin users)* 

