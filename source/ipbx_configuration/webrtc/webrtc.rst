******
WebRTC
******

General notes
=============

.. note:: added in version 2016.04

.. note:: Current WebRTC implementation (since XiVO Freya 2020.18) does not require any of these manual configuration steps :ref:`users_configuration`



XiVO comes with a WebRTC lines support, you can use in with XiVO *UC Assistant* and *Desktop Assistant*.
Before starting, please check the :ref:`webrtc_requirements`.

Former WebRTC implementation might require following configuration steps:

* configure :ref:`asterisk_http_server`,
* and create user :ref:`with one line configured for WebRTC <configure_user_with_webrtc_line>`. To have user with both SIP and WebRTC line is not supported.

.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Edit the line created and, in the *Advanced* tab, add `webrtc=yes` options:

.. figure:: webrtc_line.png
    :scale: 100%

Fallback Configuration
======================

When the user is not connected to its WebRTC line, or disconnect from the assistant, you can route the call to a default number
as for example the user mobile number.
Update the fail option on the No Answer user tab configuration, and add an extension to the appropriate context.

.. figure:: user_fail.png
    :scale: 100%

.. _configure_webrtc_video:

Experimental video call feature
===============================

  .. important:: **Removed since 2021.08**

Manual configuration of user with WebRTC line
==============================================

For the records 

.. toctree::
    
    webrtc_manualconf


Updating LineConfig Sip response
==============================================

Since Helios 17, we added a new field `sipPort` inside LineConfig Sip response to avoid DNS requests

Before::

     "msgType": "LineConfig",
     "ctiMessage": {
        "hasDevice": false,
        "id": "1",
        "isUa": false,
        "mobileApp": true,
        "name": "l1ze3zd8",
        "number": "4000",
        "password": "e2xyubvn",
        "sipProxyName": "default",
        "vendor": null,
        "webRtc": true,
        "xivoIp": "192.168.56.3"
    }

After::

     "msgType": "LineConfig",
     "ctiMessage": {
        "hasDevice": false,
        "id": "1",
        "isUa": false,
        "mobileApp": true,
        "name": "l1ze3zd8",
        "number": "4000",
        "password": "e2xyubvn",
        "sipProxyName": "default",
        "sipPort": "5060", <== New Field
        "vendor": null,
        "webRtc": true,
        "xivoIp": "192.168.56.3"
    }
