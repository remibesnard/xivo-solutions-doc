.. _outgoing_calls:

**************
Outgoing Calls
**************

You can configure outgoing calls settings in :menuselection:`Services --> IPBX --> Call Management --> Outgoing calls`.

An outgoing call is composed with a **definition** of a outgoing route (the extension patterns that should match) and a **destination** (a trunk owned by a media server).

.. figure:: images/outgoing_route.png
   :align: center
   :scale: 80%

Definition
==========

A route can define

* :guilabel:`Priority`: A number that will prioritize a route compare to another one, if both routes are available for a same matching extension.
* :guilabel:`Call Pattern`:

  * :guilabel:`Extension`: The pattern that will match an extension. More details on `the Asterisk wiki <https://wiki.asterisk.org/wiki/display/AST/Pattern+Matching>`_.
  * (advanced) :guilabel:`RegExp`: A pattern that will match the dialed number - see :ref:`outgoing_calls-dialednumxform` below.
  * (advanced) :guilabel:`Target`: The transformation to apply to the dialed number - see :ref:`outgoing_calls-dialednumxform` below.
  * (advanced) :guilabel:`Caller ID`: Override the presented number once call is performed.

* :guilabel:`Internal`: set the route to forward the internal caller’s caller ID to the trunk. This option is useful when the other side of the trunk can reach the user with it’s caller ID number.
* :guilabel:`Media server`: define the route only for specific media server.
* :guilabel:`Context`: define the route only for specific context.

.. _outgoing_calls-dialednumxform:

Dialed Number Transformation
============================

The fields :guilabel:`RegExp` and :guilabel:`Target` can be used to change the dialed number before it is sent via the trunk defined in the *Route*.

Basically one uses:

* the :guilabel:`RegExp` field to define a pattern that will match the dialed number
* and the :guilabel:`Target` field to define how to transform the dialed number

Below are some examples of how you can take advantage of **RegExp** and **Target**:

Add or Remove Prefix
--------------------

========== ======== =======================================================================================================================
 Regexp    Target                                                          Result
========== ======== =======================================================================================================================
0(.*)      \\1      Delete prefix 0 from dialed number and keep the rest (e.g. 00123456789 → 0123456789)
14(.*)..   \\1      Delete prefix 14 and last two digits from dialed number and keep the rest (e.g. 1455660 → 556)
.{1}(.*)   \\1      Delete leading digit from dialed number and keep the rest (e.g. 03601 → 3601)
+33(.*)    0\\1     Replcae leading +33 with 0 from dialed number and keep the rest (e.g. +33123456789 → 0123456789)
0(.*)      +33\\1   Replace leading 0 with +33 from dialed number with 33 (e.g. 0123456789 → +33123456789)
14(.*)..   33\\18   Delete prefix 14 and last two digits, and then prefix the dialed number with 33 and suffix it with 8 (1455660 → 335568)
========== ======== =======================================================================================================================


Change the Number
-----------------

========== ======== ======================================================================================================================
 Regexp    Target                                                          Result
========== ======== ======================================================================================================================
(.*)       \\1      Get whole called number (e.g. 0123456789 → 0123456789)
(.*)       445      Replace called number with 445 number (e.g. 0123456789 → 445)
========== ======== ======================================================================================================================


Destination
===========

Once route is defined and therefore can be selected if an extension match the route, you need to set the wanted destination.
A route destination is materialized by:

* **Trunk**: SIP or Custom trunk that will be used to reach desired network.
* **Subroutine**: Subroutine to apply once route has been selected.


Rights and schedules
====================

A route can define **rights** aka call permissions, it means that a call can be discarded if missing the right to use this route.
The same applies for **schedules** where you can define time slots of availability of the route.
