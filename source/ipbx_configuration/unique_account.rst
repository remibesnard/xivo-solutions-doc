.. _unique_account:

**************
Unique Account
**************

The *Unique Account* feature is a new feature introduced in Freya (2020.18) LTS version.

This feature enables a user to use, with the same phone number, its Deskphone or UC Assistant (using WebRTC).

Description
===========

.. important:: Prerequisites:

    * have the :ref:`webrtc_requirements` configured
    * and to use UA user on MDS follow the :ref:`xds_webrtc-configuration` section

A *Unique Account* user can receive calls either on its deskphone or on its UC Assistant (via WebRTC) - check the :ref:`unique_account_features` section.

To configure a *Unique Account* user a XiVO Administrator must follow the :ref:`unique_account_conf` section.

See also the :ref:`unique_account_usersguide` for more details.


.. _unique_account_usersguide:

User's guide
============


Connection to UC Assistant
--------------------------

A *Unique Account* user can connect to the UC Assistant.
When connected, the user will be connected as a WebRTC user.

#. Go to https://xivocc/ucassistant
#. Login with CTI credentials of UA user
#. The user is logged in with its WebRTC account
#. Then:

  * All calls towards your user's extension will be sent to your UC Assistant
  * You can also place internal our outgoing calls with your UC Assistant

.. note:: When connecting for the first time, the user will be using its WebRTC account.
    He can then chose the device he wants to use using the :ref:`unique_account_device_selection` menu.


.. _unique_account_device_selection:

Device selection
----------------

.. figure:: images/ua_toggle_menu.png
   :scale: 85%

The user can see which device he's using in the menu ``Call Management`` shown just above.
In this menu he can change back his device to deskphone. That means the calls emitted or received will be
on the deskphone and the user can only control the deskphone with the UC Assistant.

.. note:: The Device Selection is saved for a user.

Call history
------------

Once you are connected to UC assistant you are able to see mixed history of your
deskphone and your calls made through webrtc.

.. note:: If someone is making a call with deskphone while you are connected to UC assistant, the call will be also displayed in call history.


.. _unique_account_features:

Features
========

Given a user Alice configured as a *Unique Account* user with an internal phone number.
Given Alice's account is provisionned on a deskphone.

Use UC Assistant
----------------

If Alice **logs in** UC Assistant, then:

* Alice can use her UC Assistant
* Alice can call people from her UC Assistant
* when someone calls Alice (through its internal extension or DID number if she has one), her UC Assistant rings

Use Deskphone
-------------

If Alice *switches device* from the ``Call Management`` menu, nd chose the *Deskphone* then:

* Alice can call people from her Deskphone
* when someone calls Alice (through its internal extension or DID number if she has one), her Deskphone rings.

Fallback
--------

If Alice *logs out* from UC Assistant, then all call will be received on her *Deskphone*.
Even if she had chosen the *Browser/XiVO Client* as device, when she logs out from UC Assistant, the call will fallback to her *Deskphone*.


Presence
--------

Alice's presence is seen by her colleague whether she uses her deskphone or UC Assistant:

* When Alice is logged out of UC Assistant then

  * presence shown to other users is the status of the Deskphone
* When Alice is logged in the UC Assistant then

  * presence shown to other users is the status of the device Alice selected
  * status of the WebRTC line takes precedence over the Deskphone status

    * unless Alice emits a call with its Deskphone while logged in (then status will be In Use)


History
-------

When Alice is logged in UC Assistant, then she sees history of both calls received/emitted via her deskphone and UC Assistant.


Voicemail
---------

Alice can check its voicemail with its Deskphone and UC Assistant.

No Answer and Forwarding
------------------------

For a *Unique Account* user, the forwarding rules are applied to the device the user is using.

For example if the user is connected to the UC Assistant using its WebRTC line, and if he doesn't answer a call,
the non answer rule is directly applied without making the deskphone ring.


Limitations
-----------

* If a *Unique Account* user logs in UC Assistant and then quits the page (without logging out),
  **it can take few minutes before the next call will ring on its deskphone**
  (the WebRTC line gets unregistered, but after a delay depending on XiVO configuration).


Not supported features
----------------------

These XiVO features **don't work** with a *Unique Account* user:

* **Paging**: a *Unique Account* user cannot be part of a paging
* **Contact Center**

  * CC Agent: a *Unique Account* user cannot be an agent.
* **Switchboard**: a *Unique Account* user can't be used for a Switchboard agent.

.. _unique_account_conf:

Configuration
=============

To enable the *Unique Account* feature for a user:

#. Create a user
#. Go to the *Lines* tab and select the **Unique Account** line type
#. Save the form

.. figure:: ua_line_type.png

The *Unique Account* feature is activated for this user.

.. note:: You can also create users via :ref:`user_import`.


