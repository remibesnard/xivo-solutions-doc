**********************
Upgrade Jabbah to Kuma
**********************

In this section is listed the manual steps to do when migrating from Jabbah to Kuma.

.. contents::

.. warning:: Upgrade to Kuma:

    * Postgres database will be updated from 11 to version 15 during upgrade.
    * Asterisk will be updated from 18 to version 20 during upgrade.

Before Upgrade
==============

On XiVO PBX
-----------

.. important:: **Behavior change** If you come from a version lower than 2022.05.10 this upgrade will take more time to REINDEX the asterisk database.
    
    Examples:
    - with 1 000 000 cel and 75 000 queue_log and 50 000 call_log it takes **7 sec** (on a high performance disk Read:400MB/s, Write:500MB/s)
    - with 3 600 000 cel and 15 000 000 queue_log and 4 000 000 call_log it takes **22 min** (on a *low* performance disk - Read:80MB/s, Write:80MB/s)
  

UC Addon
^^^^^^^^

During upgrade, ``pgxivocc`` container will be dropped.
Data from the following databases will be dropped if not backuped before the upgrade:

* spagobi: you should backup it if you added custom spagobi reports to your UC Addon install
* recording: you should backup it if you added recording to your UC Addon install
* xivo_stats: history will be recomputed after upgrade

On XiVO CC
----------


After Upgrade
=============

On XIVO PBX
-----------

* Postgres was upgraded from version 11 to version 15. During the upgrade the following postgres 11 configuration files were restored
  in the postgres 15 folder:

  * :file:`pg_hba.conf`
  * and :file:`conf.d/*.conf` files
  * The rest of the postgres 11 configuration was saved in this directory :file:`/var/tmp/xivo-migrate-db-11-to-15/postgresql-11-conf-backup`.
    You might want to compare its content with what is now for postgres 15 (in :file:`/var/lib/postgresql/15/data`).


UC Addon
^^^^^^^^

* pgxivocc and db_replic containers were removed
  to finish the migration you MUST run::
            
    xivo-dcomp up -d --remove-orphans
    xivocc-dcomp up -d --remove-orphans
* Call history was cleaned: xivo_stats will  
  recompute it. You can check the progress with
  the following command::

    psql -U asterisk -qc '
    SELECT
        x.max_cel_id,
        y.last_cel_id AS last_cel_processed,
        x.max_cel_id - y.last_cel_id
          AS Nb_cel_to_process
    FROM
      (SELECT max(id) AS max_cel_id FROM cel) x
      NATURAL FULL JOIN
      (SELECT id AS last_cel_id FROM last_cel_id) y'
* Chat history was restored (if it is installed):
  you can check xivo-upgrade log to verify if 
  everything went well.
* When you checked everything is ok, you should
  clean pgxivocc data::
            
    rm -rf /var/lib/postgresql/xivouc/     


On XiVO CC
----------

.. important:: **Database** after the upgrade you MUST upgrade pgxivocc from 11 to version 15.

    To do this, launch the script (it will restart the pgxivocc container).
    On XiVO CC::

       xivocc-migrate-db-11-to-15

    Or on XiVO with UC addon::

       xivouc-migrate-db-11-to-15

    If you are installing the chat backend, you must upgrade pgxivocc first and then start the chat backend installation.


* The configuration to enable webrtc on mds has been automated. If you have this file
  `/etc/docker/nginx/sip_proxy/sip_proxy.conf`, you can check that its content is the same 
  as the now auto-generated file on the container with :

   .. code-block:: bash

    xivocc-dcomp exec nginx cat /etc/nginx/sip_proxy/sip_proxy.conf

  You should remove it as it is no more mounted.

   .. code-block:: bash

    rm /etc/docker/nginx/sip_proxy/sip_proxy.conf
