**********************
Upgrade Helios to Izar
**********************

.. contents:: :local:

.. warning:: For Izar Debian was upgraded to Debian 11 (Bullseye).

  Therefore:

      * the upgrade to Izar will take longer than usual
      * upgrade from older version than XiVO Freya (2020.18) are not supported (you need first to upgrade
        to a version above or equal to Freya before being able to upgrade to Izar).

    Please read carefully :ref:`Debian 11 (Bullseye) upgrade notes <debian_11_upgrade_notes>` page.

    Note also that upgrade to Debian 11 on MDS and XiVO CC is manual (see manual procedure below).

.. warning::
    For Izar asterisk SIP channel driver was switched from ``chan_sip`` to ``pjsip``.

    **Please read carefully the** :ref:`upgrade_chan_sip_pjsip_migration_guide`.

Before Upgrade
==============

On XiVO PBX
-----------

.. _izar_py3_plugins:

Python3 migration: Phone Device Plugins
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. important:: In short, you must check that the plugins installed on the XiVO are present in the new python3 compatible repo::

       http://provd.xivo.solutions/plugins/2/stable

     If not you will need some additional checks before upgrading.

All xivo python services were switched to python3, including ``xivo-provd`` our provisioning server.
It makes all python2 provisioning plugins incompatible.

During upgrade:

* the provd plugin repository will be automatically switched from the python2 compatible repo to the python3 compatible repo

  * switched

    * from http://provd.xivo.solutions/plugins/1/stable
    * to http://provd.xivo.solutions/plugins/2/stable
  * more precisely

    * from ``<whatever>/1/<whatever>``
    * to ``<whatever>/2/<whatever>``
* and then it will try to upgrade the currenlty installed plugins on your XiVO to the plugins present on the python3 compatible provd repo

But you have to know that:

#. In the http://provd.xivo.solutions/plugins/2/stable repository only Yealink (v85) and Snom (v10) plugins were kept (i.e. the officialy supported phone brands)
#. The rest of the plugins were put in the http://provd.xivo.solutions/plugins/2/addons/ repository:

   * those in ``addons/stable`` are migrated to python3 and were tested
   * those in ``addons/testing`` were automatically migrated to python3 **but not tested**

**Therefore**, if the plugin installed by your XiVO:

* is present in ``plugins/2/stable``: then everything is automatic
* is present in ``plugins/2/addons/stable``: then you will need an additional manual step after the migration to configure this repo and update you're installed plugin from this repo
* is present in ``plugins/2/addons/testing``: you should test this plugin on a lab on Izar version before doing the upgrade. If you find some problems, please fix them and open a Merge request on the xivo-provd-plugins-addons_ repository

.. _xivo-provd-plugins-addons: https://gitlab.com/xivo.solutions/xivo-provd-plugins-addons

.. _izar_py3_scripts:

Python3 migration: Scripts and AGI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. important:: In short, you must check that your custom AGI are compatible with python3.

All xivo python services were switched to python3, including ``xivo-agid`` our AGI server.
It makes all python2 custom AGI incompatible.

You must update your custom AGI from python2 to python3 before upgradeing your XiVO.

It is also true of all python scritps that would import one of ``xivo-*`` python libs (like ``xivo-confd-client`` or ``xivo-lib-python``).


On XiVO CC
----------

N.A.

On MDS
------

N.A.

After Upgrade
=============

On XIVO PBX
-----------

Manual steps to follow after xivo-ctid containerization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Backend certificate re-generation**: if you had followed the :ref:`cert_change_xivonbackend_cert` procedure, then **you MUST regenerate**
the self-signed certificate used for the backend services:

#. Check if the backend certificate is self-signed

   .. code-block:: bash

     (openssl x509 -in /usr/share/xivo-certs/server.crt -noout -issuer | grep -wq Avencall) && echo "Certificate is self-signed" || echo "Certificate is not self-signed. You MUST re-generate the backend certificate"
#. If it is **not** self-signed, follow the :ref:`regenerating_certificate` section to **regerate a self-signed** certificate.
   Otherwise, if it is self-signed, you can skip the rest of this procedure.
#. Then remove the certificate configuration for xivo services:

   .. code-block:: bash

     rm -rf /etc/xivo/custom
     for config_dir in /etc/xivo-*/conf.d/ ; do
       rm "$config_dir/010-custom-certificate.yml"
     done
#. Clean custom-templates:

   #. Remove the FQDN from the `127.0.0.1` line in:

      * the sysconfd custom template: :file:`/etc/xivo/sysconfd/custom-templates/resolvconf/hosts`
      * the xivo custom template: :file:`/etc/xivo/custom-templates/system/etc/hosts`
   #. And update the configuration:

      .. code-block:: bash

        xivo-update-config
#. Also, you must replace the FQDN, in the definition of your directories in the web interface under :menuselection:`Configuration --> Directories`, by ``localhost``.
#. Then, when done, you must re-save, the CTI Directories definition:

   * Go to :menuselection:`Services --> CTI Server --> Directories --> Definitions`
   * Edit each directory to re-select the new URI
   * And save it
#. And restart all the services:

   .. code-block:: bash

     xivo-service restart all


**Removing xivo-ctid debian package leftovers**: some lefovers of the xivo-ctid debian package should be cleaned manually.
You may want to backup these files before cleaning them.

.. code-block:: bash

  # Clean config dir which is not used anymore
  rm -rf /etc/xivo-ctid
  # Clean old logs
  rm -rf /var/log/xivo-ctid.log*

Recording on Gateway
^^^^^^^^^^^^^^^^^^^^

If you're using the :ref:`recording_gw_configuration` feature, you **MUST** update the recording dialplan with the one provided in the Izar documentation:

#. Update recording dialplan on Gateway according to :ref:`Gateway recording on GW dialplan <recording_gw_configuration_gwdp>`
#. Update recording dialplan on XiVO PBX according to :ref:`XiVO PBX recording on GW dialplan <recording_gw_configuration_xivopbxdp>`


On XiVO CC
----------

* Upgrade system to Debian 11 (Bullseye) with the :ref:`following manual procedure <upgrade_bullseye>`:

On MDS
------


On Edge
-------

* Upgrade system to Debian 11 (Bullseye):

  #. Rewrite the apt preferences for docker-ce

     .. code-block:: bash

      #set docker-ce preferences
      cat > /etc/apt/preferences.d/docker-ce << EOF
      Package: docker-ce*
      Pin: version 5:20.10.13*
      Pin-Priority: 1000
      EOF

  #. and then follow the :ref:`Debian 11 upgrade manual procedure <upgrade_bullseye>`:

On Meeting Rooms
----------------

* Upgrade system to Debian 11 (Bullseye):

  #. Rewrite the apt preferences for docker-ce

     .. code-block:: bash

      #set docker-ce preferences
      cat > /etc/apt/preferences.d/docker-ce << EOF
      Package: docker-ce*
      Pin: version 5:20.10.13*
      Pin-Priority: 1000
      EOF

  #. and then follow the :ref:`Debian 11 upgrade manual procedure <upgrade_bullseye>`:

.. _upgrade_bullseye:

Upgrade to Debian11
-------------------

* These steps are to be done after an upgrade from Helios to Izar on **XiVO CC**, **Edge** and **Meeting Rooms**:

  #. Check GRUB **before upgrading**

     .. code-block:: bash

      install_device=$(debconf-show grub-pc | grep 'grub-pc/install_devices:' | cut -b3- | cut -f2 -d' ' | cut -d',' -f1)
      if [ "$install_device" -a ! -e "$install_device" ]; then
        echo -e "\e[1;31mYou must install GRUB BEFORE upgrading\e[0m"
      fi

     If it's broken you can fix it this way before rechecking

     .. code-block:: bash

      apt update
      apt install grub-pc
      dpkg-reconfigure grub-pc

  #. Upgrade to Debian11

     .. code-block:: bash

      # Move to bullseye
      sed -i 's/stretch/bullseye/' /etc/apt/sources.list /etc/apt/sources.list.d/*.list
      sed -i 's/buster/bullseye/' /etc/apt/sources.list /etc/apt/sources.list.d/*.list
      sed -i 's/bullseye\/updates/bullseye-security/' /etc/apt/sources.list
      apt update

      export DEBIAN_FRONTEND=noninteractive
      export APT_LISTCHANGES_FRONTEND=none
      force_yes="--allow-downgrades --allow-remove-essential --allow-change-held-packages"

      echo "Download packages..."
      apt full-upgrade -d --yes
      echo "Executing full upgrade actions..."
      apt full-upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      apt autoremove --yes

  #. Reboot
