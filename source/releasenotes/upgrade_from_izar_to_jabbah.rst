**********************
Upgrade Izar to Jabbah
**********************

Before Upgrade
==============

On XiVO PBX
-----------

*  If meetingroom server is installed, update ``MEETINGROOM_AUTH_DOMAIN`` value to ``meet.jitsi`` in :file:`/etc/docker/xivo/custom.env`
