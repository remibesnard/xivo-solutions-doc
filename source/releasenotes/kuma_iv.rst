*********************************
XiVO Kuma Intermediate Versions
*********************************

2023.04.00 (IV Kuma)
--------------------

Consult the `2023.04.00 (IV Kuma) Roadmap <https://projects.xivo.solutions/versions/419>`_.

Components updated: 

Docker :

edge-nginx,pgxivocc,xivo-db,xivo-web-interface,xucserver

Debian :

gatlingxuc,xivo-backup,xivo-confd,xivo-config,xivo-install-script,xivo-manage-db,xivo-upgrade,xivo-utils,xivocc-installer

**Asterisk**

* `#5973 <https://projects.xivo.solutions/issues/5973>`_ - Upgrade from asterisk 18 to asterisk 20

**Reporting**

* `#6433 <https://projects.xivo.solutions/issues/6433>`_ - [S] Postgresql JDBC - Vulnerability analysis - CERTFR-2022-AVI-1054

**XUC Server**

* `#6370 <https://projects.xivo.solutions/issues/6370>`_ - Add an endpoint to authenticate a web service user
* `#6372 <https://projects.xivo.solutions/issues/6372>`_ - Check and renew web service user JWT token
* `#6388 <https://projects.xivo.solutions/issues/6388>`_ - upgrade gatlingxuc endpoints to login agents
* `#6403 <https://projects.xivo.solutions/issues/6403>`_ - Add a scenario for agent logins in gatlingxuc
* `#6442 <https://projects.xivo.solutions/issues/6442>`_ - Create new dialToQueue REST API
* `#6448 <https://projects.xivo.solutions/issues/6448>`_ - Web service token is not created if accesswebservice contains user with no passwd

**XiVO PBX**

* `#6099 <https://projects.xivo.solutions/issues/6099>`_ - Force wget to use ipV4 instead of ipV6
* `#6202 <https://projects.xivo.solutions/issues/6202>`_ - [Doc] Boss Secretay Filter not working when switching user state from boss to secretaty
* `#6213 <https://projects.xivo.solutions/issues/6213>`_ - Restore data backup
* `#6320 <https://projects.xivo.solutions/issues/6320>`_ - Add additional registrations on the load
* `#6378 <https://projects.xivo.solutions/issues/6378>`_ - Add securisation for web service users
* `#6381 <https://projects.xivo.solutions/issues/6381>`_ - [Doc] UCAddon - wrong debian version in doc
* `#6384 <https://projects.xivo.solutions/issues/6384>`_ - Remove xivocc_pgxivocc container from xivo-webi in a UC installation
* `#6400 <https://projects.xivo.solutions/issues/6400>`_ - XiVO / MDS - Upgrade PostgreSQL from 11 to 15 on XiVO

**XiVOCC Infra**

* `#5974 <https://projects.xivo.solutions/issues/5974>`_ - XiVO CC - Upgrade PostgreSQL from 11 to 15
* `#6373 <https://projects.xivo.solutions/issues/6373>`_ - Audio converter - add it to the build
* `#6401 <https://projects.xivo.solutions/issues/6401>`_ - Upgrade PostgreSQL from 11 to 15

**edge**

* `#6379 <https://projects.xivo.solutions/issues/6379>`_ - Add API v2 routes to Edge
* `#6389 <https://projects.xivo.solutions/issues/6389>`_ - [Doc] Edge - missing doc step to add RECORDING_SERVER_HOST in CC


2023.03.00 (IV Kuma)
--------------------

Consult the `2023.03.00 (IV Kuma) Roadmap <https://projects.xivo.solutions/versions/417>`_.

Components updated: 

Docker :

pack-reporting,recording-server,xivo-db,xivo-full-stats,xivo-web-interface,xucmgt,xucserver

Debian :

asterisk,xivo-confd,xivo-liquibase,xivo-provd-plugins,xivo-upgrade,xivocc-installer

**Asterisk**

* `#6348 <https://projects.xivo.solutions/issues/6348>`_ - Refresh build/patches for asterisk 20.2.0
* `#6363 <https://projects.xivo.solutions/issues/6363>`_ - Build asterisk 20.02 for Deb11 (Kuma)

**Reporting**

* `#6204 <https://projects.xivo.solutions/issues/6204>`_ - Remove pgxivocc from XiVOUC Addon for clean install
* `#6205 <https://projects.xivo.solutions/issues/6205>`_ - Handle upgrade from pgxivocc to db
* `#6214 <https://projects.xivo.solutions/issues/6214>`_ - Plug fullstat on XiVO db container and asterisk database and ensure purge is still working
* `#6296 <https://projects.xivo.solutions/issues/6296>`_ - Duplicates in stat_agent_periodic when recompiling from db_replic

**Web Assistant**

* `#6344 <https://projects.xivo.solutions/issues/6344>`_ - UC assistant- Remove the star of favorites for users without contact-id 

**XUC Server**

* `#6366 <https://projects.xivo.solutions/issues/6366>`_ - Add handling of multiple OIDC keycloak for xuc 

**XiVO PBX**

* `#4467 <https://projects.xivo.solutions/issues/4467>`_ - Web-I - CSV Import should basically ignore provisioning_code colum from a CSV export
* `#5091 <https://projects.xivo.solutions/issues/5091>`_ - As an admin I want to be able to override the XiVO container configuration
* `#6326 <https://projects.xivo.solutions/issues/6326>`_ - Add install of mattermost in the daily-tests job
* `#6333 <https://projects.xivo.solutions/issues/6333>`_ - Webi - Clarify schedule naming
* `#6335 <https://projects.xivo.solutions/issues/6335>`_ - Confd api for user import with csv file is broken
* `#6368 <https://projects.xivo.solutions/issues/6368>`_ - (fix release )Since PJSIP, Asterisk charts in WebI are not rendered properly

**XiVO Provisioning**

* `#6273 <https://projects.xivo.solutions/issues/6273>`_ - Plugins can't be easilly download as they point to HTTP repository instead HTTPS
* `#6322 <https://projects.xivo.solutions/issues/6322>`_ - Add new mac address OUI 24:9A:D8 for yealink

**XiVOCC Infra**

* `#6156 <https://projects.xivo.solutions/issues/6156>`_ - As an admin I want to only have one db on UC Addon in order to have a more efficient installation



2023.02.00 (IV Kuma)
--------------------

Consult the `2023.02.00 (IV Kuma) Roadmap <https://projects.xivo.solutions/versions/416>`_.

Components updated: 

Docker :

xivo-agid,xivo-web-interface,xucmgt

Debian :

asterisk,xivo-agid,xivo-upgrade

**Asterisk**

* `#5618 <https://projects.xivo.solutions/issues/5618>`_ - Update asterisk to 18.15.1 - deb11

**Desktop Assistant**

* `#6247 <https://projects.xivo.solutions/issues/6247>`_ - Autologin by token seems broken

**Web Assistant**

* `#6277 <https://projects.xivo.solutions/issues/6277>`_ - UC assistant  - Search results are not all displayed on frontend side 

**XUC Server**

* `#6318 <https://projects.xivo.solutions/issues/6318>`_ - [Doc] Agent on pause is set back to ready status after refreshing page 
* `#6319 <https://projects.xivo.solutions/issues/6319>`_ - [Doc] Roaming agent does not work with 2 webrtc lines - Relogging a wertc agent with default line on other webrtc line fails

**XiVO PBX**

* `#6261 <https://projects.xivo.solutions/issues/6261>`_ - Secure xivo-upgrade loging off agents prior to launch real upgrade
* `#6264 <https://projects.xivo.solutions/issues/6264>`_ - Upgrade from Izar to Jabbah breaks MDS replication for extensions table

  .. important:: **Behavior change** If you come from a version lower than 2022.05.10 this upgrade will take more time to REINDEX the asterisk database.
    
    Examples:
    - with 1 000 000 cel and 75 000 queue_log and 50 000 call_log it takes **7 sec** (on a high performance disk Read:400MB/s, Write:500MB/s)
    - with 3 600 000 cel and 15 000 000 queue_log and 4 000 000 call_log it takes **22 min** (on a *low* performance disk - Read:80MB/s, Write:80MB/s)

* `#6269 <https://projects.xivo.solutions/issues/6269>`_ - xivo-agid RAM over-consumption
* `#6279 <https://projects.xivo.solutions/issues/6279>`_ - I can't click on the + button to add a user to a group


2023.01.00 (IV Kuma)
--------------------

Consult the `2023.01.00 (IV Kuma) Roadmap <https://projects.xivo.solutions/versions/410>`_.

Components updated: 

Docker :

edge-kamailio,ivr-editor,xivo-usage-writer,xivo-web-interface,xivo-web-jitsi,xucserver

Debian :

xivocc-installer

**Reporting**

* `#6203 <https://projects.xivo.solutions/issues/6203>`_ - Make fullstat db configurable

**Usage statistics**

* `#6165 <https://projects.xivo.solutions/issues/6165>`_ - configure usm telegraph to send data to a remote influxdb server
* `#6166 <https://projects.xivo.solutions/issues/6166>`_ - create usm backend with influxdb

**Visioconf**

* `#6179 <https://projects.xivo.solutions/issues/6179>`_ - When I hide self view there's no way to reactivate

**XUC Server**

* `#6177 <https://projects.xivo.solutions/issues/6177>`_ - If a user logs in with the XiVO Client (!) and change its forward/dnd it breaks dnd/forward status on UC assistant

**XiVO PBX**

* `#4957 <https://projects.xivo.solutions/issues/4957>`_ - SVI / IVR : API that allows the integration of several mass audio files for the IVR 
* `#6150 <https://projects.xivo.solutions/issues/6150>`_ - [Web-i] - Remove the ...annoying... tooltip in provisioning/general/ that leads to misconfiguration of provd
* `#6190 <https://projects.xivo.solutions/issues/6190>`_ - [Doc] More explicite command and 1 more requirements 

**edge**

* `#6195 <https://projects.xivo.solutions/issues/6195>`_ - [S] Calls can be sometimes intercepted by a third party user if mobile is not reachable

