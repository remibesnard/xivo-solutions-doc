.. _xivosolutions_release:

*************
Release Notes
*************

.. _kuma_release:

Kuma
======

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Jabbah (2022.10).

New Features
------------

**XiVO PBX**

* The web service configuration page has evolved. The password field is now hidden, and a strong password policy was added for security safety.
  You also have the option to generate a random password fitting the newly required policy. 
* The password generator button was also added to the cti password field, in user edition.

**XiVO UC Addon**

* :ref:`xivouc_addon` environment was reworked to have only one database container (see :ref:`xivouc_addon_archi` schema).
  So now the pgxivocc and the db_replic containers are removed on this install type.
  We added a new variable ``UC=yes`` in files :file:`/etc/docker/compose/custom.env` and
  :file:`/etc/docker/xivo/custom.env` if we are in a UC Addon environment.
  Also the file :file:`/var/lib/xivo/xc_enabled` was replace to :file:`/var/lib/xivo/uc_enabled` on UC Addon environment.

**API**

* New API are available through webservice users (which are different from cti users) with custom ACL.

**System**

* Upgrade to asterisk 20 the latest LTS version of asterisk
* Upgrade to postgresql 15 the latest release of postgres
* Ease of installation process
* Anonymous usage data collection

Behavior Changes
----------------

**XiVO PBX**

* You will need to match the new password policy when creating web service users. (mandatory)

Deprecations
------------

This release deprecates:

* `LTS Electra (2020.07) <https://documentation.xivo.solutions/en/2020.07/>`_ : This version is no longer supported.
 
Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia
   upgrade_from_gaia_to_helios
   upgrade_from_helios_to_izar
   upgrade_from_izar_to_jabbah
   upgrade_from_jabbah_to_kuma

**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Kuma Bugfixes Versions
========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2023.05.02     |
+----------------------+----------------+
| config_mgt           | 2023.05.00     |
+----------------------+----------------+
| db                   | 2023.05.00     |
+----------------------+----------------+
| outcall              | 2023.05.00     |
+----------------------+----------------+
| db_replic            | 2023.05.00     |
+----------------------+----------------+
| nginx                | 2023.05.00     |
+----------------------+----------------+
| webi                 | 2023.05.02     |
+----------------------+----------------+
| switchboard_reports  | 2023.05.00     |
+----------------------+----------------+
| usage_writer         | 2023.05.02     |
+----------------------+----------------+
| usage_collector      | 2023.05.02     |
+----------------------+----------------+
| asterisk             | 20.2           |
+----------------------+----------------+
| docker-ce            | 5:20.10.13     |
+----------------------+----------------+
| docker-compose       | 1.29.2         |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        |  7.14.0        |
+----------------------+----------------+
| kibana               |  7.14.0        |
+----------------------+----------------+
| logstash             | 2023.05.00     |
+----------------------+----------------+
| mattermost           | 2023.05.00     |
+----------------------+----------------+
| nginx                | 2023.05.00     |
+----------------------+----------------+
| pack-reporting       | 2023.05.00     |
+----------------------+----------------+
| pgxivocc             | 2023.05.00     |
+----------------------+----------------+
| recording-rsync      | 2023.05.00     |
+----------------------+----------------+
| recording-server     | 2023.05.00     |
+----------------------+----------------+
| spagobi              | 2023.05.00     |
+----------------------+----------------+
| xivo-full-stats      | 2023.05.00     |
+----------------------+----------------+
| xuc                  | 2023.05.02     |
+----------------------+----------------+
| xucmgt               | 2023.05.02     |
+----------------------+----------------+
| **Edge**             | 2023.05.00     |
+----------------------+----------------+
| edge                 | 2023.05.00     |
+----------------------+----------------+
| nginx                | 2023.05.02     |
+----------------------+----------------+
| kamailio             | 2023.05.00     |
+----------------------+----------------+
| coturn               | 2023.05.00     |
+----------------------+----------------+
| **Meeting Rooms**    | 2023.05.00     |
+----------------------+----------------+
| meetingroom          | 2023.05.00     |
+----------------------+----------------+
| web-jitsi            | 2023.05.00     |
+----------------------+----------------+
| jicofo-jitsi         | 2023.05.00     |
+----------------------+----------------+
| prosody-jitsi        | 2023.05.00     |
+----------------------+----------------+
| jvb-jitsi            | 2023.05.00     |
+----------------------+----------------+
| jigasi-jitsi         | 2023.05.00     |
+----------------------+----------------+
| **IVR**              | 2023.05.00     |
+----------------------+----------------+
| ivr-editor           | 2023.05.00     |
+----------------------+----------------+

2023.05.02 (Kuma.02)
--------------------

Consult the `2023.05.02 (Kuma.02) Roadmap <https://projects.xivo.solutions/versions/435>`_.

Components updated: 

Docker :

edge-nginx,xivo-usage-writer,xivo-web-interface,xucmgt,xucserver

Debian :

xivo-config,xivo-install-script,xivo-upgrade,xivo-usage-collector,xivocc-installer

**CCManager**

* `#6457 <https://projects.xivo.solutions/issues/6457>`_ - Can't import or donwload callbacks from CCManager when used with Edge

**Desktop Assistant**

* `#6580 <https://projects.xivo.solutions/issues/6580>`_ - CCagent do not resize to correct size at startup of desktop assistant

**Usage statistics**

* `#5613 <https://projects.xivo.solutions/issues/5613>`_ - USM - Usage Report
* `#6520 <https://projects.xivo.solutions/issues/6520>`_ - Add xivo_uuid to the USM get token request
* `#6534 <https://projects.xivo.solutions/issues/6534>`_ - Automate USM token renewal
* `#6544 <https://projects.xivo.solutions/issues/6544>`_ - USM - add a monthly collection

**WebRTC**

* `#6360 <https://projects.xivo.solutions/issues/6360>`_ - [Doc] - Clarify Audio Quality issue computation
* `#6479 <https://projects.xivo.solutions/issues/6479>`_ - Packet loss warning does not work with Chrome 110 and higher

**XUC Server**

* `#6328 <https://projects.xivo.solutions/issues/6328>`_ - Secure REST API
* `#6434 <https://projects.xivo.solutions/issues/6434>`_ - Automatically update the repository of web service users with RabbitMQ
* `#6445 <https://projects.xivo.solutions/issues/6445>`_ - XUC REST API - Bump old 1.0 API to 2.0 with authentication
* `#6531 <https://projects.xivo.solutions/issues/6531>`_ - Webservice token is refreshed with AUTH_EXPIRES time when asking for renewal

**XiVO PBX**

* `#5951 <https://projects.xivo.solutions/issues/5951>`_ - Remove chan_sccp support from XiVO

**XiVOCC Infra**

* `#6526 <https://projects.xivo.solutions/issues/6526>`_ - The link between xuc and recording server is broken

2023.05.01 (Kuma.01)
--------------------

Consult the `2023.05.01 (Kuma.01) Roadmap <https://projects.xivo.solutions/versions/441>`_.

Components updated: 

Docker :

xivo-web-interface,xucserver

Debian :

xivo-confd

**XUC Server**

* `#6524 <https://projects.xivo.solutions/issues/6524>`_ - Cannot invite user to temporary meeting room

**XiVO PBX**

* `#6523 <https://projects.xivo.solutions/issues/6523>`_ - Confd regex dont authorize password with length of 4

2023.05.00 (Kuma.00)
--------------------

Consult the `2023.05.00 (Kuma.00) Roadmap <https://projects.xivo.solutions/versions/373>`_.

Components updated: 

Docker :

edge-nginx,xivo-db,xivo-outcall,xivo-usage-writer,xivo-web-interface,xivoxc-nginx,xucmgt,xucserver,xivocc-fluentd,xivo-grafana

Debian :

xivo,xivo-auth,xivo-confd,xivo-purge-db,xivo-service,xivo-upgrade,xivocc-installer

**Desktop Assistant**

* `#6474 <https://projects.xivo.solutions/issues/6474>`_ - DApp - CC agent application does not resize the DApp size (dapp enveloppe)

**Reporting**

* `#6259 <https://projects.xivo.solutions/issues/6259>`_ - Add purge mechanism for USM on XiVo main

**Usage statistics**

* `#6216 <https://projects.xivo.solutions/issues/6216>`_ - configure usm telegraph to send data to a remote influxdb server
* `#6478 <https://projects.xivo.solutions/issues/6478>`_ - Create USM VM

**Visioconf**

* `#6497 <https://projects.xivo.solutions/issues/6497>`_ - When in a meeting room, if I receive another meeting room invitation I can't quit my current MR to join the new invitation

**XUC Server**

* `#6375 <https://projects.xivo.solutions/issues/6375>`_ - Check user ACL

  .. important:: **Behavior change** Rest Api usage can now be restricted by ACL

* `#6451 <https://projects.xivo.solutions/issues/6451>`_ - Visualising call quality from xuc
* `#6452 <https://projects.xivo.solutions/issues/6452>`_ - Parsing compressed xuc logs into xivo_db
* `#6453 <https://projects.xivo.solutions/issues/6453>`_ - Create a separate project for fluentd
* `#6461 <https://projects.xivo.solutions/issues/6461>`_ - WebService - token is not recreated
* `#6487 <https://projects.xivo.solutions/issues/6487>`_ - Change some default values on xucserver application.conf
* `#6507 <https://projects.xivo.solutions/issues/6507>`_ - Fix the memory leak when a user is connecting to xuc

**XiVO PBX**

* `#6212 <https://projects.xivo.solutions/issues/6212>`_ - Doc how I can restore a db backup of LTS N on a xivo LTS N+6
* `#6374 <https://projects.xivo.solutions/issues/6374>`_ - Add default ACLs for CTI users
* `#6454 <https://projects.xivo.solutions/issues/6454>`_ - Create a separate project for grafana
* `#6455 <https://projects.xivo.solutions/issues/6455>`_ - Add purge mechanism to the call quality visualisation
* `#6458 <https://projects.xivo.solutions/issues/6458>`_ - UC Addon - mattermost db restore fails
* `#6488 <https://projects.xivo.solutions/issues/6488>`_ - Change some default values on xivo-outcall application.conf
* `#6496 <https://projects.xivo.solutions/issues/6496>`_ - Broken webi user import 

**XiVOCC Infra**

* `#4964 <https://projects.xivo.solutions/issues/4964>`_ - Automate the WebRTC configuration for XiVOCC
* `#6225 <https://projects.xivo.solutions/issues/6225>`_ - [Doc] MR for more informations on passing the wizard via JSON
* `#6290 <https://projects.xivo.solutions/issues/6290>`_ - [S] Elastic Kibana - CVE - several vulnerability
* `#6332 <https://projects.xivo.solutions/issues/6332>`_ - [SF] - XiVO Installation - provide minimal debugging & ops tools in base install
* `#6477 <https://projects.xivo.solutions/issues/6477>`_ - Remove excessive permissions in pgxivocc

**edge**

* `#6471 <https://projects.xivo.solutions/issues/6471>`_ - [S] - Edge - Call Qualification export API is accessible without authentication


Kuma Intermediate Versions
============================

.. toctree::
   :maxdepth: 2

   kuma_iv
