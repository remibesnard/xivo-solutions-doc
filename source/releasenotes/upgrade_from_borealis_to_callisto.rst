*****************************
Upgrade Borealis to Callisto
*****************************

In this section is listed the manual steps to do when migrating from Borealis to Callisto.

.. contents::

.. warning:: Upgrade to Callisto:

    * Postgres database will be updated from 9.4 to version 11 during upgrade.
    * Asterisk will be updated from 13 to version 16 during upgrade.
    * IAX trunks are no longer supported

Before Upgrade
==============

On XiVO PBX
-----------

* Postgres upgrade:

  * Your database MUST be in locale ``fr_FR.UTF-8`` or ``en_US.UTF-8``. Otherwise upgrade won't be possible.
    If your database is not in this locale you must first change it :ref:`as documented here <postgresql_localization_errors>`.
  * Old configuration parameters (e.g. in files :file:`/etc/postgresql/9.4/postgresql.conf` or
    or :file:`/etc/postgresql/9.4/pg_ha.conf`) will be saved in during the upgrade in folder
    :file:`/var/tmp/xivo-migrate-db-94-to-11/postgresql-94-conf-backup/` (see note After Upgrade below).


After Upgrade
=============

On XiVO PBX
-----------

* Outcall migration to Route:

  #. at the end of the upgrade check if there was any error during outcall migration to route::

      zgrep '\[MIGRATE_OUTCALL\].*WARNING' /var/log/postgresql/postgresql-11-main.log*
  #. if yes, follow our :ref:`migration guide <callisto_route_upgrade_guide>`.

* Postgresql: postgresql service now runs inside a container. During the upgrade the postgresql configuration
  **WAS NOT** migrated (i.e. parameters in file :file:`postgresql.conf` or connection authorizations in file :file:`pg_hba.conf`).
  But the old configuration was saved during upgrade in folder :file:`/var/tmp/xivo-migrate-db-94-to-11/postgresql-94-conf-backup/`
  
  * **postgresql.conf**: if you have any specific parameters you should add them to the postgresql container configuration as documented in the
    :ref:`database_configuration` section.

.. _restricting_database_access:

  * **pg_hba.conf**: specific authorization (for XiVO CC ...) **MUST** be added again to new :file:`pg_hba.conf` file
    (now located in :file:`/var/lib/postgresql/11/main/pg_hba.conf`). Follow the instrcutions below depending on your installation:

    * (XiVO CC) If you have XiVO CC installed, add this entry for xuc connection::

        host    asterisk      all      XIVOCC_IP_ADDRESS/32      md5

    * (XDS) Add entries for **every** Media Server::

        host    asterisk      all      MDS_IP_ADDRESS/32         md5

    * (High Availability) On slave XiVO, add entry for replication from master::

        host    asterisk    postgres   XIVO_MASTER_VOIP_IP/32   trust

    * (XiVO) If you changed the xivo docker network (in :file:`factory.env`), add this entry::

        host    all         all        XIVO_DOCKER_NETWORK/NETMASK  md5

    * **Apply the settings** by command ``xivo-dcomp reload-db``. It will not restart db container despite the message *"Killing xivo_db_1"*.

* High availibility: If you have XiVO CC installed, follow the :ref:`ha_interconnection_with_cc` procedure
  to install DB Replic on the slave XiVO.

* Asterisk HTTP server configuration **security warning**:

  .. warning:: **Security warning:** Asterisk HTTP server (used for WebRTC and xivo-outcall) was reconfigured
     to accept websocket connections from outside. See the :ref:`asterisk_http_server` documentation
     and ensure that the configuration is secure.

* XDS: if you had an XDS installation you MUST remove SIP trunks added for intra-mds routing
  (that is the SIP trunks named `default`, and `mdsX` - e.g. `mds0`, `mds1` ...).
  These trunks are now generated automatically.


On MDS
------

* dahdi-modules: after upgrade, remove old ``dahdi-linux-modules``,
  which were replaced by ``dahdi-linux-dkms``::

    apt-get purge '^dahdi-linux-modules*' -y

* Run ``xivo-remove-postgres-94`` after dist-upgrade to remove postgres 9.4.

* Postgresql: check :file:`/var/log/postgresql/postgresql-11-main.log` for specific upgrade steps
