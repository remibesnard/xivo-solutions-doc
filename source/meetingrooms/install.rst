.. _meetingrooms_install:

************************************
Meeting Rooms Installation & Upgrade
************************************

.. contents:: :local:

.. _meetingrooms_install_requirement:

Requirements
============

.. important:: The Meeting Rooms is an enterprise version feature. To enable it, please contact `XiVO team <https://www.xivo.solutions/contact/>`_.

.. important:: The Meeting Rooms components works only if you have the :ref:`xivo_edge`
    component installed and configured.

Server Requirements
-------------------

The Meeting Rooms components must be installed on a different server as the XiVO CC/UC server.

The minimal requirements would be:

* OS: **Debian 11** (Bullseye), 64 bits
* CPU: 4 CPU
* RAM: 8 Gb

See: https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-scalable#machine-sizing

.. _meetingrooms_install_base:

Base installation
=================

1. Docker & Docker compose Installation
---------------------------------------

These commands will install docker and docker compose on the host.

.. code-block:: bash

    # Install docker prerequisites
    apt install wget dirmngr gnupg ca-certificates ntp curl

    # Install docker
    DOCKER_KEYRING_FILE="/etc/apt/trusted.gpg.d/download.docker.com.gpg"
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key --keyring ${DOCKER_KEYRING_FILE} add -
    echo "deb https://download.docker.com/linux/debian bullseye stable" > /etc/apt/sources.list.d/docker.list
    cat > /etc/apt/preferences.d/docker-ce <<EOF
    Package: docker-ce*
    Pin: version 5:20.10.13*
    Pin-Priority: 1000
    EOF

    apt update
    apt install docker-ce

    #Install docker-compose
    DOCKER_COMPOSE_VERSION=1.29.2
    COMPOSE="/usr/local/bin/docker-compose"
    curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > "$COMPOSE"
    chmod +x "$COMPOSE"


.. _meetingroom_install_launcher:

2. XiVO Meeting Rooms Launcher Setup
------------------------------------

#. Create ``meetingrooms`` directory:

   .. code-block:: bash

    mkdir -p /etc/docker/meetingrooms

#. Download *XiVO Meeting Rooms* configuration. In the following script replace ``TAG_OR_BRANCH`` by the name of a tag or a branch.

   .. note:: currently to install the latest stable version of Jabbah, use ``TAG_OR_BRANCH=2023.05.00``

   .. code-block:: bash

    TAG_OR_BRANCH=2023.05.00
    cd /etc/docker/meetingrooms
    wget "https://gitlab.com/xivo.solutions/xivo-meetingrooms/-/archive/${TAG_OR_BRANCH}/${TAG_OR_BRANCH}.tar.gz"
    tar -zxvf ${TAG_OR_BRANCH}.tar.gz -C /etc/docker/meetingrooms --strip-components 1
    rm ${TAG_OR_BRANCH}.tar.gz

#. Create the jitsi configuration directory:

   .. code-block:: bash

    cd /etc/docker/meetingrooms
    mkdir -p jitsi/{web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}

#. Create bash alias to launch services:

   .. code-block:: bash

    echo "alias meetingrooms-dcomp='docker-compose -p meetingrooms -f /etc/docker/meetingrooms/xivo-meetingrooms.yml --env-file=/etc/docker/meetingrooms/.env'" >> ~/.bashrc
    source ~/.bashrc


3. Next step
------------

Congrats: You're done with the Meeting Rooms Installation.

* **Next step**: go to :ref:`meetingrooms_config`


.. _meetingrooms_upgrade:

Upgrade
=======

Currently there is no *automatic upgrade* process. Here is the manual process that you need to follow on the Meeting Room server.

* Make a backup of the Meeting Room launcher:

  .. code-block:: bash

    cp -aR /etc/docker/meetingrooms/ /var/tmp/meetingrooms-backup/

* Re-install the Meeting Room Launcher (it will override the :file:`.yml` files): follow the :ref:`meetingroom_install_launcher` steps to install new version

  .. note:: This step overrides the current :file:`.yml` files. If you had made some customization in them you will have to backport them by comparing the new one with the backup you did at previous step.
* And then verify that the content of the :file:`.env` file is correct:

  * Compare the old version and what is defined in the :ref:`meetingrooms_config` section.
  * Verify that the ``XIVOCC_TAG`` and ``XIVOCC_DIST`` correspond to what you want to install (it should be `2022.10` and `latest`).
* Finally pull the new images and restart the containers:

  .. warning:: it will stop all Meeting Room calls (and disconnect users from the application).

  .. code-block:: bash

    docker login -u xivoxc
    (use the token provided by the XiVO team)

    meetingrooms-dcomp pull && docker logout
    meetingrooms-dcomp up -d
