.. _recording_api:

*************************
Recording server REST API
*************************

This section describes the Recording server API.

In the following, all urls are relative to the recording server base url and port. For example a relative URL
``/recording/records/search`` is meant to be replaced by ``http://192.168.29.101:9400/recording/records/search``
assuming the recording server is available on port ``9400`` at ``192.168.29.101``

Authentication
==============

To use the recording server API you need to add an additional header in every HTTP Request. The header name is ``X-Auth-Token``
and its value must be the same as the ``PLAY_AUTH_TOKEN`` environment variable defined in the ``custom.env`` of the docker-compose environment hosting the recording server container (see :ref:`api_shared_token`).

Example:

::

   curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json"
            -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"  http://192.168.29.101:9400/recording/records/search?pageSize=3  -d '{"agent": "1573"}'



Records
=======

Search
------

This api allows to search for recorded **answered** calls in the database using criteria.

**Description:**

:URL: ``/recording/records/search``
:Method: ``POST``
:Url parameters:
   :page: The page number to return, counting from 1
   :pageSize: The number of elements per page
:Request body: Json object with field & value pair.

----

.. _recording_search:

**Allowed field names:**

:agent: The agent number or part of the name to filter on
:queue: The queue number or part of the name to filter on
:start: Return only calls starting or ending after the given value
:end: Return only calls starting before the given value
:callee: Return only calls with the given destination number. `%` char can be used as wildcard.
:caller: Return only calls with the give source number. `%` char can be used as wildcard.
:direction: Filter calls based on the call direction, one of ``incoming``, ``outgoing`` or ``all``
:queueCallStatus: Filter based on queue status ``abandoned``  ``answered``  ``divert_ca_ratio``  ``divert_waittime``  ``closed``
  ``full``  ``joinempty``  ``leaveempty``  ``timeout``
:key: filter based on the given key name in the attached data along with the value of the ``value`` field
:value: value of the key defined in the ``key`` field

----

**Example**

Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json"
            -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"  http://192.168.29.101:9400/recording/records/search?pageSize=3  -d '{"agent": "1573"}'

Response::

    {
        "hasNext": true,
        "records": [
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433866.13971"
                },
                "dst_num": "73555",
                "duration": "00:00:20",
                "id": "xivocc_gateway-1459433866.13971",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:17:46",
                "status": "answered"
            },
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433330.13665"
                },
                "dst_num": "73555",
                "duration": "00:01:01",
                "id": "xivocc_gateway-1459433330.13665",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:08:51",
                "status": "answered"
            }
        ]
    }


Search by call id
-----------------

This api allows to search for **any** recorded call based on call id.

**Description:**

:URL: ``/recording/records/callid_search``
:Method: ``POST``
:Url parameters:
   :callid: The call id to retrieve

----

**Example**
Query::

  curl -XPOST -H "Accept: application/json"
            -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"  http://192.168.29.101:9400/recording/records/callid_search?callid=1459435466.286075

Response::

        {
            "hasNext": false,
            "records": [
                {
                    "agent": "Dino Falconetti (1564)",
                    "attached_data": {
                        "recording": "xivocc_gateway-1459435465.15089"
                    },
                    "dst_num": "73556",
                    "duration": "",
                    "id": "xivocc_gateway-1459435465.15089",
                    "queue": "hotline (3556)",
                    "src_num": "loadtester",
                    "start": "2016-03-31 16:44:26",
                    "status": "answered"
                }
            ]
        }

Retrieve audio file
-------------------

This api allows to get metadata or retrieve audio file of a given call. Each action performmed will be logged in access log file. See :ref:`recording_access`.

**Description:**

:URL: ``/recording/records/<file-id>/audio/<action>``
:Method: ``GET``
:Url parameters:
   :file-ud: The file to retrieve
   :action: Action done on the audio file

----

**Allowed actions:**

:result: To retrieve metadata from a search
:listen: To notify that we listened to the file
:download: To get the file locally

----

**Example**::

  curl -XGET -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"
            http://192.168.29.101:9400/recording/records/xivocc_gateway-1459435465.15089/audio/download


.. _recording_api_attach_data:

Attach call data
----------------

This api allows to attach data to a given call

**Description:**

:URL: ``/recording/call_data/<call-data-id>/attached_data``
:Method: ``POST``
:Url parameters:
   :call-data-id: The id of the call-data, not to be confused by the call id or unique id.
:Request Body: An array of key value

----

**Example**::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"
            http://192.168.29.101:9400/recording/call_data/761054/attached_data  -d '[{"key":"color", "value": "green"}]'

History
=======

Search
------

This API gives the call history of a given interface.

It returns 10 results by default - one can use the parameter ``size`` to specify how many results the query should return.

**Description:**

:URL: ``/recording/history``
:Method: ``POST``
:Url parameters:
   :size: The maximum number of results to return
:Request Body: A json object with a field named ``interface`` containing the interface to search for.

----

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" http://localhost:9400/recording/history  -d '{"interface":"SIP/az9kf7"}'

Response::

  [
   {
    "start": "2017-01-27 15:37:54",
    "duration": "00:00:18",
    "src_num": "1001",
    "dst_num": "3000",
    "status": "emitted",
    "src_firstname": "Poste",
    "src_lastname": "Poste 1001",
    "dst_firstname": null,
    "dst_lastname": "Cars"
   }
  ]

Search by number of days
------------------------

This API gives the call history of a given interface from today till a given number of days ago.

Use the parameter ``days`` to specify the number of days to return the history for.

With no parameters, the query will fall back on the previous API and display the 10 last calls.

Warning: do not use the parameter ``days`` from this API and the parameter ``size`` from the previous API at the same time, as it will cause an error.

**Description:**

:URL: ``/recording/history``
:Method: ``POST``
:Url parameters:
   :days: The number of days to display
:Request Body: A json object with a field named ``interface`` containing the interface to search for.

----

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" http://localhost:9400/recording/history?days=7  -d '{"interface":"SIP/az9kf7"}'

Response::

  [
   {
    "start": "2017-01-27 15:37:54",
    "duration": "00:00:18",
    "src_num": "1001",
    "dst_num": "3000",
    "status": "emitted",
    "src_firstname": "Poste",
    "src_lastname": "Poste 1001",
    "dst_firstname": null,
    "dst_lastname": "Cars"
   }
  ]

Search by agent number
----------------------

This API gives the call history of a given agent number.

**Description:**

:URL: ``/recording/history/agent``
:Method: ``POST``
:Url parameters:
   :size: The maximum number of result to return (*default:* 10)
   :days: The number of days to retrieve (*default:* 7)

        .. warning:: Take care that the greater the number of days you ask to retrieve,
            the heavier the request will be to the databvase (depending on the number of calls in the dababase).
:Request Body: A json object with a field named ``agentNum`` containing the agent number to search for.

----

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' \
    http://localhost:9400/recording/history/agent  -d '{"agentNum":"1004"}'

Response::

  [
   {
    "start": "2021-01-19 16:19:54",
    "duration": "00:00:02",
    "src_num": "1008",
    "dst_num": "1023",
    "status": "emitted",
    "src_firstname": "Yealink",
    "src_lastname": "T54W",
    "dst_firstname": "Marc",
    "dst_lastname": "WebRTC"
   }
  ]


Search by customer
------------------

This api helps to find call history of a customer thanks to a list of predefined filters.

**Description:**

:URL: ``/recording/history/customer``
:Method: ``POST``
:Request Body: A json object with filters (optional) named ``filters`` containing the customer to search for and ``size`` to limit the results returned.
:Response: ``total`` the number of call received by this customer, ``list`` the call details reduced to the ``size`` set in query.

----

A filter is composed of a ``field`` as key (basically column name), an ``operator`` (=, <, >) and a ``value``.

**Allowed filter field:**

:src_num: The customer phone number
:key: Call Attached data key
:value: Call Attached data value

----

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}"
        http://localhost:9400/recording/history/customer -d '{"filters": [{"field":"src_num", "operator": "=", "value": "1456"}], "size":2}'

Response::

  {
    "total":11,
    "list":[
      {"start":"2017-06-13 17:32:45","duration":"00:00:08",
      "wait_time":"00:00:06","agent_name":"Brucé Waill",
      "agent_num":"2500","queue_name":"Bl Record",
      "queue_num":"3012","status":"answered"},
      {"start":"2017-06-13 17:26:54","duration":"00:00:06",
      "wait_time":"00:00:05","agent_name":"Brucé Waill",
      "agent_num":"2500","queue_name":"Blue Ocean",
      "queue_num":"3000","status":"answered"
      }
    ]
  }

Last agent for number
---------------------

This api retrieves the last agent id who answered a given caller number.

**Description:**

:URL: ``/recording/last_agent``
:Method: ``GET``
:Url parameters:
   :callerNo: The calling number
   :since: The number of days to search in the history

----

**Example**
Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" 'http://localhost:9400/recording/last_agent?callerNo=1002&since=100'

Response::

  {"agentNumber":"2000"}

Channel Events Logging
======================

Get CEL
-------

This api retrieve the CEL from the whole XDS System (main and media servers).

**Description:**

:URL: ``/recording/cel``
:Method: ``GET``
:Url parameters:
    :from: The first cel id to retrieve
    :limit: The number of cel to retrieve

**Example**
Query::

  curl H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9400/recording/cel?from=10&limit=1000'

Response::

  [
  {
    "id": "11",
    "eventtype": "LINKED_END",
    "eventtime": "2020-03-23 16:21:25.722316",
    "userdeftype": "",
    "cid_name": "James Bond",
    "cid_num": "1000",
    "cid_ani": "1000",
    "cid_rdnis": "",
    "cid_dnid": "",
    "exten": "s",
    "context": "echotest",
    "channame": "SIP/4uqyf9q-00000001",
    "appname": "",
    "appdata": "",
    "amaflags": "3",
    "accountcode": "",
    "peeraccount": "",
    "uniqueid": "1584976887.1",
    "linkedid": "1584976887.1",
    "userfield": "",
    "peer": "",
    "call_log_id": "",
    "extra": ""
  },
  {...}
  ]
