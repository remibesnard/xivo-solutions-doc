.. _user_api:

********
User API
********

.. _user_api_login:

Login and Authentication
=========================

Users can connect using login, password and phone number:

::

   var wsurl = "ws://"+server+"/xuc/api/2.0/cti?token="+token;
   Cti.WebSocket.init(wsurl,username,phoneNumber);


Directory And Favorites
=======================

Cti.directoryLookUp: function(term)
-----------------------------------

This command deprecates previously used `Cti.searchDirectory(pattern)`.
This command deprecates previously used `Cti.searchDirectory(pattern)` removed in xuc xivo16 versions.

Associated Handler
------------------
* Cti.MessageType.DIRECTORYRESULT

Triggered by command `Cti.directoryLookUp(pattern)`.

.. code-block:: json

    {
        "msgType": "DirectoryResult",
        "ctiMessage": {
           "entries": [
               { "status": 0, "entry": [ "hawkeye", "pierce", "1002", "0761187406", "false"]},
               { "status": -2, "entry": [ "peter", "pan", "1004", "", "false"]}
            ],
           "headers":
               ["Firstname", "Lastname", "Number", "Mobile", "Favorite"]
        }
    }

Cti.getFavorites: function()
----------------------------
Retrieve all the favorites defined for the user connected

Cti.addFavorite: function(contactId, source)
--------------------------------------------

To set a contact (e.g. from search results) as favorite, source is the directory where favorite will be owned.

Cti.removeFavorite: function(contactId, source)
-----------------------------------------------

User Methods
============

Cti.changeUserStatus(reason)
----------------------------
**Depreciated** Update user status using a Cti server configured status name. (Use :ref:`agent_pause_api` instead for agent only)

Cti.displayNameLookup(username)
-------------------------------
Retrieves user's display name by providing it's username.

Cti.setUserPreference(key, value, value_type)
---------------------------------------------
Update user's preference key. See :ref:`user_preference_keys` for the list of existing keys.

User Events
===========

User Statuses
-------------
* Cti.MessageType.USERSTATUSES : "UsersStatuses"

List all the statuses configured on XiVO to know which are possible pause reasons.

.. code-block:: json

  {
    "msgType":"UsersStatuses",
    "ctiMessage":[
        {"name":"disconnected","color":"#9E9E9E","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]},
        {"name":"away","color":"#FFDD00","longName":"Sorti","actions":[{"name":"enablednd","parameters":"false"}]},
        {"name":"berightback","color":"#F2833A","longName":"Bientôt de retour","actions":[{"name":"enablednd","parameters":"false"}]},
        {"name":"available","color":"#9BC920","longName":"Disponible","actions":[{"name":"enablednd","parameters":"false"}]},
        {"name":"ook","color":"#FF0F0F","longName":"Far Away","actions":[{"name":"queuepause_all","parameters":"true"}]},
        {"name":"outtolunch","color":"#6CA6FF","longName":"Parti Manger","actions":[{"name":"queuepause_all","parameters":"true"},
        {"name":"enablednd","parameters":"false"}]},
        {"name":"donotdisturb","color":"#D13224","longName":"Ne pas déranger","actions":[{"name":"enablednd","parameters":"true"}]}
    ]
  }

User Status Update
------------------
* Cti.MessageType.USERSTATUSUPDATE : "UserStatusUpdate",

**Depreciated** Triggered when user changes status (while calling `Cti.changeUserStatus()`)

User Config Update
------------------
* Cti.MessageType.USERCONFIGUPDATE : "UserConfigUpdate",

Triggered when config of the user is updated. This happens if forward config is modified or voicemail for example. Any change of the following attribute might trigger this event.

.. code-block:: json

    {
        "msgType":"UserConfigUpdate",
        "ctiMessage":{
            "userId":9,
            "dndEnabled":false,
            "naFwdEnabled":false,
            "naFwdDestination":"",
            "uncFwdEnabled":false,
            "uncFwdDestination":"",
            "busyFwdEnabled":false,
            "busyFwdDestination":"",
            "firstName":"Alice",
            "lastName":"Johnson",
            "fullName":"Alice Johnson",
            "mobileNumber":"064574512",
            "agentId":22,
            "lineIds":[5],
            "voiceMailId":58,
            "voiceMailEnabled":true
        }
    }

User Preference Update
----------------------

* Cti.MessageType.USERPREFERENCE : "UserPreference",

Triggered on login to retrieve all user preferences.
Then triggered when a user preference is updated.

.. code-block:: json

    {
        "msgType":"UserPreference",
        "ctiMessage":{
            "PREFERRED_DEVICE": {
                "value": "TypePhoneDevice",
                "value_type": "String"
            }, "MOBILE_APP_INFO": {
                "value": "true",
                "value_type": "Boolean"
            }
        }
    }

.. _user_preference_keys:

UserPreferences keys
--------------------
The existing keys are :

* `PREFERRED_DEVICE`: can be `TypePhoneDevice` or `TypeDefaultDevice`
* `MOBILE_APP_INFO`: can be `true` or `false`
