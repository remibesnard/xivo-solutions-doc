.. _conferences_api:

***************
Conferences API
***************

This api is to manipulate voice conference room

You can refer to :ref:`conference_configuration` configuration for organizer feature

A sample of implementation is available in `app/assets/javascripts/pages/sampleConferences.js`
and `app/views/sample/sampleConferences.scala.html`

.. _conference_methods:

Conference Methods
==================

Cti.getConferenceRooms()
------------------------
Request the list of conference rooms. Also receive event when the list is updated.

.. warning:: The xuc user must have a line.

::

    [
      {
        "number": "4000",
        "name": "public",
        "pinRequired": false,
        "startTime": 1519659524032,
        "members": [
          {
            "joinOrder": 1,
            "joinTime": 1519659524032,
            "muted": false,
            "name": "James Bond",
            "number": "1002",
            "username": "jbond"
          }
        ]
      },
      {
        "number": "4001",
        "name": "conference_support",
        "pinRequired": true,
        "startTime": 0,
        "members": []
      }
    ]

Cti.conference()
----------------
Start a conference using phone set capabilities

Cti.conferenceMuteMe(conferenceNumber)
--------------------------------------
Mute the current user in the given conference.

See also `conference_command_error`_.

Cti.conferenceUnmuteMe(conferenceNumber)
----------------------------------------
Un-mute the current user in the given conference

See also `conference_command_error`_.

Cti.conferenceMuteAll(conferenceNumber)
---------------------------------------
Mute all attendees except current user in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceUnmuteAll(conferenceNumber)
-----------------------------------------
Un-Mute all attendees in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceMute(conferenceNumber, index)
-------------------------------------------
Mute an attendee by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceUnmute(conferenceNumber, index)
---------------------------------------------
Un-Mute an attendee by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceDeafen(conferenceNumber, index)
---------------------------------------------
Make an attendee deaf by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin. After this command, the participant will not hear the conference but other participants can hear him.

.. note::
   When a conference participant is deafened, a button will appear in the UC Assistant to undeafen him. This button is only available for conference organizers.

See also `conference_command_error`_.

Cti.conferenceUndeafen(conferenceNumber, index)
-----------------------------------------------
Make an attendee undeaf by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin. After this command, the participant hear again the conference.

See also `conference_command_error`_.

Cti.conferenceKick(conferenceNumber, index)
---------------------------------------------
Kick an attendee out of the given conference by its index. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.


Cti.conferenceClose(conferenceNumber)
-------------------------------------
Close the given conference and hangup all participants. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.


Cti.conferenceInvite(conferenceNumber, exten, role, earlyJoin, variables)
-------------------------------------------------------------------------
Invite the given "exten" in the conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

Parameters :

    * conferenceNumber: conference number
    * exten: the guest extension to invite in the conference
    * role: role of the guest once in the conference, either "User" or "Organizer"
    * earlyJoin: boolean, set to "true" to ear the ringback in the conference or set to "false" if you want the guest to join once he answered 
    * variables: set of variables to be propagated
    
  Note: Variables need to be in object format {"key1": "value1", "key2": "value2"} and they will be passed to the channels variables, prepended by `USR_` for each key. (USR_key1, USR_key2 in this case)

See also `conference_command_error`_.

Cti.conferenceReset(conferenceNumber)
-------------------------------------
Reset the participants state in the given conference to their default state. All muted or deaf participants are not muted or deaf anymore. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.


.. _conference_events:

Conference Events
=================

See associated :ref:`conference_methods`

Conference Event
----------------

- Cti.MessageType.CONFERENCEEVENT

Received when you enter or leave a conference room.

::

    {
      "eventType": "Join",
      "uniqueId": "1519658665.8",
      "conferenceNumber": "4001",
      "conferenceName": "conference_support",
      "participants": [
        {
          "index": 1,
          "callerIdName": "James Bond",
          "callerIdNum": "1002",
          "since": 0,
          "isTalking": false,
          "role": "User",
          "isMuted": false,
          "isMe": true,
          "username": "jbond",
          "isDeaf": false
        }
      ],
      "since": 0
    }

Fields description :

    * eventType: "Join" or "Leave"
    * uniqueId: channel / call unique id entering this conference (related to Phone events)
    * conferenceNumber: DN Number of the joined/left conference
    * conferenceName: Name of the joined/left conference
    * participants: array of participant
    * since: delay in seconds since the beginning of the conference


Conference Participant Event
----------------------------

- Cti.MessageType.CONFERENCEPARTICIPANTEVENT

Received when a participant enter, leave, or be updated in your conference room.

::

    {
      "eventType": "Update",
      "uniqueId": "1519658665.8",
      "conferenceNumber": "4001",
      "index": 1,
      "callerIdName": "James Bond",
      "callerIdNum": "1002",
      "since": 0,
      "isTalking": true,
      "role": "Organizer",
      "isMuted": false,
      "isMe": false,
      "username": "jbond",
      "isDeaf": false
    }

Fields description :

    * eventType: "Join", "Leave" or "Update"
    * uniqueId: channel / call unique id entering this conference (related to Phone events)
    * conferenceNumber: DN Number of the joined/left conference
    * conferenceName: Name of the joined/left conference
    * index: position of the participant in the conference
    * callerIdName: Name of the participant
    * callerIdNum: DN Number of the participant
    * since: delay in seconds since the beginning of the conference
    * isTalking: true or false if participant is talking
    * role: participant role, either "User" or "Organizer"
    * isMuted: indicate if participant is muted
    * isMe: indicate if participant is the current user
    * username: username cti of the user, it's only defined for internal users
    * isDeaf: indicate if the participant is deaf

.. _conference_command_error:

Conference Command Error Event
------------------------------

- Cti.MessageType.CONFERENCECOMMANDERROR

Received after a conference command (mute/unmute, muteme/unmuteme, muteall/unmuteall, kick) if an error is encountered while processing the command

::

    {
      "error": "NotAMember"
    }

The error code can be one of the following:

    * NotAMember: The current user is not a member of the given conference number.
    * NotOrganizer: The current user is not an organizer in the given conference number and cannot perform the command.
    * CannotKickOrganizer: You cannot kick an organizer out of a conference.
    * ParticipantNotFound: The targeted participant wasn't found.
