.. _restapi:

********
Rest API
********

The REST API allows you to control XiVO from a remote application. It can be accessed through HTTP requests using the following base URL: `http://localhost:${XUC_PORT}/xuc/api/2.0` where the default XUC port number is `8090`.

Type of users
=============

Two types of users can request the API: CTI users and web service users.

CTI users
---------

CTI users are XiVO users which are usually associated with a line (either plastic phone or WebRTC).
They have have limited rights to the API, they can only request specific endpoints.


Web service users
-----------------

Web service users are technical users. They are not associated with any line. They have specific and custom rights to use the API.


Permissions and ACLs
====================

ACLs handle user permissions. CTI users all have the same permissions, they all are contained into the alias `alias.ctiuser`. They only can request some endpoints of the API.

However, web service user may have specific and custom permissions to request the API.
A web service user can have access to specific endpoints, or to specific methods.

See :ref:`rest-api-acl` for more information.

ACLs of CTI users
-----------------

All CTI users share the same permissions.
The alias `alias.ctiuser` contains the following permissions:

::

   ["xuc.rest.contact.personal.read",
   "xuc.rest.contact.personal.create",
   "xuc.rest.contact.personal.delete",
   "xuc.rest.contact.personal.*.read",
   "xuc.rest.contact.personal.*.update",
   "xuc.rest.contact.personal.*.delete",
   "xuc.rest.contact.export.personal.read",
   "xuc.rest.contact.import.personal.create",
   "xuc.rest.contact.display.personal.read",
   "xuc.rest.call_qualification.queue.*.read",
   "xuc.rest.call_qualification.csv.#.read",
   "xuc.rest.call_qualification.create",
   "xuc.rest.mobile.push.register.create",
   "xuc.rest.config.meetingrooms.static.token.*.read",
   "xuc.rest.config.meetingrooms.personal.token.*.read",
   "xuc.rest.config.meetingrooms.temporary.token.*.read",
   "xuc.rest.config.meetingrooms.personal.*.read",
   "xuc.rest.config.meetingrooms.personal.create",
   "xuc.rest.config.meetingrooms.personal.update",
   "xuc.rest.config.meetingrooms.personal.*.delete",
   "xuc.rest.config.#.read",
   "xuc.rest.config.#.create",
   "xuc.rest.config.#.update"]

.. _rest_authentication:

User basic authentication
=========================

Only authenticated users can use the API. The authentication process provides a JWT token that must be used in all subsequent requests.

CTI user authentication
-----------------------

Request
^^^^^^^

+--------+--------------------------------+
| Method | POST                           |
+--------+--------------------------------+
| URI    | /auth/login                    |
+--------+--------------------------------+
| Header | Content-Type: application/json |
+--------+--------------------------------+

Body parameters
"""""""""""""""

+----------+--------+-------------------+
| Field    | Type   | Description       |
+==========+========+===================+
| login    | string | CTI user login    |
+----------+--------+-------------------+
| password | string | CTI user password |
+----------+--------+-------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/auth/login \
   -d '{"login": "cti_user_login", "password": "cti_user_password"}' \
   -H "Content-Type: application/json"

Response
^^^^^^^^

::

   {
      login: "$CTI_USER_LOGIN",
      token: "$CTI_USER_JWT_TOKEN",
      TTL: $CTI_USER_TTL
   }

Model
"""""

+-------+---------+--------------------------------------+
| Field | Type    | Description                          |
+=======+=========+======================================+
| login | string  | CTI user login                       |
+-------+---------+--------------------------------------+
| token | string  | CTI user JWT token                   |
+-------+---------+--------------------------------------+
| TTL   | integer | Time to live of the token in seconds |
+-------+---------+--------------------------------------+

Error messages
""""""""""""""

+--------------------+------------------+-------------------------------------------------+
| Error code         | HTTP header code | Possible cause                                  |
+====================+==================+=================================================+
| InvalidCredentials | 401              | Invalid login and/or invalid password           |
+--------------------+------------------+-------------------------------------------------+
| InvalidJson        | 400              | The request body does not follow the body model |
+--------------------+------------------+-------------------------------------------------+
| NotHandledError    | 500              | Error not covered in current implementation     |
+--------------------+------------------+-------------------------------------------------+


Web service user authentication
-------------------------------

Request
^^^^^^^

+--------+--------------------------------+
| Method | POST                           |
+--------+--------------------------------+
| URI    | /auth/webservice               |
+--------+--------------------------------+
| Header | Content-Type: application/json |
+--------+--------------------------------+

Body parameters
"""""""""""""""

+----------+--------+---------------------------+
| Field    | Type   | Description               |
+==========+========+===========================+
| login    | string | Web service user login    |
+----------+--------+---------------------------+
| password | string | Web service user password |
+----------+--------+---------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/auth/webservice \
   -d '{"login": "web_service_user_login", "password": "web_service_user_password"}' \
   -H "Content-Type: application/json"

Response
^^^^^^^^

::

   {
      login: "$WEB_SERVICE_USER_LOGIN",
      token: "$WEB_SERVICE_USER_JWT_TOKEN",
      TTL: $WEB_SERVICE_USER_TTL
   }


Model
"""""

+-------+---------+--------------------------------------+
| Field | Type    | Description                          |
+=======+=========+======================================+
| login | string  | Web service user login               |
+-------+---------+--------------------------------------+
| token | string  | Web service user JWT token           |
+-------+---------+--------------------------------------+
| TTL   | integer | Time to live of the token in seconds |
+-------+---------+--------------------------------------+

Error messages
""""""""""""""

+--------------------+------------------+-------------------------------------------------+
| Error code         | HTTP header code | Possible cause                                  |
+====================+==================+=================================================+
| InvalidCredentials | 401              | Invalid login and/or invalid password           |
+--------------------+------------------+-------------------------------------------------+
| InvalidJson        | 400              | The request body does not follow the body model |
+--------------------+------------------+-------------------------------------------------+
| NotHandledError    | 500              | Error not covered in current implementation     |
+--------------------+------------------+-------------------------------------------------+

JWT token
---------

The JWT token is necessary to use the API the other endpoints of the API. It must be provided in the `Authorization` header of all subsequent requests.

The contents of the token changes from Kuma.
It now contains the following informations:
- login: the login of the user
- expiresAt: the date at which the token expires (in milliseconds since the epoch)
- issuedAt: the date at which the token was issued (in milliseconds since the epoch)
- userType: the type of the user (`cti` or `webservice`)
- acls: the permissions of the user (see :ref:`rest-api-acl`)

As a consequence, any token generated before Kuma will not work anymore and users need to re-login.

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_renew`.


.. _kerberos_authentication:

Single sign-in (SSO authentication)
===================================

.. note:: Only CTI users can be authenticated using SSO.


Authentication with Kerberos
----------------------------

Request
^^^^^^^

+--------+-----------+
| Method | GET       |
+--------+-----------+
| URI    | /auth/sso |
+--------+-----------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/auth/sso

Response
^^^^^^^^

::

   {
      login: "$CTI_USER_LOGIN",
      token: "$CTI_USER_JWT_TOKEN",
   }

Model
"""""

+-------+--------+--------------------+
| Field | Type   | Description        |
+=======+========+====================+
| login | string | CTI user login     |
+-------+--------+--------------------+
| token | string | CTI user JWT token |
+-------+--------+--------------------+

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_renew`.

Error messages
""""""""""""""

+-------------------------+------------------+----------------------------------------------------+
| Error code              | HTTP header code | Possible cause                                     |
+=========================+==================+====================================================+
| UserNotFound            | 401              | The user does not exist                            |
+-------------------------+------------------+----------------------------------------------------+
| SsoAuthenticationFailed | 401              | The user could not be authenticated using Kerberos |
+-------------------------+------------------+----------------------------------------------------+
| NotHandledError         | 500              | Error not covered in current implementation        |
+-------------------------+------------------+----------------------------------------------------+


Error messages
""""""""""""""

+-------------------------+------------------+----------------------------------------------------+
| Error code              | HTTP header code | Possible cause                                     |
+=========================+==================+====================================================+
| UserNotFound            | 401              | The user does not exist                            |
+-------------------------+------------------+----------------------------------------------------+
| SsoAuthenticationFailed | 401              | The user could not be authenticated using Kerberos |
+-------------------------+------------------+----------------------------------------------------+
| NotHandledError         | 500              | Error not covered in current implementation        |
+-------------------------+------------------+----------------------------------------------------+


.. _cas_authentication:

Authentication with CAS
-----------------------

Request
^^^^^^^

+--------+-----------+
| Method | GET       |
+--------+-----------+
| URI    | /auth/cas |
+--------+-----------+


URL parameters
""""""""""""""

+---------+--------+---------------------------+
| Field   | Type   | Description               |
+=========+========+===========================+
| service | string | The URL of the CAS server |
+---------+--------+---------------------------+

| service | string | The URL of the CAS server |

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/auth/cas?service=https://cas.myplatform.com&ticket=ST-11-Qsicgrh1mZ3dgoeOx7m6-af27d9025e0c


Response
^^^^^^^^

::

   {
      login: "$CTI_USER_LOGIN",
      token: "$CTI_USER_JWT_TOKEN",
   }


Model
"""""

+-------+--------+--------------------+
| Field | Type   | Description        |
+=======+========+====================+
| login | string | CTI user login     |
+-------+--------+--------------------+
| token | string | CTI user JWT token |
+-------+--------+--------------------+

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_renew`.

Error messages
""""""""""""""

+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| Error code                        | HTTP header code | Possible cause                                                                                   |
+===================================+==================+==================================================================================================+
| UserNotFound                      | 401              | The user does not exist                                                                          |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerUrlNotSet                | 403              | XiVOCC containers are not configured (see :ref:`CAS SSO Authentication <cas-sso-configuration>`) |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidResponse          | 403              | The CAS server returned an invalid response                                                      |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidParameter         | 403              | The Parameters sent to the CAS Server are invalid                                                |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidRequest           | 403              | The Request to the CAS server is invalid                                                         |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidTicketSpec        | 403              | The ticket specification is invalid                                                              |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerUnauthorizedServiceProxy | 403              | The CAS service proxy is not authorized                                                          |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidProxyCallback     | 403              | The CAS service proxy callback is invalid                                                        |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidTicket            | 403              | The ticket is invalid (probably expired or defined for another service)                          |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInvalidService           | 403              | The service is invalid                                                                           |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| CasServerInternalError            | 403              | CAS Server internal error                                                                        |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+
| NotHandledError                   | 500              | Error not covered in current implementation                                                      |
+-----------------------------------+------------------+--------------------------------------------------------------------------------------------------+

.. _openid_authentication:

Authentication with OpenID Connect (OIDC)
-----------------------------------------

Request
^^^^^^^

+--------+------------+
| Method | GET        |
+--------+------------+
| URI    | /auth/oidc |
+--------+------------+

.. note:: The token can also be provided in the `Authorization` header using the `Bearer` scheme.

URL parameters
""""""""""""""

+-------+--------+--------------------+
| Field | Type   | Description        |
+=======+========+====================+
| token | string | CTI user JWT token |
+-------+--------+--------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/auth/oidc?token=${OAUTH_TOKEN}


Response
^^^^^^^^

::

   {
      login: "$WEBSERVICE_USER_LOGIN",
      token: "$WEBSERVICE_USER_JWT_TOKEN",
      TTL: $WEBSERVICE_USER_TTL
   }


Model
"""""

+-------+--------+--------------------+
| Field | Type   | Description        |
+=======+========+====================+
| login | string | CTI user login     |
+-------+--------+--------------------+
| token | string | CTI user JWT token |
+-------+--------+--------------------+

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_renew`.


Error messages
""""""""""""""

+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+
| Error code               | HTTP header code | Possible cause                                                                                                       |
+==========================+==================+======================================================================================================================+
| UserNotFound             | 401              | The user does not exist                                                                                              |
+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+
| OidcNotEnabled           | 403              | OpenID connect authentication is not enabled (see :ref:`OpenID Connect SSO Authentication <oidc-sso-configuration>`) |
+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+
| OidcInvalidParameter     | 403              | Missing access_token                                                                                                 |
+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+
| OidcAuthenticationFailed | 403              | The access_token forwarded to XUC is invalid                                                                         |
+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+
| NotHandledError          | 500              | Error not covered in current implementation                                                                          |
+--------------------------+------------------+----------------------------------------------------------------------------------------------------------------------+


.. _rest_authentication_renew:

Check and renew JWT token
-------------------------

Any user, either CTI or Web service, can check and renew their JWT token.

Request
^^^^^^^

+--------+------------------------------------+
| Method | GET                                |
+--------+------------------------------------+
| URI    | /auth/check                        |
+--------+------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN} |
+--------+------------------------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/auth/check \
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

::

   {
      login: "$USER_LOGIN",
      token: "$USER_JWT_TOKEN",
      TTL: $USER_TTL
   }


Model
"""""

+-------+---------+--------------------------------------+
| Field | Type    | Description                          |
+=======+=========+======================================+
| login | string  | CTI user login                       |
+-------+---------+--------------------------------------+
| token | string  | CTI user JWT token                   |
+-------+---------+--------------------------------------+
| TTL   | integer | Time to live of the token in seconds |
+-------+---------+--------------------------------------+

Error messages
""""""""""""""

+-----------------------------+------------------+------------------------------------------------------------------------+
| Error code                  | HTTP header code | Possible cause                                                         |
+=============================+==================+========================================================================+
| InvalidToken                | 403              | Token does not match XUC signature tokens                              |
+-----------------------------+------------------+------------------------------------------------------------------------+
| InvalidJson                 | 400              | The request body is not a valid JSON or does not follow the body model |
+-----------------------------+------------------+------------------------------------------------------------------------+
| BearerNotFound              | 400              | The token is not found in the `Authorization` header                   |
+-----------------------------+------------------+------------------------------------------------------------------------+
| AuthorizationHeaderNotFound | 400              | The `Authorization` header is not found in the request                 |
+-----------------------------+------------------+------------------------------------------------------------------------+
| TokenExpired                | 403              | The `Authorization` header is not found in the request                 |
+-----------------------------+------------------+------------------------------------------------------------------------+
| NotHandledError             | 500              | Error not covered in current implementation                            |
+-----------------------------+------------------+------------------------------------------------------------------------+


Personal contacts
=================

CTI users can use the endpoints related to personal contacts.

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

   {
      "error": ${ERROR_CODE}
      "message": ${ERROR_MESSAGE}
   }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| InvalidContact   | 400              | Personal contact has bad format                     |
+------------------+------------------+-----------------------------------------------------+
| ContactNotFound  | 404              | Personal contact does not exist                     |
+------------------+------------------+-----------------------------------------------------+
| DuplicateContact | 409              | Personal contact already exists                     |
+------------------+------------------+-----------------------------------------------------+
| Unreachable      | 503              | Directory server is not reachable                   |
+------------------+------------------+-----------------------------------------------------+
| JsonParsingError | 500              | Personal Contact sent to API is not JSON valid      |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+


List all personal contacts
--------------------------

### Request

+--------+------------------------------------+
| Method | GET                                |
+--------+------------------------------------+
| URI    | /contact/personal                  |
+--------+------------------------------------+
| Header | Content-Type: application/json     |
+--------+------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN} |
+--------+------------------------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/contact/personal \
   -H "Content-Type: application/json" \ 
   -H "Authorization: Bearer user_jwt_token"

Response
^^^^^^^^

::

   {
     "entries": [
         { "status": 0, "entry": [ "hawkeye", "pierce", "1002", "0761187406", "false"]},
         { "status": -2, "entry": [ "peter", "pan", "1004", "", "false"]}],
     "headers":
         ["Firstname", "Lastname", "Number", "Mobile", "Favorite"]
    }


Retrieve a specific personal contact
------------------------------------

Request
^^^^^^^

+--------+---------------------------------------------------------------------------+
| Method | GET                                                                       |
+--------+---------------------------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/personal/${CONTACT_UUID} |
+--------+---------------------------------------------------------------------------+
| Header | Content-Type: application/json                                            |
+--------+---------------------------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                                        |
+--------+---------------------------------------------------------------------------+

URL parameters
""""""""""""""

+--------------+--------+----------------------------------------+
| Field        | Type   | Description                            |
+==============+========+========================================+
| CONTACT_UUID | string | UUID of the requested personal contact |
+--------------+--------+----------------------------------------+


Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f \
   -H "Content-Type: application/json" \ 
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

::

   {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
   }


Create a personal contact
-------------------------

Request
^^^^^^^

+--------+-----------------------------------------------------------+
| Method | POST                                                      |
+--------+-----------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/personal |
+--------+-----------------------------------------------------------+
| Header | Content-Type: application/json                            |
+--------+-----------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                        |
+--------+-----------------------------------------------------------+

Body parameters
"""""""""""""""

+-----------+--------+---------------------------------------+
| Field     | Type   | Description                           |
+===========+========+=======================================+
| lastname  | string | Last name of the personal contact     |
+-----------+--------+---------------------------------------+
| firstname | string | First name of the personal contact    |
+-----------+--------+---------------------------------------+
| company   | string | Company of the personal contact       |
+-----------+--------+---------------------------------------+
| email     | string | Email address of the personal contact |
+-----------+--------+---------------------------------------+
| number    | string | Phone number of the personal contact  |
+-----------+--------+---------------------------------------+
| mobile    | string | Mobile number of the personal contact |
+-----------+--------+---------------------------------------+
| fax       | string | Fax number of the personal contact    |
+-----------+--------+---------------------------------------+

.. note:: At least one of the following fields must be set: `number`, `mobile`, `fax`, `email` **and** `lastname` and/or `firstname`.

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/contact/personal \
   -H "Content-Type: application/json" \ 
   -H "Authorization: Bearer user_jwt_token" \
   -d '{"firstname":"doe","lastname":"john","number":"1111","mobile":"2222","fax":"3333","email":"j.doe@my.corp","number":"1111","mobile":"2222","fax":"3333"}'

Response
^^^^^^^^

::

   {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
   }


Update a personal contact
-------------------------

Request
^^^^^^^

+--------+---------------------------------------------------------------------------+
| Method | PUT                                                                       |
+--------+---------------------------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/personal/${CONTACT_UUID} |
+--------+---------------------------------------------------------------------------+
| Header | Content-Type: application/json                                            |
+--------+---------------------------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                                        |
+--------+---------------------------------------------------------------------------+

URL parameters
""""""""""""""

+--------------+--------+----------------------------------------+
| Field        | Type   | Description                            |
+==============+========+========================================+
| CONTACT_UUID | string | UUID of the requested personal contact |
+--------------+--------+----------------------------------------+

Body parameters
"""""""""""""""

+-----------+--------+---------------------------------------+
| Field     | Type   | Description                           |
+===========+========+=======================================+
| lastname  | string | Last name of the personal contact     |
+-----------+--------+---------------------------------------+
| firstname | string | First name of the personal contact    |
+-----------+--------+---------------------------------------+
| company   | string | Company of the personal contact       |
+-----------+--------+---------------------------------------+
| email     | string | Email address of the personal contact |
+-----------+--------+---------------------------------------+
| number    | string | Phone number of the personal contact  |
+-----------+--------+---------------------------------------+
| mobile    | string | Mobile number of the personal contact |
+-----------+--------+---------------------------------------+
| fax       | string | Fax number of the personal contact    |
+-----------+--------+---------------------------------------+

.. note:: All fields are optional

Curl example
""""""""""""

::

   curl -X PUT http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f \
   -H "Content-Type: application/json" \ 
   -H "Authorization: Bearer user_jwt_token" \
   -d '{"firstname":"doe","lastname":"john","number":"1111","mobile":"2222","fax":"3333","email":"j.doe@my.corp","number":"1111","mobile":"2222","fax":"3333"}'

Response
^^^^^^^^

::

   {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
   }


Delete a personal contact
-------------------------

Request
^^^^^^^

+--------+---------------------------------------------------------------------------+
| Method | DELETE                                                                    |
+--------+---------------------------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/personal/${CONTACT_UUID} |
+--------+---------------------------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                                        |
+--------+---------------------------------------------------------------------------+

URL parameters
""""""""""""""

+--------------+--------+----------------------------------------+
| Field        | Type   | Description                            |
+==============+========+========================================+
| CONTACT_UUID | string | UUID of the requested personal contact |
+--------------+--------+----------------------------------------+


Curl example
""""""""""""

::

   curl -X DELETE http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f \
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

It will return a 204 response.


Delete all personal contacts
----------------------------

Request
^^^^^^^

+--------+-----------------------------------------------------------+
| Method | DELETE                                                    |
+--------+-----------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/personal |
+--------+-----------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                        |
+--------+-----------------------------------------------------------+


Curl example
""""""""""""

::

   curl -X DELETE http://localhost:8090/xuc/api/2.0/contact/personal \
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

It will return a 204 response.

Export personal contacts
------------------------

Request
^^^^^^^

+--------+------------------------------------------------------------------+
| Method | GET                                                              |
+--------+------------------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/export/personal |
+--------+------------------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                               |
+--------+------------------------------------------------------------------+


Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/contact/export/personal \
   -H "Authorization: Bearer user_jwt_token"

Response
^^^^^^^^

It will return a csv file with UTF-8 encoding containing all personal of a user

::

   company,email,fax,firstname,lastname,mobile,number
   corp,jdoe@company.corp,3333,John,Doe,2222,1111


Import personal contacts
------------------------

Request
^^^^^^^

+--------+------------------------------------------------------------------+
| Method | POST                                                             |
+--------+------------------------------------------------------------------+
| URI    | http://localhost:${XUC_PORT}/xuc/api/2.0/contact/import/personal |
+--------+------------------------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                               |
+--------+------------------------------------------------------------------+


Curl example
""""""""""""

::

   curl -X POST http://localhost:${XUC_PORT}/xuc/api/2.0/contact/import/personal \
   -H "Authorization: Bearer user_jwt_token" \
   -F 'file=@/path/to/file.csv;type=text/csv'

.. note:: A CSV file must be sent as the body of the request. The first line of the CSV file must be the header. The file may contain the following fields: `company`, `email`, `fax`, `firstname`, `lastname`, `mobile`, `number`. The fields are optional. However, at least one of these fields is required: `number`, `mobile`, `fax`, `email` **and** `lastname` and/or `firstname` The fields must be separated by a comma. The CSV file must be encoded in UTF-8.

::

   company,email,fax,firstname,lastname,mobile,number
   corp,jdoe@company.corp,3333,John,Doe,2222,1111


Response
^^^^^^^^

The request will create and return the response for each personal contact (either success or failure) (HTTP code 201).

Example
"""""""

::

   {
      "created": [{
         "id": "e62ee672-f74a-498c-b138-86ac1b0ae429",
         "lastname": "Wayne"
      }, {
         "id": "d3a67f8e-3d8a-465a-9633-bde65a41f1bb",
         "lastname": "Hawking"
      }],
      "failed": [{
         "line": 3,
         "errors": ["too many fields"]
      }]
   }


Call qualifications
===================

To retrieve call qualification options and create call qualification answer.

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

   {
      "error": ${ERROR_CODE}
      "message": ${ERROR_MESSAGE}
   }

Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Qualification answer sent to API is not found       |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Qualification answer sent to API is not JSON valid  |
+------------------+------------------+-----------------------------------------------------+
| Unreachable      | 503              | Config management server is not available           |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

Get call qualifications for a queue
-----------------------------------

Request
^^^^^^^

+--------+---------------------------------------+
| Method | GET                                   |
+--------+---------------------------------------+
| URI    | /call_qualification/queue/${QUEUE_ID} |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/call_qualification/queue/42 \
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

Will retrieve a list of objects containing all qualifications and sub qualifications for a single queue (HTTP code 200)

::

   [
      {
         "id": 6,
            "name": "General Questions",
            "subQualifications": [
               { "id": 14, "name": "Common"},
               { "id": 15, "name": "Technical" }
            ]
      },
      {
         "id": 5,
            "name": "Support",
            "subQualifications": [
               { "id": 12, "name": "Technical" },
               { "id": 13, "name": "General" }
            ]
      }
   ]

Create call qualifications
--------------------------

Request
^^^^^^^

+--------+------------------------------------+
| Method | POST                               |
+--------+------------------------------------+
| URI    | /call_qualification                |
+--------+------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN} |
+--------+------------------------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/call_qualification \
   -H "Authorization: Bearer user_jwt_token"

Response
^^^^^^^^

::

   {
      "sub_qualification_id": 1,
      "time": "2018-03-21 17:00:00",
      "callid": "callid1",
      "agent": 1,
      "queue": 1,
      "first_name": "john",
      "last_name": "doe",
      "comment": "some comment",
      "custom_data": "some custom data"
   }


Export call qualifications
--------------------------

Request
^^^^^^^

+--------+---------------------------------------------------+
| Method | GET                                               |
+--------+---------------------------------------------------+
| URI    | /call_qualification/csv/${QUEUE_ID}/${FROM}/${TO} |
+--------+---------------------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}                |
+--------+---------------------------------------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/call_qualification/42/home/office \
   -H "Authorization: Bearer user_jwt_token"

Response
^^^^^^^^

It will return a csv file with UTF-8 encoding containing all call qualification answers of a queue

::

   sub_qualification_id,time,callid,agent,queue,first_name,last_name,comment,custom_data
   1,2018-03-21 17:00:00,callid1,1,1,john,doe,some comment,some custom data


Mobile application
==================

To configure/handle XiVO mobile phone application

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::


  {
      "error": <error_code>
      "message": <cause>
  }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Token sent to API is not found                      |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Request sent to API is not JSON valid               |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

Set push registration token
---------------------------

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /mobile/push/register                 |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

Body parameters
"""""""""""""""

+----------+--------+-------------------+
| Field    | Type   | Description       |
+==========+========+===================+
| login    | string | CTI user login    |
+----------+--------+-------------------+

Curl example
""""""""""""

::

   curl -X GET http://localhost:8090/xuc/api/2.0/mobile/push/register  \
   -d '{"token": "1234-5678"}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


Response
^^^^^^^^

The response will register push notification token to wake up the mobile application answer (HTTP code 201).


Dial
====

Multiple dial APIs are available to generate dial actions from a user.


Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

   {
      "error": ${ERROR_CODE}
      "message": ${ERROR_MESSAGE}
   }


Else, the API will return the following JSON format

::

   {
      "res": "success"
   }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| BadRequest       | 400              | The body is missing or is invalid                   |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+


Dial a number
-------------

This request dials the destination using the username of a CTI user provided in the url, also passing optional variables along.

.. note:: The CTI user whose username is provided must be an authenticated.

.. warning:: This action is only available for web service users with `xuc.rest.dial.number.*.create` ACL.


Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /dial/number/${USERNAME}              |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+

Body parameters
"""""""""""""""

+----------------+--------+-----------------------------------------+
| Field          | Type   | Description                             |
+================+========+=========================================+
| destination    | string | Number to be called someone is in queue |
+----------------+--------+-----------------------------------------+
| variables      | object | Any other variables                     |
+----------------+--------+-----------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/dial/number/jbond \
   -d '{"destination":"0123456789", "variables": { "varname1": "varval1",  "varname2": "varval2"}}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


Dial a user
-----------

This request dials the payload username using the username provided in the url, also passing optional variables along.

.. note:: The CTI user whose username is provided must be an authenticated.

.. warning:: This action is only available for web service users with `xuc.rest.dial.user.*.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /dial/user/${USERNAME}                |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+

Body parameters
"""""""""""""""

+----------------+--------+-----------------------------------------+
| Field          | Type   | Description                             |
+================+========+=========================================+
| username       | string | Username of the user to call            |
+----------------+--------+-----------------------------------------+
| variables      | object | Any other variables                     |
+----------------+--------+-----------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/dial/user/jbond \
   -d '{"username":"drno", "variables": { "varname1": "varval1",  "varname2": "varval2"}}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


Dial from queue
---------------

This request dials the destination from a queue using the username provided in the url, also passing optional variables along.

.. warning:: This action is only available for web service users with `xuc.rest.dial.fromqueue.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /dial/fromqueue/${USERNAME}           |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+

Body parameters
"""""""""""""""

+----------------+--------+-----------------------------------------+
| Field          | Type   | Description                             |
+================+========+=========================================+
| destination    | string | Number to be called someone is in queue |
+----------------+--------+-----------------------------------------+
| queueId        | Int    | Identifier of the queue                 |
+----------------+--------+-----------------------------------------+
| variables      | object | Any other variables                     |
+----------------+--------+-----------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/dial/fromqueue/jbond  \
   -d '{"destination":"0123456789", "queueId": 152, "variables": { "varname1": "varval1",  "varname2": "varval2"}}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"




Dial to queue
-------------

This request dials the destination with a specific caller ID and then add it into a queue, also passing optional variables along.

.. warning:: This action is only available for web service users with `xuc.rest.dial.toqueue.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /dial/toqueue                         |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

Body parameters
"""""""""""""""

+----------------+--------+-----------------------------------------+
| Field          | Type   | Description                             |
+================+========+=========================================+
| destination    | string | Number to be called someone is in queue |
+----------------+--------+-----------------------------------------+
| queueName      | string | Name of the queue                       |
+----------------+--------+-----------------------------------------+
| callerIdNumber | string | Number displayed to people in queue     |
+----------------+--------+-----------------------------------------+
| variables      | object | Any other variables                     |
+----------------+--------+-----------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/dial/toqueue  \
   -d '{"destination":"0123456789", "queueName":"support", "callerIdNumber":"0403010200", "variables": { "varname1": "varval1",  "varname2": "varval2"}}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


Do not disturb status and forwarding
====================================

Multiple APIs are available to control user forwards and DND status

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

  {
    "error": <error_code>
    "message": <cause>
  }

Else, the API will return the following JSON format

::

   {
      "res": "success"
   }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Token sent to API is not found                      |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Request sent to API is not JSON valid               |
+------------------+------------------+-----------------------------------------------------+
| UserNotFound     | 400              | User not found                                      |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

Set do not disturb status
-------------------------

This request will set the do not disturb status of the user provided in the url according to the state in the payload.

.. warning:: This action is only available for web service users with `xuc.rest.dnd.*.create` ACL.


Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /dnd/${USERNAME}                      |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+


Body parameters
"""""""""""""""

+----------------+---------+-----------------------------------------------------+
| Field          | Type    | Description                                         |
+================+=========+=====================================================+
| state          | boolean | Whether the do not disturb status is enabled or not |
+----------------+---------+-----------------------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/dnd/jbond  \
   -d '{"state": true}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


Unconditional forwarding
------------------------

This requests sets the unconditional forwarding of the user provided in the url according to the payload.

.. warning:: This action is only available for web service users with `xuc.rest.forward.unconditional.*.create` ACL.


Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /forward/unconditional/${USERNAME}    |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+


Body parameters
"""""""""""""""

+----------------+---------+---------------------------------------+
| Field          | Type    | Description                           |
+================+=========+=======================================+
| destination    | string  | Destination of the forwarding         |
+----------------+---------+---------------------------------------+
| enabled        | boolean | State of the unconditional forwarding |
+----------------+---------+---------------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/forward/unconditional/jbond  \
   -d '{"destination": "0123456789", "enabled": true}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


No answer forwarding
--------------------

This requests sets the no answer forwarding of the user provided in the url according to the payload

.. warning:: This action is only available for web service users with `xuc.rest.forward.noanswer.*.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /forward/noanswer/${USERNAME}         |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+


Body parameters
"""""""""""""""

+----------------+---------+-----------------------------------+
| Field          | Type    | Description                       |
+================+=========+===================================+
| destination    | string  | Destination of the forwarding     |
+----------------+---------+-----------------------------------+
| enabled        | boolean | State of the no answer forwarding |
+----------------+---------+-----------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/forward/noanswer/jbond  \
   -d '{"destination": "0123456789", "enabled": true}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"

Busy forwarding
---------------

This requests sets the busy forwarding of the user provided in the url according to the payload.

.. warning:: This action is only available for web service users with `xuc.rest.forward.busy.*.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /forward/busy/${USERNAME}             |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+


Body parameters
"""""""""""""""

+----------------+---------+-----------------------------------+
| Field          | Type    | Description                       |
+================+=========+===================================+
| destination    | string  | Destination of the forwarding     |
+----------------+---------+-----------------------------------+
| enabled        | boolean | State of the busy forwarding      |
+----------------+---------+-----------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/forward/busy/jbond  \
   -d '{"destination": "0123456789", "enabled": true}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


User call history
=================

Multiple APIs are available to retrieve user call history

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format:

::

  {
    "error": <error_code>
    "message": <cause>
  }

Else, the API will return the following JSON format:

::

   {
      "res": "success",
      "data": {...}
   }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Token sent to API is not found                      |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Request sent to API is not JSON valid               |
+------------------+------------------+-----------------------------------------------------+
| UserNotFound     | 400              | User not found                                      |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

User call history
-----------------
This request retrieves the call history of the user provided in the url.

.. warning:: This action is only available for web service users with `xuc.rest.history.*.read` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | GET                                   |
+--------+---------------------------------------+
| URI    | /history/${USERNAME}                  |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+

Path parameters
"""""""""""""""

+-------+---------+-----------------------------------+
| Field | Type    | Description                       |
+=======+=========+===================================+
| size  | integer | Number of calls to retrieve       |
+-------+---------+-----------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/history/jbond?size=10  \
   -H "Authorization: Bearer user_jwt_token"



User call history by days
-------------------------

This request retrieves the call history of the user provided in the url.

.. warning:: This action is only available for web service users with `xuc.rest.history.days.*.read` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | GET                                   |
+--------+---------------------------------------+
| URI    | /history/days/${USERNAME}             |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

URL parameters
""""""""""""""

+----------+--------+-------------------------------------------+
| Field    | Type   | Description                               |
+==========+========+===========================================+
| USERNAME | string | Username of the CTI user to make the call |
+----------+--------+-------------------------------------------+

Path parameters
"""""""""""""""

+-------+---------+-----------------------------------+
| Field | Type    | Description                       |
+=======+=========+===================================+
| days  | integer | Number of days to retrieve        |
+-------+---------+-----------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/history/days/jbond?days=10  \
   -H "Authorization: Bearer user_jwt_token"


Agent callbacks and tickets
===========================

Import and export callback requests and tickets

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

  {
    "error": <error_code>
    "message": <cause>
  }

Else, the API will return the following JSON format

::

   {
      "res": "success",
      "data": {...}
   }

Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Token sent to API is not found                      |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Request sent to API is not JSON valid               |
+------------------+------------------+-----------------------------------------------------+
| UserNotFound     | 400              | User not found                                      |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

Import callback requests
------------------------

This requests imports a csv of callback requests as text.

.. warning:: This action is only available for web service users with `xuc.rest.callback_lists.callback_requests.csv.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /callback_lists/callback_requests/csv |
+--------+---------------------------------------+
| Header | Content-Type: text/json               |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+

Body parameters
"""""""""""""""

+----------+--------+---------------------------------------+
| Field    | Type   | Description                           |
+==========+========+=======================================+
| listUuid | string | UUID of the list of callback requests |
+----------+--------+---------------------------------------+
| csv      | string | CSV contents of the callback requests |
+----------+--------+---------------------------------------+

.. note:: The `csv` parameter consists in one line: each line break should be replace with `\n`.


Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/callback_lists/callback_requests/csv \
   -H "Content-Type: text/json" \ 
   -H "Authorization: Bearer user_jwt_token" \
   -d '{"listUuid": "1", "csv": "phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|dueDate|period\n0230210092|0689746321|John|Doe|MyCompany|Call back quickly||\n0587963214|0789654123|Alice|O'Neill|YourSociety||2016-08-01|Afternoon"}'


Export callback tickets
-----------------------

This requests exports the list of tickets associated with the callback list uuid.

.. warning:: This action is only available for web service users with `xuc.rest.callback_lists.callback_tickets.csv.create` ACL.

Request
^^^^^^^

+--------+---------------------------------------+
| Method | POST                                  |
+--------+---------------------------------------+
| URI    | /callback_lists/callback_tickets/csv  |
+--------+---------------------------------------+
| Header | Content-Type: application/json        |
+--------+---------------------------------------+
| Header | Authorization: Bearer ${JWT_TOKEN}    |
+--------+---------------------------------------+


Body parameters
"""""""""""""""

+----------------+---------+-----------------------------------+
| Field          | Type    | Description                       |
+================+=========+===================================+
| listUuid       | string  | UUID of the list to export        |
+----------------+---------+-----------------------------------+

Curl example
""""""""""""

::

   curl -X POST http://localhost:8090/xuc/api/2.0/callback_lists/callback_tickets/csv  \
   -d '{"listUuid": "jsdfh-90298J-hdjkfd"}' \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer user_jwt_token"


.. _restapi_xuc_deprecated:

Deprecated APIs
===============

.. warning:: The following APIs are now deprecated.
 You can re-enable them to specific client based on their IP address. To allow a client, add its IP address to the DEPRECATED_API_HOST key in the *XiVO CC* :file:`custom.env` file. You can add multiple address separated by comma (,).

Connection
----------

This api is deprecated all the method are now able to autoconnect the user.

   POST http://localhost:$xucport/xuc/api/1.0/connect/$domain/$username/

::


   {"password" : "password"}

    curl -XPOST -d '{"password":"<password>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/connect/avencall.com/<username>/

DND
---

   POST http://localhost:$xucport/xuc/api/1.0/dnd/$domain/$username/

::


   {"state" : [false|true]}


   curl -XPOST -d '{"state":false}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dnd/avencall.com/<username>/


Dial
----
   POST http://localhost:$xucport/xuc/api/1.0/dial/$domain/$username/

::


   {"number" : "1101"}

   curl -XPOST -d '{"number":"<number>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dial/avencall.com/<username>/

Phone number sanitization
^^^^^^^^^^^^^^^^^^^^^^^^^

Dial command automatically applies filters to the phone number provided to make it valid for Xivo. Especially, it removes invalid characters and handles properly different notations of international country code.

Some countries don't follow the international standard and actually keep the leading zero after the country code (e.g. Italy). Because of this, if the zero isn't surrounded by parenthesis, the filter keeps it [1]_.

.. [1] See `Redmine ticket #150 <http://redmine.avencall.com:3000/issues/150>`_



DialByUsername
--------------
  POST http://localhost:$xucport/xuc/api/1.0/dialByUsername/$domain/$username/

::


   {"username" : "john"}

   curl -XPOST -d '{"username":"<username>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialByUsername/avencall.com/<username>/


DialFromQueue
-------------
   POST http://localhost:$xucport/xuc/api/1.0/dialFromQueue/$domain/$username/

::


   {"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}

   curl -XPOST -d '{"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialFromQueue/avencall.com/<username>/

**Limitations:** Queue No Answer settings does not work - see :ref:`xivo-queues-no-answer`. Except: when there is no free Agent to queue (none attached, all Agents on pause or busy), then No answer settings work (but Fail does not).

.. note:: Line should be configured with enabled "Ring instead of On-Hold Music" enabled (on "Application: tab in queue configuration - see :ref:`xivo-queues`). Otherwise the queue will answers the call and the destination rings even if there are no agents available.


Forward
-------
All forward commands use the above payload

::


      {"state" : [true|false],
        "destination" : "1102")



Unconditionnal
^^^^^^^^^^^^^^
   POST http://localhost:$xucport/xuc/api/1.0/uncForward/$domain/$username/

::


   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/uncForward/avencall.com/<username>/


On No Answer
^^^^^^^^^^^^
   POST http://localhost:$xucport/xuc/api/1.0/naForward/$domain/$username/

::


   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/naForward/avencall.com/<username>/


On Busy
^^^^^^^
   POST http://localhost:$xucport/xuc/api/1.0/busyForward/$domain/$username/

::


   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/busyForward/avencall.com/<username>/


Handshake
---------
Will repost all events on the configured URL

   POST http://localhost:$xucport/xuc/api/1.0/handshake/$domain/

Agent
-----

AgentLogin
^^^^^^^^^^
Log an agent on an extension

    POST http://$xuchost:$xucport/xuc/api/1.0/agentLogin/

::


    curl -XPOST -d '{"agentphonenumber":"<phone number>", "agentnumber":"<agent number>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/agentLogin/


AgentLogout
^^^^^^^^^^^
Logout un agent

    POST http://$xuchost:$xucport/xuc/api/1.0/agentLogout/

::


    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/agentLogout/

TogglePause
^^^^^^^^^^^
Change state of an agent, pause if ready, ready if on pause or on wrapup

    POST http://$xuchost:$xucport/xuc/api/1.0/togglePause/

::


    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/togglePause/

User Call History by size
-------------------------
Get the last X calls of the user call history


::


    curl -XGET -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/historyByUsername/<domain>/<username>?size=X


Answer

::


    [
      {"start":"2018-09-20 17:38:41","duration":"00:00:02","srcUsername":"bruce","dstUsername":"alicej","status":"emitted"},
      {"start":"2018-09-20 17:19:40","duration":"00:00:01","srcUsername":"bruce","dstUsername":"cquefia","status":"emitted"},
      {"start":"2018-09-20 17:15:00","duration":"00:00:18","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"},
      {"start":"2018-09-20 17:14:16","duration":"00:00:11","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"}
    ]

User Call History by days
-------------------------
Get user call history of the last X days


::


    curl -XGET -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/historyByUsername/<domain>/<username>?days=X


Answer

::


    [
      {"start":"2018-09-20 17:38:41","duration":"00:00:02","srcUsername":"bruce","dstUsername":"alicej","status":"emitted"},
      {"start":"2018-09-20 17:19:40","duration":"00:00:01","srcUsername":"bruce","dstUsername":"cquefia","status":"emitted"},
      {"start":"2018-09-20 17:15:00","duration":"00:00:18","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"},
      {"start":"2018-09-20 17:14:16","duration":"00:00:11","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"}
    ]


Export events
=============

  Xuc post JSON formated events on URL ``eventUrl = "http://localhost:8090/xivo/1.0/event/avencall.com/dropbox/"``
  configured in /usr/share/xuc/application.conf


Phone Event Notification
------------------------

Related to a username, phone event is in message payload same structure as javascript :ref:`phoneevents`

::


    {
      "username":"alicej",
      "message":{
         "msgType":"PhoneEvent",
         "ctiMessage":{"eventType":"EventDialing","DN":"1058","otherDN":"3000","linkedId":"1447670380.34","uniqueId":"1447670380.34","userData":{"XIVO_USERID":"9"}}}}

