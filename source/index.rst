.. XiVO-doc Documentation master file.

********************************************************
XiVO Solutions |version| Documentation (Kuma Edition)
********************************************************

.. important:: **What's new in this version ?**

   **WARNING:**  This version is not publicly released yet... only technical release so far !

   * New technical APIs available for webservice users replacing all :ref:`deprecated API <restapi_xuc_deprecated>`: with flexible access control list.
   * Overall deployment ease
   * Merging db of UC Addon in only one instance of PostgreSQL
   * Collecting Anonymous data usage to focus future roadmap on features widely used 
   * Update to recent version of Asterisk (20) and PostgresSQL(15)
   * Embeding call quality visualisation tool to debug bad quality voice communications
   
   See :ref:`kuma_release` page for the complete list of **New Features** and **Behavior Changes**.
  
   **Deprecations**   

     * End of Support for LTS Electra (2020.07).
     * End of Support for SCCP Protocol.
     * :ref:`deprecated API <restapi_xuc_deprecated>`: will be definitely removed in next version


   

.. figure:: logo_xivo.png
 
XiVO_ solutions developed by Wisper_ group is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _Wisper: https://wisper.io/
.. _XiVO: https://xivo.solutions/
.. _Jabbah: https://documentation.xivo.solutions/en/2022.10/

.. toctree::
   :maxdepth: 2

   introduction/introduction
   getting_started/getting_started
   installation/index
   administrator/index
   ipbx_configuration/administration
   contact_center/contact_center
   edge/index
   meetingrooms/index
   ivr/index
   mobile_application/index
   usersguide/index
   Devices <https://documentation.xivo.solutions/projects/devices/en/latest/>
   api_sdk/api_sdk
   contributing/contributing
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
