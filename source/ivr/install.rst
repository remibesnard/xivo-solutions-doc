.. _ivr_install:

************************************
IVR Installation & Upgrade
************************************

.. contents:: :local:

Requirements
============

.. _ivr_install_base:

.. important:: Please contact support contact@wisper.io to get access to the IVR Editor components

Server Requirements
-------------------

The IVR Editor component must be installed on a XiVO server.

Base installation
=================

.. _ivr_upgrade:

1. Docker compose Installation
------------------------------

These commands will install docker compose on the host.

.. code-block:: bash

    # Download docker compose
    TAG_OR_BRANCH=2023.05.00
    cd /etc/docker/xivo
    wget https://gitlab.com/xivo.solutions/ivr-editor/-/raw/${TAG_OR_BRANCH}/docker-xivo-ivr.override.yml

2. IVR Editor configuration
---------------------------

#. Enable IVR in custom.env file

.. code-block:: bash

    echo "IVR_INSTALLED=yes" >> /etc/docker/xivo/custom.env

Start the services
------------------

Start the services:

.. code-block:: bash

  docker login -u xivoxc
  (use the token provided by the XiVO team)

  xivo-dcomp pull
  xivo-dcomp up -d

Upgrade
=======

Currently there is no *automatic upgrade* process. Here is the manual process that you need to follow on the XiVO host.


#. Make a backup of the IVR Editor compose override file:

.. code-block:: bash

    cp -aR /etc/docker/xivo/docker-xivo-ivr.override.yml /var/tmp/docker-xivo-ivr.override.yml

#. Re-download the ivr override compose file

.. code-block:: bash

    # Download docker compose
    TAG_OR_BRANCH=2022.10
    cd /etc/docker/xivo
    wget https://gitlab.com/xivo.solutions/ivr-editor/-/raw/${TAG_OR_BRANCH}/docker-xivo-ivr.override.yml

#. Finally pull the new images and restart the containers:

.. code-block:: bash

    xivo-dcomp pull
    xivo-dcomp up -d
