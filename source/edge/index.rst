.. _xivo_edge:

*********
XiVO Edge
*********

The XiVO Edge is a new product introduced in the Gaia LTS.

His intended usage is to be able to use the WebRTC application without the need of a VPN.
See architecture for more details.

.. toctree::
   :maxdepth: 2

   architecture
   install
   configuration
   administration
   features
