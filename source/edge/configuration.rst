.. _edge_config:

******************
Edge Configuration
******************

.. contents:: :local:

Edge Server configuration
=========================

.. _edge_config_sslcert:

SSL Certificates
----------------

.. note:: This step is to be done on each host

The Edge Solution must be configured with valid certificate for your domain.

You have to put the:

* **fullchain** certificate: here :file:`/etc/docker/ssl/xivo-edge.crt`
  and also here :file:`/etc/docker/ssl/xivo-edge.chain` (this last location is mandatory for the SIP Proxy service)
* certificate key: here :file:`/etc/docker/ssl/xivo-edge.key`


Make sure that the certificate key are given readonly permission to root:

.. code-block:: bash

    chmod 400 /etc/docker/ssl/xivo-edge.key

.. _edge_config_turnsecret:

TURN Server Secret
------------------

You must generate a secret for the TURN server (in the following it will referred to as ``<TURN_SECRET>``).

Generate it with the following command:

.. code:: bash

    openssl rand -hex 16

Keep this ``<TURN_SECRET>``. You will need it to configure

* the TURN Server
* the xucserver on the *XiVO CC/UC*
* and to generate the turn configuration on for asterisk (on XiVO Main and MDS)

.. _edge_config_turnrelaypermission:

TURN Server Relay Authorization
-------------------------------

.. important:: You **MUST** follow this step on the TURN Server host.
    Otherwise the TURN relay **will not work**.

You **must add to the TURN configuration** the **IP addresses it is allowed to relay traffic to**.
Therefore you must add the permission for the IP of:

* the XiVO
* the MDS (if in XDS architecture)
* and the Meetingroom (if installed)

In order to do this:

* lists the IP addresses towards which the **STUN/TURN Server** will be allowed to send traffic to. Which is at least:

  * the XiVO
  * (if applicable) the MDS
  * (if applicable) the Meetingroom
* then add to the :file:`.env` file (of the server which will host **STUN/TURN Server** (coturn) service) a line with the following format::

    TURN_ALLOWED_PEERS="--allowed-peer-ip=<XIVO IP> --allowed-peer-ip=<MDS1 IP> --allowed-peer-ip=<MEETINGROOM IP>"

  For example::

    TURN_ALLOWED_PEERS="--allowed-peer-ip=192.168.240.2 --allowed-peer-ip=192.168.240.4"


.. _edge_config_3servers:

3 Servers Configuration
-----------------------

.. note:: Follow this part if you configure the Edge Solution on 3 servers

.. _edge_config_3serversschema:

3 Servers Example Schema
^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: images/edge_archi_3servers_example.png
   :alt: edge_archi_3servers_example

Web Proxy
^^^^^^^^^

.. note:: This step is to be done on the server which will host the **Web Proxy** (nginx) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: bash

    cat > /etc/docker/edge/.env << EOF
    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    XIVOCC_HOST=<IP ADDRESS OF THE XIVO CC/UC (xucserver/xucmgt/nginx server)>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    EDGE_FQDN=<XIVO EDGE FQDN>
    EDGE_KAMAILIO_HOST=<IP ADDRESS OF THE KAMAILIO SERVER>
    EOF

**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    XIVOCC_HOST=LAN_IP1
    XIVO_HOST=LAN_IP2
    EDGE_FQDN=edge.mycompany.com
    EDGE_KAMAILIO_HOST=DMZ_IP2


.. _edge_config_3serverssipproxy:

SIP Proxy
^^^^^^^^^

.. note:: This step is to be done on the server which will host the **SIP Proxy** (kamailio) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: bash

    cat > /etc/docker/edge/.env << EOF
    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    EDGE_FQDN=<XIVO EDGE FQDN>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    XIVO_MDS_HOST_DEFAULT="default: <DATA IP ADDRESS TO MDS DEFAULT>"
    EDGE_HOST_IP=<Edge server IP on which to listen>
    EOF

.. important:: If you configure the Edge Solution for a XDS installation you must:

     * add to the ``.env`` file a line per MDS with the following format::

         XIVO_MDS_HOST_MDS1="<mds technical name>: <DATA IP ADDRESS TO MDS1>"

      For example: ``XIVO_MDS_HOST_MDS1="mds1: 192.168.240.2"``

      Mandatory step: You need to change any underscore in your mds naming to an hyphen
      For example, for an mds named media_server_1 : ``XIVO_MDS_HOST_MDS1="media-server-1: 192.168.240.2"``

     * and add to the ``extra_hosts`` section of the kamailio service the
       variable ``XIVO_MDS_HOST_MDS1``

**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    EDGE_FQDN=edge.mycompany.com
    XIVO_HOST=LAN_IP2
    XIVO_MDS_HOST_DEFAULT="default: LAN_IP2"
    XIVO_MDS_HOST_MDS1="mds1: LAN_IP3"
    EDGE_HOST_IP=DMZ_IP2


STUN/TURN server
^^^^^^^^^^^^^^^^

.. note:: This step is to be done on the server which will host the **STUN/TURN Server** (coturn) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: bash

    cat > /etc/docker/edge/.env << EOF
    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    EDGE_HOST_IP=<IP OF THE STUN/TURN SERVER>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    TURN_EXTERNAL_IP=<External IP of the edge Server>
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=<domain name of client>
    TURN_ALLOWED_PEERS=<list of allowed peers IP>
    EOF


.. important:: Don't forget to fill in the ``TURN_ALLOWED_PEERS`` var following :ref:`edge_config_turnrelaypermission`


**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    EDGE_HOST_IP=DMZ_IP3
    XIVO_HOST=LAN_IP2
    TURN_EXTERNAL_IP=DMZ_IP3
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=mycompany.com
    TURN_ALLOWED_PEERS="--allowed-peer-ip=LAN_IP2 --allowed-peer-ip=LAN_IP3 --allowed-peer-ip=LAN_IP4"


.. _edge_config_1server:

1 Server Configuration
----------------------

.. note:: Follow this part if you configure the Edge Solution 1 server


.. _edge_config_1serverschema:

1 Server Example Schema
^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: images/edge_archi_1server_example.png
   :alt: edge_archi_1server_example

.. _edge_config_1serverconf:

Web Proxy, SIP Proxy and TURN Server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: bash

    cat > /etc/docker/edge/.env << EOF
    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    XIVOCC_HOST=<IP ADDRESS OF THE XIVOCC>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    EDGE_FQDN=<EDGE FQDN>
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=<EDGE DOMAIN>
    EDGE_HOST_IP=<EDGE HOST IP>
    TURN_EXTERNAL_IP=<EDGE HOST IP>
    TURN_ALLOWED_PEERS=<list of allowed peers IP>
    XIVO_MDS_HOST_DEFAULT="default: <DATA IP ADDRESS TO MDS DEFAULT>"
    EOF


.. important:: Don't forget to fill in the ``TURN_ALLOWED_PEERS`` var following :ref:`edge_config_turnrelaypermission`

.. _edge_config_1server_xds-meetingroom:

.. important:: **Specific steps when configuring the Edge Solution with XDS architecture**

     **Add alias for MDS IP (for SIP Proxy)**: you must add the MDS in the extra_host
     section of the *SIP Proxy* yml file. For this do:

       * add to the :file:`.env` file a line **per MDS** with the following format::

           XIVO_MDS_HOST_MDS1="<mds technical name>: <DATA IP ADDRESS TO MDS1>"

        For example: ``XIVO_MDS_HOST_MDS1="mds1: 192.168.240.2"``

        Mandatory step: You need to change any underscore in your mds naming to an hyphen
        For example, for an mds named media_server_1 : ``XIVO_MDS_HOST_MDS1="media-server-1: 192.168.240.2"``

       * and add to the ``extra_hosts`` section of the kamailio service the
         defined variable ``XIVO_MDS_HOST_MDS1`` (in file :file:`kamailio-edge.yml`):

         .. code-block:: yaml
            :emphasize-lines: 3

            extra_hosts:
            - ${XIVO_MDS_HOST_DEFAULT}
            - ${XIVO_MDS_HOST_MDS1}


**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_1serverschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2022.10
    XIVOCC_DIST=latest
    XIVOCC_HOST=LAN_IP1
    XIVO_HOST=LAN_IP2
    EDGE_FQDN=edge.mycompany.com
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=mycompany.com
    EDGE_HOST_IP=DMZ_IP1
    TURN_EXTERNAL_IP=DMZ_IP1
    XIVO_MDS_HOST_DEFAULT="default: LAN_IP2"
    XIVO_MDS_HOST_DEFAULT="mds1: LAN_IP3"
    TURN_ALLOWED_PEERS="--allowed-peer-ip=LAN_IP2 --allowed-peer-ip=LAN_IP3 --allowed-peer-ip=LAN_IP4"


Start the services
------------------

To start the services run the following command (you have to run it on all servers if you are in a 3 servers installation):

.. code:: bash

    edge-dcomp up -d

XiVO CC configuration
=====================

.. note:: These steps are to be done on the XiVO CC.

   For :ref:`xivouc_addon` mode see :ref:`edge_config_xivoucaddon` section.

On XiVO CC server (the server which hosts the xucmgt/xucserver and nginx) add the following variables in the ``/etc/docker/compose/custom.env`` file:

* add XUC_INTERNAL_HOST=<XIVOCC_IP>
* change XUC_HOST to the EDGE FQDN
* and add the TURN_SERVER_SECRET with the ``<TURN_SECRET>`` value
  (the ``<TURN_SECRET>`` is the secret generated for during :ref:`edge_config_turnsecret`)
* specify the ``REPORTING_HOST`` and ``ELASTICSEARCH_HOST`` to the XiVO CC LAN IP address in order for SpagoBI and Kibana report to work (note that SpagoBI and Kibana work only if you access them via the internal LAN IP address).
* in order for the history to work add the RECORDING_SERVER_HOST=<IP of recording server>

.. code-block:: ini
    
    XUC_INTERNAL_HOST=<XIVOCC_IP>
    XUC_HOST=<XIVO EDGE FQDN>
    TURN_SERVER_SECRET=<TURN_SECRET>
    REPORTING_HOST=<LAN_IP1>
    ELASTICSEARCH_HOST=<LAN_IP1>
    RECORDING_SERVER_HOST=<RECORDING_SERVER_IP>
    RECORDING_SERVER_PORT=9400

* then you need to recreate components containers

  .. code-block:: bash

    xivocc-dcomp up -d

.. _edge_config_xivoucaddon:

XiVO UC add-on Configuration
----------------------------

.. important:: This section applies only if you are in the :ref:`xivouc_addon` mode.

To configure the UC-addon part of a XiVO, add the following variables in the :file:`/etc/docker/compose/custom.env` file:

* change XUC_HOST to the EDGE FQDN
* and add the TURN_SERVER_SECRET with the ``<TURN_SECRET>`` value
  (the ``<TURN_SECRET>`` is the secret generated for during :ref:`edge_config_turnsecret`)

.. code-block:: ini
    
    XUC_HOST=<XIVO EDGE FQDN>
    TURN_SERVER_SECRET=<TURN_SECRET>

* then you need to recreate components containers

  .. code-block:: bash

    xivocc-dcomp up -d


XiVO Configuration
==================

.. note:: These steps are to be done on the XiVO

   If you are using the :ref:`xivouc_addon` mode
   you must also follow the section :ref:`edge_config_xivoucaddon`


STUN/TURN server configuration
------------------------------

On the XiVO you need to configure the STUN/TURN server address.

.. warning:: Your Edge installation must be up and running when you do this step.
    As soon as you change this configuration all WebRTC users **won't work** without the Edge Solution.

* In the XiVO Admin webi :menuselection:`Services -> IPBX -> General Settings -> SIP Protocol`
  tab **Network**
* In parameter :guilabel:`XiVO Edge FQDN` enter the **TURN server**
  address with format ``FQDN:3478`` or ``IP:3478`` (you must put port **3478**).
  **Warning:** this FQDN must be resolvable and reachable from the XiVO & Public Internet.

  * 1 Server deployment: use the server public IP address or FQDN (i.e. ``edge.mycompany.com`` or ``DMZ_IP1`` in 1 Server Schema)
  * 3 Servers deployment: use the TURN server public IP address or FQDN (i.e. ``DMZ_IP3`` in 3 Servers Schema)
* Click save: it will reload the SIP and the RTP configuration of the XiVO (Main and MDS if any)
* Then **on each XiVO/MDS** run the following script to generate the credentials for the TURN server
  (the ``<TURN_SECRET>`` is the secret generated during :ref:`edge_config_turnsecret`):

  .. code-block:: bash

      xivo-edge-gen-turn-cred <TURN_SECRET>

* This script will generate new turn username and password in :file:`/etc/asterisk/rtp.d/01-xivo-edge-turn-cred.conf`
* You then need to reload the rtp configuration

  .. code-block:: bash

    asterisk -rx 'module reload res_rtp_asterisk.so'


.. note:: The turn address ``turnaddr`` is generated by confgend (see conf in file :file:`/etc/asterisk/rtp.conf`).

Fail2ban
--------

.. important:: If you configure the Edge Solution for a XDS installation you must do it on each MDS.

You need to add Kamailio's ip address to protect it from
being banned and resulting in complete block of webrtc users.

* Edit file :file:`/etc/fail2ban/filter.d/asterisk-xivo.conf` and add to the ``ignoreregex`` section:

  * In *3 Servers Schema* replace ``<SIP_PROXY_HOST_IP>`` by ``DMZ_IP2``
  * In *1 Server Schema* replace ``<SIP_PROXY_HOST_IP>`` by ``DMZ_IP1``

  .. code-block:: ini

      ignoreregex = <SIP_PROXY_HOST_IP>


* Finally restart fail2ban service:

  .. code-block:: bash

    systemctl restart fail2ban.service
