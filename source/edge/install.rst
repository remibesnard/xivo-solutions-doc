.. _edge_install:

***************************
Edge Installation & Upgrade
***************************

.. contents:: :local:

Requirements
============

Server Requirements
-------------------

In case of :ref:`edge_install_3servers` each server should have:

* OS : **Debian 11** (Bullseye), 64 bits.
* CPU: 2 CPU
* RAM: 2 Gb
* DD: 50 Gb

In case of :ref:`edge_install_1server` the server should have:

* OS : **Debian 11** (Bullseye), 64 bits.
* CPU: 6 CPU
* RAM: 6 Gb
* DD: 50 Gb


Network Requirements
--------------------

You will also need:

* 3 public IP addresses (or 1 for :ref:`edge_install_1server` deployment)
* 1 FQDN (i.e. edge.mycompany.com) that resolves:

  * to the Web Proxy server public IP address (in case of :ref:`edge_install_3servers`)
  * to the Edge server public IP address (in case of :ref:`edge_install_1server` deployment)
* a valid SSL certificate for this FQDN
* authorize the network flow as shown in the :ref:`edge_archi_network` section
* to make all your WebRTC users connect to the UC application via the Edge Solution

  .. important:: otherwise it won't work !


.. _edge_install_base:

Base installation
=================

Whatever the type of installation (1 or 3 servers) you need to do the following on the host(s):

#. install docker and docker compose
#. download XiVO Edge service launcher


1. Docker & Docker compose Installation
---------------------------------------

These commands will install docker and docker compose on the host.

.. code-block:: bash

    # Install docker prerequisites
    apt install wget dirmngr gnupg ca-certificates ntp curl

    # Install docker
    DOCKER_KEYRING_FILE="/etc/apt/trusted.gpg.d/download.docker.com.gpg"
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key --keyring ${DOCKER_KEYRING_FILE} add -
    echo "deb https://download.docker.com/linux/debian bullseye stable" > /etc/apt/sources.list.d/docker.list
    cat > /etc/apt/preferences.d/docker-ce <<EOF
    Package: docker-ce*
    Pin: version 5:20.10.13*
    Pin-Priority: 1000
    EOF

    apt update
    apt install docker-ce

    #Install docker-compose
    DOCKER_COMPOSE_VERSION=1.29.2
    COMPOSE="/usr/local/bin/docker-compose"
    curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > "$COMPOSE"
    chmod +x "$COMPOSE"


.. _edge_install_launcher:

2. XiVO Edge Launcher Setup
---------------------------

The following commands describe how to install the Edge Solution launcher.

*  Create ``edge`` directory:
	
   .. code-block:: bash

    mkdir -p /etc/docker/edge

.. _edge_install_launcher_downloadextract:

*  Download *XiVO Edge* configuration. In the following script replace ``TAG_OR_BRANCH`` by the name of a tag
   or a branch.

   .. note:: currently, to install the latest stable version of Jabbah, use ``TAG_OR_BRANCH=2023.05.00``

   .. code-block:: bash
     
    TAG_OR_BRANCH=2023.05.00
    cd /etc/docker/edge
    wget "https://gitlab.com/xivo.solutions/xivo-edge/-/archive/${TAG_OR_BRANCH}/${TAG_OR_BRANCH}.tar.gz"
    tar -zxvf ${TAG_OR_BRANCH}.tar.gz -C /etc/docker/edge --strip-components 1
    rm ${TAG_OR_BRANCH}.tar.gz

*  Create the ``ssl`` dir to put the ssl certificates:
   
   .. code-block:: bash

    mkdir -p /etc/docker/ssl

*  Congrats: **You're done with the base installation**.
  
  * **Next step** follow:

    * either the :ref:`edge_install_3servers` 
    * or the :ref:`edge_install_1server`


.. _edge_install_3servers:

3 Servers Installation
======================

.. note:: If you're doing the 3 servers installation (1 host per service) you must follow the three subsections below.

You need one machine per service with :ref:`edge_install_base` done on all three.

Then you need to install the services on each host independently as explained below. 

Web Proxy
---------

.. note:: This step is to be done on the server which will host the **Web Proxy** (nginx) service.


*  Create bash alias to launch services:

  .. code-block:: bash

   echo "alias edge-dcomp='docker-compose -p edge -f /etc/docker/edge/nginx-edge.yml --env-file=/etc/docker/edge/.env'" >> ~/.bashrc
   source ~/.bashrc


SIP Proxy
---------

.. note:: This step is to be done on the server which will host the **SIP Proxy** (kamailio) service.

*  Create bash alias to launch services:

  .. code-block:: bash

   echo "alias edge-dcomp='docker-compose -p edge -f /etc/docker/edge/kamailio-edge.yml -f /etc/docker/edge/kamailio-edge.override.yml --env-file=/etc/docker/edge/.env'" >> ~/.bashrc
   source ~/.bashrc


TURN Server
-----------

.. note:: This step is to be done on the server which will host the **TURN Server** (coturn) service.

*  Create bash alias to launch services:

  .. code-block:: bash

   echo "alias edge-dcomp='docker-compose -p edge -f /etc/docker/edge/coturn-edge.yml --env-file=/etc/docker/edge/.env'" >> ~/.bashrc
   source ~/.bashrc


Next step
---------

*  Congrats: You're done with the Edge 3 Servers Installation.
  
  * **Next step**: go to :ref:`edge_config`


.. _edge_install_1server:

1 Server Installation
=====================

.. note:: If you're doing the mono server installation (all services on one host) you must follow the subsection below.


Web Proxy, SIP Proxy and TURN Server
------------------------------------

* Create bash alias to launch services:
   
   .. code-block:: bash

    echo "alias edge-dcomp='docker-compose -p edge -f /etc/docker/edge/nginx-edge.yml -f /etc/docker/edge/coturn-edge.yml -f /etc/docker/edge/kamailio-edge.yml --env-file=/etc/docker/edge/.env'" >> ~/.bashrc
    source ~/.bashrc


Next step
---------

* Congrats: You're done with the Edge Mono Server Installation.

  * **Next step**: go to :ref:`edge_config`


Upgrade
=======

Currently there is no *automatic upgrade* process. Here is the manual process that you need to follow on the 3 edge servers (or the edge server depending if it's a mono or three servers install).

* Make a backup of the Edge launcher:

  .. code-block:: bash

    cp -aR /etc/docker/edge/ /var/tmp/edge-backup/

* Re-install the Edge Launcher (it will override the :file:`.yml` files): follow the :ref:`Download and Extract of the Edge launcher files <edge_install_launcher_downloadextract>` by taking the new versions

  .. note:: This step overrides the current :file:`.yml` files. If you had made some customization in them you will have to backport them by comparing the new one with the backup you did at previous step.
* Replace the ``edge-dcomp`` alias by what is described in the :ref:`edge_install_3servers` or :ref:`edge_install_1server` sections (replace the aliases or verify that they did not change).
* And then verify that the content of the :file:`.env` file is correct:

  * Compare the old version and what is defined in the :ref:`edge_config` section.
  * Verify that the ``XIVOCC_TAG`` and ``XIVOCC_DIST`` correspond to what you want to install (it should be `2022.10` and `latest`).
* Finally pull the new images and restart the containers:

  .. warning:: it will stop all WebRTC calls (and disconnect users from the application).

  .. code-block:: bash

    edge-dcomp pull
    edge-dcomp up -d
